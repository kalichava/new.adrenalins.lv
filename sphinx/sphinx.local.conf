indexer
{
    mem_limit = 128M
}

searchd
{
    listen = 9312
    listen = 127.0.0.1:9306:mysql41
    read_timeout = 5
    max_children = 30
    pid_file = /var/www/new.adrenalins.lv/sphinx/pid/searchd.pid
    max_matches = 10000
    workers = threads
    seamless_rotate = 1
    preopen_indexes = 1
    unlink_old = 1
    log = /var/www/new.adrenalins.lv/sphinx/log/searchd.log
    binlog_path = /var/www/new.adrenalins.lv/sphinx/data
    #query_log = /var/www/new.adrenalins.lv/sphinx/log/query.log
}

source base
{
    type = mysql
    sql_host = 127.0.0.1
    sql_user = adrenalins
    sql_pass = adrenalins
    sql_db = adrenalins
    sql_port = 3306

    sql_query_pre = SET NAMES utf8
    sql_query_pre = SET CHARACTER SET utf8
    sql_ranged_throttle = 0
}

# galleries
source gallery : base
{
    sql_query_range = SELECT MIN(id), MAX(id) FROM media__gallery
    sql_range_step = 2000

    sql_query = \
      SELECT \
        CONCAT(g.id, crc32('gallery')) as id, \
        g.id, \
        0 as type, \
        g.id as object_id, \
        'gallery' as object_type, \
        g.name as title, \
        '' as short_description, \
        '' as content, \
        g.created_at \
      FROM media__gallery g \
      WHERE g.enabled = 1 \
      AND id >= $start AND id <= $end \

    # int
    sql_attr_uint = id
    sql_attr_uint = type
    sql_attr_uint = object_id

    # string
    sql_field_string = created_at
    sql_field_string = object_type
}

index gallery
{
    source = gallery
    path = /var/www/new.adrenalins.lv/sphinx/data/gallery/gallery
    docinfo = extern
    mlock = 0
    min_word_len = 1
    min_infix_len = 1
    charset_type = utf-8
    charset_table = 0..9, A..Z->a..z, _, a..z, \
    	U+410..U+42F->U+430..U+44F, U+430..U+44F
}

# static page indexes
source static_page_lv : base
{
    sql_query_range = SELECT MIN(id), MAX(id) FROM pages
    sql_range_step = 2000

    sql_query = \
      SELECT \
        CONCAT(p.id, crc32('static_page_lv')) as id, \
        p.id, \
        0 as type, \
        p.id as object_id, \
        'static_page' as object_type, \
        p.title, \
        '' as short_description, \
        p.content, \
        p.created_at \
      FROM pages p \
      WHERE p.is_active = 1 \
      AND id >= $start AND id <= $end \

    # int
    sql_attr_uint = id
    sql_attr_uint = type
    sql_attr_uint = object_id

    # string
    sql_field_string = created_at
    sql_field_string = object_type
}

index static_page_lv
{
    source = static_page_lv
    path = /var/www/new.adrenalins.lv/sphinx/data/static_page/static_page_lv
    docinfo = extern
    mlock = 0
    min_word_len = 1
    min_infix_len = 1
    charset_type = utf-8
    charset_table = 0..9, A..Z->a..z, _, a..z, \
        U+410..U+42F->U+430..U+44F, U+430..U+44F
}

source static_page_en : base
{
    sql_query_range = SELECT MIN(id), MAX(id) FROM pages
    sql_range_step = 2000

    sql_query = \
      SELECT \
        CONCAT(p.id, crc32('static_page_en')) as id, \
        p.id, \
        0 as type, \
        p.id as object_id, \
        'static_page' as object_type, \
        (SELECT content FROM page_translations nt WHERE field = 'title' AND locale = 'en' AND nt.object_id = p.id) as title, \
        '' as short_description, \
        (SELECT content FROM page_translations nt WHERE field = 'content' AND locale = 'en' AND nt.object_id = p.id) as content, \
        p.created_at \
      FROM pages p \
      WHERE p.is_active = 1 \
      AND id >= $start AND id <= $end \

    # int
    sql_attr_uint = id
    sql_attr_uint = type
    sql_attr_uint = object_id

    # string
    sql_field_string = created_at
    sql_field_string = object_type
}

index static_page_en
{
    source = static_page_en
    path = /var/www/new.adrenalins.lv/sphinx/data/static_page/static_page_en
    docinfo = extern
    mlock = 0
    min_word_len = 1
    min_infix_len = 1
    charset_type = utf-8
    charset_table = 0..9, A..Z->a..z, _, a..z, \
    	U+410..U+42F->U+430..U+44F, U+430..U+44F
}

source static_page_ru : base
{
    sql_query_range = SELECT MIN(id), MAX(id) FROM pages
    sql_range_step = 2000

    sql_query = \
      SELECT \
        CONCAT(p.id, crc32('static_page_ru')) as id, \
        p.id, \
        0 as type, \
        p.id as object_id, \
        'static_page' as object_type, \
        (SELECT content FROM page_translations nt WHERE field = 'title' AND locale = 'ru' AND nt.object_id = p.id) as title, \
        '' as short_description, \
        (SELECT content FROM page_translations nt WHERE field = 'content' AND locale = 'ru' AND nt.object_id = p.id) as content, \
        p.created_at \
      FROM pages p \
      WHERE p.is_active = 1 \
      AND id >= $start AND id <= $end \

    # int
    sql_attr_uint = id
    sql_attr_uint = type
    sql_attr_uint = object_id

    # string
    sql_field_string = created_at
    sql_field_string = object_type
}

index static_page_ru
{
    source = static_page_ru
    path = /var/www/new.adrenalins.lv/sphinx/data/static_page/static_page_ru
    docinfo = extern
    mlock = 0
    min_word_len = 1
    min_infix_len = 1
    charset_type = utf-8
    charset_table = 0..9, A..Z->a..z, _, a..z, \
    	U+410..U+42F->U+430..U+44F, U+430..U+44F
}

# news indexes
source news : base
{
    sql_query_range = SELECT MIN(id), MAX(id) FROM news
    sql_range_step = 2000

    sql_query = \
      SELECT \
        CONCAT(n.id, crc32('news')) as id, \
        n.id, \
        n.type, \
        n.id as object_id, \
        'news' as object_type, \
        n.title, \
        n.short_description, \
        n.content, \
        n.created_at \
      FROM news n \
      WHERE n.is_active = 1 \
      AND id >= $start AND id <= $end \

    # int
    sql_attr_uint = id
    sql_attr_uint = type
    sql_attr_uint = object_id

    # string
    sql_field_string = created_at
    sql_field_string = object_type
}

index news
{
    source = news
    path = /var/www/new.adrenalins.lv/sphinx/data/news/news
    docinfo = extern
    mlock = 0
    min_word_len = 1
    min_infix_len = 1
    charset_type = utf-8
    charset_table = 0..9, A..Z->a..z, _, a..z, \
    	U+410..U+42F->U+430..U+44F, U+430..U+44F
}