<?php

namespace Application\Sonata\MediaBundle\Controller;

use Locale;
use IntlDateFormatter;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

/**
 * Class MediaController
 *
 * @package Adrenalins\FaqBundle\Controller
 */
class MediaController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $page = null;
        $paginator = $this->get('knp_paginator');

        $limit = (int) $this->get('adrenalins.settings')->get('media-per-page', 12);
        $pageNum = (int) $request->query->get('page', 1);
        $context = trim(strip_tags($request->get('context')));

        /** @var PageRepository $pageRepo */
        $repo = $em->getRepository('PageBundle:Page');
        $parent = $repo->findOneBySlug('media');
        if ($parent) {
            $page = $repo->findActivePage($context, $parent);
        }

        $method = sprintf('get%sViewData', ucfirst($context));
        $contentData = $this->$method($pageNum, $limit, $paginator);

        if ($contentData instanceof JsonResponse) {
            return $contentData;
        }

        $viewData = array_merge(
            [
                'page'    => $page,
                'context' => $context
            ],
            $contentData
        );

        $template = sprintf('ApplicationSonataMediaBundle::index_%s.html.twig', $context);

        return $this->render($template, $viewData);
    }

    /**
     * @param integer           $page
     * @param integer           $limit
     * @param SlidingPagination $paginator
     *
     * @return array
     */
    private function getPhotoViewData($page, $limit, $paginator)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em
            ->getRepository('ApplicationSonataMediaBundle:Gallery')
            ->createQueryBuilder('g')
            ->where('g.enabled = true')
            ->andWhere("g.context IN ('users', 'photo')")
            ->orderBy('g.createdAt', 'DESC')
            ->getQuery();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate(
            $query,
            $page,
            $limit
        );

        if ($this->get('request')->isXmlHttpRequest()) {
            $items = [];
            $formatter = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::NONE, IntlDateFormatter::NONE);
            $formatter->setPattern("dd MMMM yyyy");

            /** @var Gallery $item */
            foreach ($pagination->getItems() as $item) {
                $galleryImage = '';
                $galleryMedia = $item->getGalleryHasMedias();
                $galleryMediaNum = $galleryMedia->count();

                if ($galleryMediaNum) {
                    /** @var Media $media */
                    $media = $galleryMedia->first()->getMedia();
                    $provider = $this->get($media->getProviderName());
                    $providerFormat = $provider->getFormatName($media, 'medium');
                    $galleryImage = $provider->generatePublicUrl($media, $providerFormat);
                }

                $route = 'sonata_media_gallery_view';
                $itemUrl = $this->generateUrl($route, ['id' => $item->getId()], true);
                $publishedAt = $formatter->format($item->getCreatedAt());

                $items[] = [
                    'url'         => $itemUrl,
                    'title'       => $item->getName(),
                    'image'       => $galleryImage,
                    'publishedAt' => $publishedAt,
                ];
            }

            return new JsonResponse(
                [
                    'total'  => (!empty($items)) ? $pagination->getTotalItemCount() : 0,
                    'result' => $items
                ]
            );
        }

        return [];
    }

    /**
     * @return array
     */
    private function getVideoViewData($page, $limit, $paginator)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em
            ->getRepository('ApplicationSonataMediaBundle:Media')
            ->createQueryBuilder('v')
            ->where('v.enabled = true')
            ->andWhere('v.providerName IN (:providers)')
            ->setParameter('providers', ['sonata.media.provider.vimeo', 'sonata.media.provider.youtube'])
            ->orderBy('v.createdAt', 'DESC')
            ->getQuery();

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit);

        if ($this->get('request')->isXmlHttpRequest()) {
            $items = [];

            /** @var Media $item */
            foreach ($pagination->getItems() as $item) {
                $provider = $this->get($item->getProviderName());
                $providerUrl = ($item->getProviderName() == 'sonata.media.provider.youtube') ?
                    'http://www.youtube.com/watch?v=' :
                    'http://vimeo.com/';
                $providerFormat = $provider->getFormatName($item, 'medium');

                $url = $providerUrl . $item->getProviderReference();
                $image = $provider->generatePublicUrl($item, $providerFormat);

                $items[] = [
                    'url'   => $url,
                    'image' => $image,
                    'title' => $item->getName()
                ];
            }

            return new JsonResponse(
                [
                    'total'  => (!empty($items)) ? $pagination->getTotalItemCount() : 0,
                    'result' => $items
                ]
            );
        }

        return [];
    }

    /**
     * @return array
     */
    private function getAerophotoViewData($page, $limit, $paginator)
    {
        $repo = $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media');
        $query = $repo->findEnabledByContext('aerophoto');

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit);

        $request = $this->get('request');
        if ($request->isXmlHttpRequest()) {
            $http = $request->getSchemeAndHttpHost();
            $items = [];

            /** @var Media $item */
            foreach ($pagination->getItems() as $item) {
                $provider = $this->get($item->getProviderName());

                $image = $http . $provider->generatePublicUrl($item, $provider->getFormatName($item, 'medium'));
                $itemUrl = $http . $provider->generatePublicUrl($item, $provider->getFormatName($item, 'big'));

                $items[] = [
                    'url'   => $itemUrl,
                    'image' => $image
                ];
            }

            return new JsonResponse(
                [
                    'total'  => (!empty($items)) ? $pagination->getTotalItemCount() : 0,
                    'result' => $items
                ]
            );
        }

        return [];
    }
}
