<?php

namespace Application\Sonata\MediaBundle\Controller;

use Adrenalins\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\MediaBundle\Entity\MediaManager;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryTempMedia;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Application\Sonata\MediaBundle\Form\Type\GalleryType;
use Sonata\MediaBundle\Controller\GalleryController as BaseGallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;

/**
 * Class GalleryController
 *
 * @package Application\Sonata\MediaBundle\Controller
 */
class GalleryController extends BaseGallery
{
    /**
    * @param string $id
    *
    * @return \Symfony\Bundle\FrameworkBundle\Controller\Response
    * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
    */
    public function viewAction($id)
    {
        $gallery = $this->get('sonata.media.manager.gallery')->findOneBy(array(
            'id'      => $id,
            'enabled' => true
        ));

        if (!$gallery) {
            throw new NotFoundHttpException('unable to find the gallery with the id');
        }

        return $this->render(
            'SonataMediaBundle:Gallery:view.html.twig',
            [
                'page'    => $this->getPage(),
                'gallery' => $gallery,
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $gallery = null;
        $galleryId = (int) $request->get('id');

        if ($galleryId) {
            /** @var Gallery $gallery */
            $gallery = $dm
                ->getRepository('ApplicationSonataMediaBundle:Gallery')
                ->findOneBy(
                    [
                        'id'     => $galleryId,
                        'author' => $user
                    ]
                );
        }

        $response = [
            'files' => []
        ];

        if ($request->getMethod() == 'POST') {
            $this->fixFilesArray($_FILES['files']);

            foreach ($_FILES['files'] as $k => $file) {
                $pathinfo = pathinfo($file['tmp_name']);
                $basename = $pathinfo['basename'];

                $uploadedFile = new UploadedFile(
                    $file['tmp_name'],
                    $basename,
                    $file['type'],
                    $file['size'],
                    $file['error']
                );

                $path = 'uploads/media/photo/tmp';
                $webPath = $this->container->getParameter('kernel.root_dir') . '/../web/' . $path;

                $filename = sha1(uniqid(mt_rand(), true)) . '.jpg';

                $file = $uploadedFile->move($webPath, $filename);

                if ($gallery) {
                    /** @var MediaManager $mediaManager */
                    $mediaManager = $this->get('sonata.media.manager.media');

                    $media = new Media;
                    $media->setAuthor($user);
                    $media->setContext('users');
                    $media->setProviderName('sonata.media.provider.image');
                    $media->setBinaryContent($file->getRealPath());

                    $dm->getManager()->persist($media);

                    $galleryHasMedia = new GalleryHasMedia;
                    $galleryHasMedia->setMedia($media);
                    $galleryHasMedia->setGallery($gallery);
                    $galleryHasMedia->setEnabled(true);

                    $dm->getManager()->persist($galleryHasMedia);

                    $mediaManager->save($media, true);

                    $provider = $this->get('sonata.media.provider.image');

                    $url = $request->getSchemeAndHttpHost() .
                        $provider->generatePublicUrl(
                            $media,
                            $provider->getFormatName($media, 'big')
                        );

                    $deleteRoute = 'adrenalins_user_media_gallery_upload_remove';

                    $thumbnailUrl = $request->getSchemeAndHttpHost() .
                        $provider->generatePublicUrl(
                            $media,
                            $provider->getFormatName($media, 'small')
                        );

                } else {
                    $user = $this->getUser();

                    $position = $dm
                        ->getRepository('ApplicationSonataMediaBundle:GalleryTempMedia')
                        ->getLastPositionByAuthor($user->getId());

                    $media = new GalleryTempMedia;
                    $media->setAuthor($user);
                    $media->setFilePath($filename);
                    $media->setPosition(($position) ? $position + 1 : 1);

                    $em = $dm->getManager();
                    $em->persist($media);
                    $em->flush($media);

                    $url = $request->getSchemeAndHttpHost() .'/'. $path .'/'. $filename;
                    $deleteRoute = 'adrenalins_user_media_gallery_upload_tmp_remove';
                    $thumbnailUrl = $url;
                }

                $deleteUrl = $this->generateUrl(
                    $deleteRoute,
                    [
                        'id' => $media->getId()
                    ],
                    true
                );

                $response['files'][] = [
                    'url'          => $url,
                    'thumbnailUrl' => $thumbnailUrl,
                    'name'         => $file->getFilename(),
                    'type'         => $file->getType(),
                    'size'         => $file->getSize(),
                    'deleteUrl'    => $deleteUrl,
                    'deleteType'   => 'DELETE'
                ];
            }

        }

        return JsonResponse::create($response);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $id = (int) $request->get('id');
        $page = null;
        $formErrors = [];

        if ($id) {
            /** @var Gallery $gallery */
            $gallery = $dm
                ->getRepository('ApplicationSonataMediaBundle:Gallery')
                ->findOneBy(
                    [
                        'id'     => $id,
                        'author' => $user
                    ]
                );

            if (!$gallery) {
                throw $this->createNotFoundException('Not found gallery by id - ' . $id);
            }

        } else {
            $page = $this->getPage();

            $gallery = new Gallery;
            $gallery->setAuthor($user);
            $gallery->setEnabled(true);
            $gallery->setIsPublic(true);
            $gallery->setContext('users');
            $gallery->setDefaultFormat('users_medium');
        }

        $form = $this->createForm(new GalleryType(), $gallery);

        if ($request->isMethod("POST")) {
            $form->submit($request);

            if ($form->isValid()) {
                $webPath = $this->container->getParameter('kernel.root_dir') .
                    '/../web/uploads/media/photo/tmp/';

                /** @var MediaManager $mediaManager */
                $mediaManager = $this->get('sonata.media.manager.media');

                $items = $dm
                    ->getRepository('ApplicationSonataMediaBundle:GalleryTempMedia')
                    ->findBy(
                        [
                            'author' => $user
                        ],
                        [
                            'position' => 'ASC'
                        ]
                    );

                foreach ($items as $k => $item) {
                    $fullPath = $webPath . $item->getFilePath();

                    $media = new Media;
                    $media->setAuthor($user);
                    $media->setContext('users');
                    $media->setProviderName('sonata.media.provider.image');
                    $media->setBinaryContent($webPath . $item->getFilePath());

                    $dm->getManager()->persist($media);

                    $galleryHasMedia = new GalleryHasMedia;
                    $galleryHasMedia->setMedia($media);
                    $galleryHasMedia->setGallery($gallery);
                    $galleryHasMedia->setPosition($item->getPosition());
                    $galleryHasMedia->setEnabled(true);

                    $dm->getManager()->persist($galleryHasMedia);
                    $dm->getManager()->remove($item);

                    $mediaManager->save($media, true);
                }

                // if gallery was edit
                if ($id > 0) {
                    $redirectUrl = $this->generateUrl(
                        'adrenalins_user_media_gallery_edit',
                        [
                            'id' => $gallery->getId()
                        ]
                    );

                    $flashMessage = 'user.galleries.manage.flash.edited';

                    $dm->getManager()->flush($gallery);

                } else {
                    $redirectUrl = $this->generateUrl('adrenalins_user_media_gallery_create');
                    $flashMessage = 'user.galleries.manage.flash.created';

                    $gallery->setName(trim($form['name']->getData()));
                    $dm->getManager()->persist($gallery);

                    $user->setGalleriesNum($user->getGalleriesNum() + 1);

                    $dm->getManager()->flush();
                }

                $this->get('session')->getFlashBag()->set('success', $flashMessage);

                return new RedirectResponse($redirectUrl . '#content-table');
            } else {
                $formErrors = $this->getErrorMessages($form);
                $flashMessage = 'user.galleries.manage.flash.error';
                $this->get('session')->getFlashBag()->set('error', $flashMessage);

            }
        }

        /** @var Gallery $galeries */
        $galleries = $dm
            ->getRepository('ApplicationSonataMediaBundle:Gallery')
            ->findBy(
                [
                    'author'  => $user,
                    'enabled' => true
                ],
                [
                    'createdAt' => 'DESC'
                ]
            );

        if ($gallery->getId()) {
            $mediaItems = $dm
                ->getRepository('ApplicationSonataMediaBundle:GalleryHasMedia')
                ->findBy(
                    [
                        'gallery' => $gallery,
                    ],
                    [
                        'createdAt' => 'ASC'
                    ]
                );
        } else {
            $mediaItems = $dm
                ->getRepository('ApplicationSonataMediaBundle:GalleryTempMedia')
                ->findBy(
                    [
                        'author' => $user
                    ],
                    [
                        'createdAt' => 'ASC'
                    ]
                );
        }

        return $this->render(
            'ApplicationSonataMediaBundle:Create:gallery.html.twig',
            [
                'id'         => $id,
                'page'       => $page,
                'user'       => $user,
                'form'       => $form->createView(),
                'galleries'  => $galleries,
                'mediaItems' => $mediaItems,
                'formErrors' => $formErrors
            ]
        );
    }

    /**
    * Get page
    *
    * @return Page
    */
    private function getPage()
    {
        $page = null;

        /** @var PageRepository $pageRepo */
        $repo = $this->getDoctrine()->getRepository('PageBundle:Page');
        $parent = $repo->findOneBySlug('media');
        if ($parent) {
            $page = $repo->findActivePage('photo', $parent);
        }

        return $page;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function deleteUploadAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $id = (int) $request->get('id');
        $isTmp = (int) $request->get('tmp');
        $success = false;
        $repository = sprintf('ApplicationSonataMediaBundle:%s', ($isTmp) ? 'GalleryTempMedia' : 'GalleryHasMedia');

        $galleryMedia = $dm->getRepository($repository)->find($id);

        if ($galleryMedia) {
            $em = $dm->getManager();
            $em->remove($galleryMedia);

            if (!$isTmp) {
                $em->remove($galleryMedia->getMedia());
            }

            $em->flush();

            $success = true;
        }

        return JsonResponse::create(['success' => $success]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');

        /** @var User $user */
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var Gallery $gallery */
        $gallery = $dm
            ->getRepository('ApplicationSonataMediaBundle:Gallery')
            ->findOneBy(
                [
                    'id'     => $id,
                    'author' => $user
                ]
            );

        if (!$gallery) {
            throw $this->createNotFoundException('Not found galleries by id - ' . $id);
        }

        $gallery->setEnabled(false);
        $user->setGalleriesNum($user->getGalleriesNum() - 1);

        $dm->getManager()->flush();

        $this->get('session')->getFlashBag()->set('success', 'user.galleries.manage.flash.deleted');

        return new RedirectResponse($this->generateUrl('adrenalins_user_media_gallery_create'));
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }

    /**
     * @param array $files
     */
    private function fixFilesArray(&$files)
    {
        // a mapping of $_FILES indices for validity checking
        $names = ['name' => 1, 'type' => 1, 'tmp_name' => 1, 'error' => 1, 'size' => 1];

        // iterate over each uploaded file
        foreach ($files as $key => $part) {
            // only deal with valid keys and multiple files
            $key = (string) $key;
            if (isset($names[$key]) && is_array($part)) {
                foreach ($part as $position => $value) {
                    $files[$position][$key] = $value;
                }
                // remove old key reference
                unset($files[$key]);
            }
        }
    }
}
