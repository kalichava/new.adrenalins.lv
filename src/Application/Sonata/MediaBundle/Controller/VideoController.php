<?php

namespace Application\Sonata\MediaBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Application\Sonata\MediaBundle\Form\Type\VideoType;
use Sonata\MediaBundle\Entity\MediaManager;
use Adrenalins\UserBundle\Entity\User;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * Class VideoController
 *
 * @package Adrenalins\FaqBundle\Controller
 */
class VideoController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $id = (int) $request->get('id');
        $page = null;
        $form = $this->createForm(new VideoType());

        if ($id) {
            /** @var Media $media */
            $media = $dm
                ->getRepository('ApplicationSonataMediaBundle:Media')
                ->findOneBy(
                    [
                        'id'     => $id,
                        'author' => $user
                    ]
                );

            if (!$media) {
                throw $this->createNotFoundException('Not found video by id - ' . $id);
            }

            $url = 'http://vimeo.com/';
            if ($media->getProviderName() == 'sonata.media.provider.youtube') {
                $url = 'http://www.youtube.com/watch?v=';
            }

            $url .= $media->getProviderReference();

            $form->setData(['url' => $url]);

        } else {
            $page = $this->getPage();

            $media = new Media;
            $media->setAuthor($user);
            $media->setEnabled(true);
            $media->setContext('video');
        }

        if ($request->isMethod("POST")) {
            $form->submit($request);

            if ($form->isValid()) {
                $url = trim(strip_tags($form['url']->getData()));
                $provider = 'sonata.media.provider.vimeo';
                if (mb_stristr($url, 'youtube')) {
                    $provider = 'sonata.media.provider.youtube';
                }

                /** @var MediaManager $mediaManager */
                $mediaManager = $this->get('sonata.media.manager.media');

                $media->setProviderName($provider);
                $media->setBinaryContent($url);

                if ($media->getCreatedAt()) {
                    $redirectUrl = $this->generateUrl('adrenalins_user_media_video_edit', ['id' => $id]);
                    $flashMessage = 'videos.flash.edited';
                } else {
                    $redirectUrl = $this->generateUrl('adrenalins_user_media_video_create');
                    $flashMessage = 'videos.flash.created';
                }

                try {
                    $mediaManager->save($media);

                    $user->setVideosNum($user->getVideosNum() + 1);

                    $this->get('session')->getFlashBag()->set('success', $flashMessage);

                    return new RedirectResponse($redirectUrl . '#content-table');
                } catch (\Exception $e) {
                    $form->get('url')->addError(
                        new FormError(
                            $this->get('translator')->trans('videos.flash.invalid_url')
                        )
                    );
                }

            } else {
                $flashMessage = 'videos.flash.error';
                $formErrors = $this->getErrorMessages($form);
                $this->get('session')->getFlashBag()->set('error', $flashMessage);
            }
        }

        /** @var Media $videos */
        $videos = $dm
            ->getRepository('ApplicationSonataMediaBundle:Media')
            ->findBy(
                [
                    'author'       => $user,
                    'providerName' => [
                        'sonata.media.provider.vimeo',
                        'sonata.media.provider.youtube'
                    ],
                    'enabled'      => true
                ],
                [
                    'createdAt' => 'DESC'
                ]
            );

        return $this->render(
            'ApplicationSonataMediaBundle:Create:video.html.twig',
            [
                'id'         => $id,
                'page'       => $page,
                'user'       => $user,
                'videos'     => $videos,
                'form'       => $form->createView(),
                'formErrors' => (isset($formErrors) ? $formErrors : null)
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');

        /** @var User $user */
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var Media $event */
        $video = $dm
            ->getRepository('ApplicationSonataMediaBundle:Media')
            ->findOneBy(
                [
                    'id'     => $id,
                    'author' => $user
                ]
            );

        if (!$video) {
            throw $this->createNotFoundException('Not found videos by id - ' . $id);
        }

        $video->setEnabled(false);
        $user->setVideosNum($user->getVideosNum() - 1);

        $dm->getManager()->flush();

        $this->get('session')->getFlashBag()->set('success', 'videos.flash.deleted');

        return new RedirectResponse($this->generateUrl('adrenalins_user_media_video_create'));
    }

    /**
     * @param Form $form
     *
     * @return array
     */
    private function getErrorMessages(Form $form)
    {
        $errors = [];

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }

    /**
    * Get page
    *
    * @return Page
    */
    private function getPage()
    {
        $page = null;

        /** @var PageRepository $pageRepo */
        $repo = $this->getDoctrine()->getRepository('PageBundle:Page');
        $parent = $repo->findOneBySlug('media');
        if ($parent) {
            $page = $repo->findActivePage('video', $parent);
        }

        return $page;
    }
}
