<?php

namespace Application\Sonata\MediaBundle\Admin;

use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Admin\ORM\MediaAdmin as BaseMediaAdmin;

/**
 * Class MediaAdmin
 *
 * @package Application\Sonata\MediaBundle\Admin
 */
class MediaAdmin extends BaseMediaAdmin
{
    protected $maxPerPage = 100;
    protected $translationDomain = 'SonataMediaBundle';

    /**
     * @param Media $object
     *
     * @return mixed|void
     */
    public function preRemove($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        foreach ($object->getGalleryHasMedias() as $hasMedia) {
            $em->remove($hasMedia);
            $em->flush($hasMedia);
        }
    }
}