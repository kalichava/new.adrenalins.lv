<?php

namespace Application\Sonata\MediaBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Admin\GalleryAdmin as BaseGalleryAdmin;

/**
 * Class GalleryAdmin
 *
 * @package Application\Sonata\MediaBundle\Admin
 */
class GalleryAdmin extends BaseGalleryAdmin
{
    protected $maxPerPage = 100;
    protected $translationDomain = 'SonataMediaBundle';

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with($this->trans('Gallery'), array('class' => 'col-md-9'))->end()
            ->with($this->trans('Options'), array('class' => 'col-md-3'))->end()
        ;

        $context = $this->getPersistentParameter('context');

        if (!$context) {
            $context = $this->pool->getDefaultContext();
        }

        $formats = [];
        foreach($this->pool->getContexts() as $globalContext) {
            foreach(array_keys($globalContext['formats']) as $name) {
                $formats[$name] = $name;
            }
        }

        $contexts = array();
        foreach ((array) $this->pool->getContexts() as $contextItem => $format) {
            $contexts[$contextItem] = $contextItem;
        }

        $formMapper
            ->with('Options')
                ->add('author', null, array('label' => 'Author', 'required' => false))
                ->add('context', 'sonata_type_translatable_choice', array(
                    'choices' => $contexts,
                    'catalogue' => 'SonataMediaBundle'
                ))
                ->add('enabled', null, array('required' => false))
                ->add('name')
                ->add('defaultFormat', 'hidden', ['data' => $this->getSubject()->getContext() . '_medium'])
            ->end()
            ->with('Gallery')
                ->add('galleryHasMedias', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                ), array(
                    'edit'              => 'inline',
                    'inline'            => 'table',
                    'sortable'          => 'position',
                    'link_parameters'   => array('context' => $context),
                    'admin_code'        => 'sonata.media.admin.gallery_has_media'
                )
            )
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($gallery)
    {
        $parameters = $this->getPersistentParameters();

        $gallery->setContext($parameters['context']);
        $gallery->setDefaultFormat(sprintf('%s_medium', $parameters['context']));

        // fix weird bug with setter object not being call
        $gallery->setGalleryHasMedias($gallery->getGalleryHasMedias());
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($gallery)
    {
        $gallery->setDefaultFormat(sprintf('%s_medium', $gallery->getContext()));

        // fix weird bug with setter object not being call
        $gallery->setGalleryHasMedias($gallery->getGalleryHasMedias());
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setAuthor(
            $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()
        );

        return $instance;
    }
}