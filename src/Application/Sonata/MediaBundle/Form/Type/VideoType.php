<?php

namespace Application\Sonata\MediaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class VideoType
 *
 * @package Application\Sonata\MediaBundle\Form\Type
 */
class VideoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'url',
                null,
                [
                    'label'    => 'user.videos.manage.form.label_url',
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'user.videos.manage.form.label_url_http'
                    ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'virtual' => false
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'video';
    }
}