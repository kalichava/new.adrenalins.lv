<?php

namespace Application\Sonata\MediaBundle\DataFixtures\ORM;

use Faker;
use Symfony\Component\Finder\Finder;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Sonata\MediaBundle\Entity\MediaManager;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;

/**
 * Class LoadMedia
 *
 * @package ApplicationSonataMediaBundle\DataFixtures\ORM
 */
class LoadMedia extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $path = $this->container->get('kernel')->getRootdir() . '/../web/uploads/media_faker';
        $faker = Faker\Factory::create();

        /** @var MediaManager $mediaManager */
        $mediaManager = $this->container->get('sonata.media.manager.media');

        $directories = array_diff(scandir($path), ['..', '.', '.DS_Store']);

        foreach ($directories as $directory) {

            $gallery = new Gallery;
            $gallery->setName($faker->sentence());
            $gallery->setContext('photo');
            $gallery->setAuthor($this->getReference('admin-user'));
            $gallery->setEnabled(true);
            $gallery->setIsPublic(true);
            $gallery->setDefaultFormat('medium');

            $manager->persist($gallery);
            $manager->flush($gallery);

            $finder = new Finder();
            $finder->files()->in($path .'/'. $directory);
            foreach ($finder as $file) {

                $media = new Media;
                $media->setName($faker->sentence());
                $media->setEnabled(true);
                $media->setContext('photo');
                $media->setProviderName('sonata.media.provider.image');
                $media->setBinaryContent($file);

                $mediaManager->save($media);

                $galleryMedia = new GalleryHasMedia;
                $galleryMedia->setMedia($media);
                $galleryMedia->setGallery($gallery);
                $galleryMedia->setEnabled(true);

                $manager->persist($galleryMedia);
                $manager->flush($galleryMedia);

                unset($galleryMedia, $media);
            }

            unset($gallery, $finder);
        }

        $videos = $this->getMediaVideosData();

        foreach ($videos as $videoData) {
            $media = new Media;
            $media->setContext($videoData['context']);
            $media->setProviderName($videoData['provider']);
            $media->setBinaryContent($videoData['content']);
            $media->setEnabled(true);

            $mediaManager->save($media);
        }
    }

    private function getMediaVideosData()
    {
        return [
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/31746492',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/34675448',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/34675978',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/34697455',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/85660636',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/31746492',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/92446774',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'http://vimeo.com/98843676',
                'provider' => 'sonata.media.provider.vimeo'
            ],
            [
                'context'  => 'video',
                'content'  => 'https://www.youtube.com/watch?v=v0BGAcbP65s',
                'provider' => 'sonata.media.provider.youtube'
            ],
            [
                'context'  => 'video',
                'content'  => 'https://www.youtube.com/watch?v=9pil39t8Qp4',
                'provider' => 'sonata.media.provider.youtube'
            ],
            [
                'context'  => 'video',
                'content'  => 'https://www.youtube.com/watch?v=2YCB8ENw4Sk',
                'provider' => 'sonata.media.provider.youtube'
            ],
            [
                'context'  => 'video',
                'content'  => 'https://www.youtube.com/watch?v=emw1xzYve3w',
                'provider' => 'sonata.media.provider.youtube'
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}