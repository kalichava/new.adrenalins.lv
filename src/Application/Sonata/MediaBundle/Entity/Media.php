<?php

namespace Application\Sonata\MediaBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Adrenalins\UserBundle\Entity\User;
use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\PlaceBundle\Entity\Place;
use Adrenalins\EventsBundle\Entity\Event;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Sonata\MediaBundle\Entity\BaseMedia as BaseMedia;

/**
 * @ORM\Entity(repositoryClass="Application\Sonata\MediaBundle\Entity\Repository\MediaRepository")
 * @ORM\Table(name="media__media")
 * @ORM\HasLifecycleCallbacks
 */
class Media extends BaseMedia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    protected $author;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\NewsBundle\Entity\News")
     */
    protected $news;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\PlaceBundle\Entity\Place")
     */
    protected $places;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\EventsBundle\Entity\Event")
     */
    protected $events;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
     */
    protected $galleries;

    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    protected $isPublic = 1;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->galleries = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param User $author
     *
     * @return Media
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param boolean $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

    /**
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Add news
     *
     * @param News $news
     * @return Media
     */
    public function addNews(News $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param News $news
     */
    public function removeNews(News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return Collection
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Add places
     *
     * @param Place $places
     * @return Media
     */
    public function addPlace(Place $places)
    {
        $this->places[] = $places;

        return $this;
    }

    /**
     * Remove places
     *
     * @param Place $places
     */
    public function removePlace(Place $places)
    {
        $this->places->removeElement($places);
    }

    /**
     * Get places
     *
     * @return Collection
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Add events
     *
     * @param Event $events
     * @return Media
     */
    public function addEvent(Event $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param Event $events
     */
    public function removeEvent(Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @return double
     */
    public function getDuration()
    {
        $format = ($this->length >= 3600) ? 'H:i:s' : 'i:s';

        return gmdate($format, $this->length);
    }

    /**
     * Add galleries
     *
     * @param Gallery $galleries
     * @return Media
     */
    public function addGallery(Gallery $galleries)
    {
        if(!$this->galleries->contains($galleries)) {
            $this->galleries[] = $galleries;
        }

        return $this;
    }

    /**
     * Remove galleries
     *
     * @param Gallery $galleries
     */
    public function removeGallery(Gallery $galleries)
    {
        $this->galleries->removeElement($galleries);
    }

    /**
     * Has galleries
     *
     * @return boolean
     */
    public function hasGalleries()
    {
        return count($this->galleries);
    }

    /**
     * Get galleries
     *
     * @return Collection
     */
    public function getGalleries()
    {
        return $this->galleries;
    }
}
