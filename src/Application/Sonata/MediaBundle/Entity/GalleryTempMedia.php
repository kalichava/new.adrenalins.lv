<?php

namespace Application\Sonata\MediaBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Adrenalins\UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="Application\Sonata\MediaBundle\Entity\Repository\GalleryTempMediaRepository")
 * @ORM\Table(name="media__gallery_temp_media")
 * @ORM\HasLifecycleCallbacks
 */
class GalleryTempMedia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $filePath;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $position;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    protected $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->position = 0;
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return GalleryTempMedia
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return GalleryTempMedia
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set author
     *
     * @param User $author
     *
     * @return GalleryTempMedia
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
