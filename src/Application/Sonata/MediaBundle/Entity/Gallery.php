<?php

namespace Application\Sonata\MediaBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User;
use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\PlaceBundle\Entity\Place;
use Adrenalins\EventsBundle\Entity\Event;
use Sonata\MediaBundle\Entity\BaseGallery as BaseGallery;

/**
 * @ORM\Entity(repositoryClass="Application\Sonata\MediaBundle\Entity\Repository\GalleryRepository")
 * @ORM\Table(name="media__gallery")
 * @ORM\HasLifecycleCallbacks
 */
class Gallery extends BaseGallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    protected $author;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\NewsBundle\Entity\News")
     */
    protected $news;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\PlaceBundle\Entity\Place")
     */
    protected $places;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\EventsBundle\Entity\Event")
     */
    protected $events;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    protected $videos;

    /**
     * @ORM\Column(name="num_comments", type="integer", nullable=false)
     */
    protected $numComments = 0;

    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    protected $isPublic = 1;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Author
     *
     * @param User $author
     *
     * @return Gallery
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get Author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Gets the number of comments
     *
     * @return integer
     */
    public function getNumComments()
    {
        return $this->numComments;
    }

    /**
     * Sets the number of comments
     *
     * @param integer $numComments
     */
    public function setNumComments($numComments)
    {
        $this->numComments = intval($numComments);
    }

    /**
     * Increments the number of comments by the supplied
     * value.
     *
     * @param  integer $by Value to increment comments by
     * @return integer The new comment total
     */
    public function incrementNumComments($by = 1)
    {
        return $this->numComments += intval($by);
    }

    /**
     * @param boolean $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

    /**
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Add news
     *
     * @param News $news
     * @return Gallery
     */
    public function addNews(News $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param News $news
     */
    public function removeNews(News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return Collection
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Add places
     *
     * @param Place $places
     * @return Gallery
     */
    public function addPlace(Place $places)
    {
        $this->places[] = $places;

        return $this;
    }

    /**
     * Remove places
     *
     * @param Place $places
     */
    public function removePlace(Place $places)
    {
        $this->places->removeElement($places);
    }

    /**
     * Get places
     *
     * @return Collection
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Add events
     *
     * @param Event $events
     * @return Gallery
     */
    public function addEvent(Event $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param Event $events
     */
    public function removeEvent(Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add videos
     *
     * @param Media $videos
     *
     * @return Gallery
     */
    public function addVideo(Media $videos)
    {
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos
     *
     * @param Media $videos
     */
    public function removeVideo(Media $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos
     *
     * @return Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }
}
