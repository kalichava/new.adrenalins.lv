<?php

namespace Application\Sonata\MediaBundle\Entity\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\EntityRepository;

/**
 * Class GalleryRepository
 *
 * @package Application\Sonata\MediaBundle\Entity\Repository
 */
class GalleryRepository extends EntityRepository
{
    /**
     * Find active galleries
     *
     * @return Query
     */
    public function findActiveGalleries()
    {
        $qb = $this
            ->createQueryBuilder('g')
            ->where('g.enabled = true')
            ->orderBy('g.createdAt', 'DESC');

        return $qb->getQuery();
    }
}
