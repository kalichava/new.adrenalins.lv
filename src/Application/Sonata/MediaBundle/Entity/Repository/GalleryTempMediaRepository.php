<?php

namespace Application\Sonata\MediaBundle\Entity\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class GalleryTempMediaRepository
 *
 * @package Application\Sonata\MediaBundle\Entity\Repository
 */
class GalleryTempMediaRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function getLastPositionByAuthor($authorId)
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->select('MAX(m.position)')
            ->where('m.author = :authorId')
            ->setParameter('authorId', $authorId);

        return $qb
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }
}
