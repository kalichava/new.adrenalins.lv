<?php

namespace Application\Sonata\MediaBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class MediaRepository
 *
 * @package Application\Sonata\MediaBundle\Entity\Repository
 */
class MediaRepository extends EntityRepository
{
    /**
     * Find media by gallery context
     *
     * @param string $context
     * @return array
     */
    public function findEnabledByContext($context)
    {
        $qb = $this
            ->createQueryBuilder('m')
            ->innerJoin('m.galleryHasMedias', 'mg')
            ->innerJoin('mg.gallery', 'g')
            ->where('g.context = :context')
            ->setParameters(
                [
                    'context' => $context
                ]
            )
            ->orderBy('m.updatedAt', 'DESC');

        return $qb->getQuery();
    }

    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findActiveRandomVideo()
    {
        $qb = $this->createQueryBuilder('m')
            ->select('m, RAND() as HIDDEN r')
            ->where('m.enabled = :enabled')
            ->andWhere('m.providerName IN(:providers)')
            ->andWhere('m.isPublic = :isPublic')
            ->setParameters(
                [
                    'enabled' => true,
                    'providers' => [
                        'sonata.media.provider.vimeo',
                        'sonata.media.provider.youtube',
                    ],
                    'isPublic' => true
                ]
            )
            ->orderBy('r')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
