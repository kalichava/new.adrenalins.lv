<?php

namespace Adrenalins\NewsBundle\Controller;

use Locale;
use IntlDateFormatter;
use Imagine\Gd\Imagine;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Adrenalins\NewsBundle\Form\Type\NewsType;
use Adrenalins\UserBundle\Entity\User;
use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;

/**
 * Class NewsController
 *
 * @package Adrenalins\NewsBundle\Controller
 */
class NewsController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $repo = $dm->getRepository('NewsBundle:News');
        $categoryIds = [];
        $categoryRepo = $dm->getRepository('NewsBundle:NewsCategory');
        $paginator = $this->get('knp_paginator');

        $page = (int) $request->query->get('page', 1);
        $limit = (int) $this->get('adrenalins.settings')->get('news-per-page', 10);
        $isMiniForum = (bool) $request->get('isMiniForum');
        $categories = $request->get('categories');
        $newsType = ($isMiniForum) ? News::TYPE_MINI_FORUM : NEWS::TYPE_PUBLIC;
        $newsTypeLabel = ($newsType == News::TYPE_MINI_FORUM) ? 'miniforum' : 'news';

        $redis = $this->get('snc_redis.default');
        $userId = ($this->getUser()) ? $this->getUser()->getId() : null;

        if (!empty($categories)) {
            foreach ($categories as $categorySlug) {
                $category = $categoryRepo->findOneBySlug($categorySlug);
                if ($category) {
                    $categoryIds[] = $category->getId();
                }
            }

            $query = $repo->findActiveByTypeAndCategories($newsType, $categoryIds);
        } else {
            $query = $repo->findActiveByType($newsType);
        }

        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate(
            $query,
            $page,
            $limit
        );

        if ($request->isXmlHttpRequest()) {
            $data = $pagination->getPaginationData();
            $items = $this->processJsonData($redis, $userId, $pagination->getItems());
            $hideButton = ($page >= $data['last']);

            return new JsonResponse(
                [
                    'news'       => $items,
                    'total'      => $pagination->getTotalItemCount(),
                    'nextPage'   => array_key_exists('next', $data) ? $data['next'] : null,
                    'hideButton' => $hideButton,
                ]
            );
        } else {
            /** @var News $item */
            foreach ($pagination as $item) {
                $commentsKey = sprintf('news:comments:%d', $item->getId());
                if ($redis->hExists($commentsKey, $userId)) {
                    $newsHasNewComments = $redis->hGet($commentsKey, $userId);
                    $item->setHasNewComments($newsHasNewComments);
                }
            }
        }

        $categories = $categoryRepo->findAll();

        return $this->render(
            'NewsBundle::index.html.twig',
            [
                'page'             => $this->getPage($newsTypeLabel),
                'items'            => $pagination,
                'categories'       => $categories,
                'isMiniForum'      => $isMiniForum,
                'activeCategories' => $categoryIds
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $slug = trim(strip_tags($request->get('slug')));
        $isMiniForum = (bool) $request->get('isMiniForum');
        $newsTypeLabel = ($isMiniForum) ? 'miniforum' : 'news';

        /** @var News $news */
        $news = $dm
            ->getRepository('NewsBundle:News')
            ->findOneBy(
                [
                    'slug'     => $slug,
                    'isActive' => true
                ]
            );

        if (!$news) {
            throw $this->createNotFoundException('Not found news by slug - ' . $slug);
        }

        $user = $this->getUser();
        if ($user) {
            $this->markNewsAsReadForUser($user->getId(), $news->getId());
        }

        return $this->render(
            'NewsBundle::show.html.twig',
            [
                'page'        => $this->getPage($newsTypeLabel),
                'news'        => $news,
                'isMiniForum' => $isMiniForum
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $page = null;
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $id = (int) $request->get('id');
        $type = ((bool) $request->get('isMiniForum')) ? News::TYPE_MINI_FORUM : NEWS::TYPE_PUBLIC;
        $route = ($type == News::TYPE_MINI_FORUM) ? 'miniforum' : 'news';

        if ($id) {
            /** @var News $news */
            $news = $dm
                ->getRepository('NewsBundle:News')
                ->findOneBy(
                    [
                        'id'     => $id,
                        'author' => $user
                    ]
                );

            if (!$news) {
                throw $this->createNotFoundException('Not found news by id - ' . $id);
            }
        } else {
            $page = $this->getPage($route);

            $news = new News();
            $news->setType($type);
            $news->setAuthor($user);
            $news->setIsActive(true);
            $news->setCreatedAt(new \Datetime());
        }

        $form = $this->createForm(new NewsType($route), $news);

        if ($request->isMethod("POST")) {
            $form->submit($request);

            if ($form->isValid()) {
                $manager = $dm->getManager();

                $this->uploadImages($news);

                if ($news->getId()) {
                    $redirectUrl = $this->generateUrl(sprintf('adrenalins_user_%s_edit', $route), ['id' => $id]);
                    $flashMessage = 'news.flash.edited';
                } else {
                    $redirectUrl = $this->generateUrl(sprintf('adrenalins_user_%s_create', $route));
                    $flashMessage = 'news.flash.created';
                }

                if ($news->getType() == NEWS::TYPE_PUBLIC) {
                    $user->setNewsNum($user->getNewsNum() + 1);
                }

                $manager->persist($news);
                $manager->flush();

                $this->get('session')->getFlashBag()->set('success', $flashMessage);

                return new RedirectResponse($redirectUrl . '#content-table');
            } else {
                $flashMessage = 'news.flash.error';
                $formErrors = $this->getErrorMessages($form);
                $this->get('session')->getFlashBag()->set('error', $flashMessage);
            }

        }

        /** @var News $news */
        $news = $dm
            ->getRepository('NewsBundle:News')
            ->findBy(
                [
                    'type'     => $type,
                    'author'   => $user,
                    'isActive' => true
                ],
                [
                    'createdAt' => 'DESC'
                ]
            );

        return $this->render(
            'NewsBundle:Create:index.html.twig',
            [
                'id'    => $id,
                'page'  => $page,
                'user'  => $user,
                'news'  => $news,
                'form'  => $form->createView(),
                'route' => $route,
                'formErrors' => (isset($formErrors) ? $formErrors : null)
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');

        /** @var User $user */
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var News $news */
        $news = $dm
            ->getRepository('NewsBundle:News')
            ->findOneBy(
                [
                    'id'     => $id,
                    'author' => $user
                ]
            );

        if (!$news) {
            throw $this->createNotFoundException('Not found news by id - ' . $id);
        }

        $news->setIsActive(false);

        if ($news->getType() == NEWS::TYPE_PUBLIC) {
            $user->setNewsNum($user->getNewsNum() - 1);
        }

        $dm->getManager()->flush();

        $type = ((bool) $request->get('isMiniForum')) ? News::TYPE_MINI_FORUM : NEWS::TYPE_PUBLIC;
        $route = ($type == News::TYPE_MINI_FORUM) ? 'miniforum' : 'news';
        $redirectUrl = $this->generateUrl(sprintf('adrenalins_user_%s_create', $route));

        $this->get('session')->getFlashBag()->set('notice', 'news.flash.deleted');

        return new RedirectResponse($redirectUrl);
    }

    /**
     * Process news objects to json response
     *
     * @param \Redis  $redis  Redis
     * @param integer $userId Current user id
     * @param array   $news   News objects
     *
     * @return array
     */
    private function processJsonData($redis, $userId, $news)
    {
        $items = [];

        if ($news) {
            $formatter = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::LONG, IntlDateFormatter::NONE);
            $translator = $this->get('translator');
            $labelVideos = $translator->trans('news.index.item.video');
            $labelGalleries = $translator->trans('news.index.item.photos');

            /** @var News $item */
            foreach ($news as $item) {
                $route = $item->getType() ? 'adrenalins_miniforum_show' : 'adrenalins_news_show';
                $image = $item->getImageThumb();

                if (!$image) {
                    $image = sprintf(
                        '/img/300x200/no-image-%s.jpg',
                        ($item->getType() == News::TYPE_PUBLIC) ? 'news' : 'miniforum'
                    );
                }

                $itemUrl = $this->generateUrl($route, ['slug' => $item->getSlug()], true);
                $createdAt = $formatter->format($item->getCreatedAt());

                $commentsKey = sprintf('news:comments:%d', $item->getId());
                if ($redis->hExists($commentsKey, $userId)) {
                    $newsHasNewComments = $redis->hGet($commentsKey, $userId);
                    $item->setHasNewComments($newsHasNewComments);
                }

                $items[] = [
                    'url'              => $itemUrl,
                    'title'            => $item->getTitle(),
                    'imageThumb'       => $image,
                    'createdAt'        => $createdAt,
                    'shortDescription' => $item->getShortDescription(),
                    'hasVideos'        => $item->hasVideos(),
                    'numComments'      => $item->getNumComments(),
                    'hasGalleries'     => $item->hasGalleries(),
                    'hasNewComments'   => $item->getHasNewComments(),
                    'labelVideos'      => $labelVideos,
                    'labelGalleries'   => $labelGalleries
                ];
            }

        }

        return $items;
    }

    /**
     * Mark news item was readed by current user
     *
     * @param integer $userId User Id
     * @param integer $newsId News Id
     *
     * @return boolean
     */
    private function markNewsAsReadForUser($userId, $newsId)
    {
        $redis = $this->get('snc_redis.default');
        $redis->hSetNx(sprintf('news:%d', $newsId), $userId, true);
        $redis->hSetNx(sprintf('news:comments:%d', $newsId), $userId, false);
    }

    /**
     * @param $object
     */
    private function uploadImages(News $object)
    {
        $imageParameters = $this->container->getParameter('adrenalins.news.image');
        $thumbs = $imageParameters['thumbs'];
        $dimensions = $imageParameters['dimensions'];

        // 1. Upload original image
        if (false !== $image = $this->uploadFile($object)) {
            // 3. Update entity fields
            $object->setImage($image);
            $object->setFile(null);

            // 4. Check if thumbs are needed
            foreach ($thumbs as $imageThumb) {
                $setter = 'setImage' . ucfirst($imageThumb);

                $fullImagePath = $object->getRootWebPath() . $image;
                $imagine = new Imagine();
                $original = $imagine->open($fullImagePath);
                $thumb = $imagine->open($fullImagePath);
                $thumbPath = $object->getRootWebPath() . $object->getFullUploadPath()
                    . DIRECTORY_SEPARATOR . $imageThumb . '-' . basename($image);
                $thumbName = $object->getFullUploadPath() . '/' . $imageThumb . '-' . basename($image);

                // 4a. Check if resize is needed
                if (array_key_exists($imageThumb, $dimensions)) {
                    if (array_key_exists('width', $dimensions[$imageThumb]) &&
                        $original->getSize()->getWidth() > $dimensions[$imageThumb]['width']) {
                        $thumb->resize($thumb->getSize()->widen($dimensions[$imageThumb]['width']));
                    }

                    if (array_key_exists('height', $dimensions[$imageThumb]) &&
                        $thumb->getSize()->getHeight() > $dimensions[$imageThumb]['height']) {
                        $thumb->resize($thumb->getSize()->heighten($dimensions[$imageThumb]['height']));
                    }
                }

                // 4b. Save thumbs
                $thumb->save($thumbPath);
                $object->$setter($thumbName);
            }
        }
    }

    /**
     * Upload file
     *
     * @param \Adrenalins\NewsBundle\Entity\News $object
     * @param string                             $uploadPath
     * @param string                             $fileName
     *
     * @return bool|string
     */
    protected function uploadFile($object, $uploadPath = null, $fileName = null)
    {
        $file = $object->getFile();
        if (!$file instanceof UploadedFile) {
            return false;
        }

        if (null === $fileName) {
            if (null === $ext = $file->guessExtension()) {
                $ext = $file->getExtension();
            }

            $fileName = sha1(uniqid(mt_rand(), true));

            if (null !== $ext) {
                $fileName .= '.' . $ext;
            }
        }

        $fullUploadPath = $object->getFullUploadPath($uploadPath);
        $rootPath = $object->getRootWebPath();

        $movePath = $rootPath . $fullUploadPath;

        $file->move($movePath, $fileName);

        return $fullUploadPath . '/' . $fileName;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }

    /**
     * Get page by slug
     *
     * @param string $pageSlug Slug
     *
     * @return Page
     */
    private function getPage($pageSlug)
    {
        /** @var PageRepository $pageRepo */
        $pageRepo = $this->getDoctrine()->getRepository('PageBundle:Page');

        /** @var Page $page */
        $page = $pageRepo->findActivePage($pageSlug, null);

        return $page;
    }
}
