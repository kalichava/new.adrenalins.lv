<?php

namespace Adrenalins\NewsBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Adrenalins\NewsBundle\Entity\NewsCategoryTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="news_category", indexes={
 *      @ORM\Index(name="news_category_key_idx", columns={"slug"})
 * })
 * @Gedmo\TranslationEntity(class="Adrenalins\NewsBundle\Entity\NewsCategoryTranslation")
 * @UniqueEntity("slug")
 */
class NewsCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true, length=255, nullable=false)
     * @Gedmo\Slug(fields={"title"}, separator="_", updatable=false, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\Length(
     *     max="255"
     * )
     */
    protected $title;

    /**
     * @ORM\OneToMany(targetEntity="NewsCategoryTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getTitle();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Adds translations.
     *
     * @param NewsCategoryTranslation $translation
     *
     * @return $this
     */
    public function addTranslation(NewsCategoryTranslation $translation)
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setObject($this);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param NewsCategoryTranslation $translations
     */
    public function removeTranslation(NewsCategoryTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}