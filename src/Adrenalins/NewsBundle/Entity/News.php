<?php

namespace Adrenalins\NewsBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Adrenalins\UserBundle\Entity\User;
use Adrenalins\PlaceBundle\Entity\Place;
use Adrenalins\EventsBundle\Entity\Event;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\MediaBundle\Entity\Gallery;

/**
 * @ORM\Entity(repositoryClass="Adrenalins\NewsBundle\Entity\Repository\NewsRepository")
 * @ORM\Table(name="news", indexes={
 *      @ORM\Index(name="news_key_idx", columns={"slug"})
 * })
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("slug")
 */
class News
{
    CONST THREAD_PREFIX = 'news-';
    CONST TYPE_PUBLIC = 0;
    CONST TYPE_MINI_FORUM = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    protected $type = News::TYPE_PUBLIC;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @Assert\NotNull()
     */
    protected $title;

    /**
     * @ORM\Column(type="string", unique=true, length=255, nullable=false)
     * @Gedmo\Slug(fields={"title"}, separator="_", updatable=false, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="short_description", type="string", length=200)
     *
     * @Assert\NotNull()
     * @Assert\Length(
     *     min = "1",
     *     max = "200",
     * )
     */
    protected $shortDescription;

    /**
     * @ORM\Column(name="content", type="text")
     *
     * @Assert\NotNull()
     */
    protected $content;

    /**
     * @ORM\Column(name="num_comments", type="integer", nullable=false)
     */
    protected $numComments = 0;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(name="image_thumb", type="string", length=255, nullable=true)
     */
    protected $imageThumb;

    /**
     * @ORM\Column(name="image_big_thumb", type="string", length=255, nullable=true)
     */
    protected $imageBigThumb;

    /**
     * @ORM\Column(name="image_big_image", type="string", length=255, nullable=true)
     */
    protected $imageBigImage;

    /**
     * @ORM\Column(name="published_at", type="datetime")
     */
    protected $publishedAt;

    /**
     * @ORM\Column(name="commented_at", type="datetime")
     */
    protected $commentedAt;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * Variable for file uploading
     *
     * @var UploadedFile|null
     *
     * @Assert\File(
     *     maxSize = "8M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $file;

    /**
     * Variable for redis check news is seen or not
     *
     * @var boolean
     */
    protected $isSeen = false;

    /**
     * Variable for redis check news has new comments
     *
     * @var boolean
     */
    protected $hasNewComments = true;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    protected $author;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    protected $videos;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
     */
    protected $galleries;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\EventsBundle\Entity\Event")
     */
    protected $events;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\PlaceBundle\Entity\Place")
     */
    protected $places;

    /**
     * @ORM\ManyToMany(targetEntity="NewsCategory")
     */
    protected $categories;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->places = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return str_replace('\"', '"', $this->title);
    }

    /**
     * Add category
     *
     * @param NewsCategory $category
     *
     * @return News
     */
    public function addCategory(NewsCategory $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param NewsCategory $category
     */
    public function removeCategory(NewsCategory $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set Author
     *
     * @param User $author
     *
     * @return News
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get Author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * Get shortDescription
     *
     * @return mixed
     */
    public function getShortDescription()
    {
        return str_replace('\"', '"', $this->shortDescription);
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return mixed
     */
    public function getContent()
    {
        return str_replace('\"', '"', $this->content);
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return News
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image thumb
     *
     * @param string $image
     *
     * @return News
     */
    public function setImageThumb($image)
    {
        $this->imageThumb = $image;

        return $this;
    }

    /**
     * Get image thumb
     *
     * @return string
     */
    public function getImageThumb()
    {
        return $this->imageThumb;
    }

    /**
     * Set image big thumb
     *
     * @param string $image
     *
     * @return News
     */
    public function setImageBigThumb($image)
    {
        $this->imageBigThumb = $image;

        return $this;
    }

    /**
     * Get image thumb
     *
     * @return string
     */
    public function getImageBigThumb()
    {
        return $this->imageBigThumb;
    }

    /**
     * Set big image (x1600)
     *
     * @param string $image
     *
     * @return News
     */
    public function setImageBigImage($image)
    {
        $this->imageBigImage = $image;

        return $this;
    }

    /**
     * Get big image (x1600)
     *
     * @return string
     */
    public function getImageBigImage()
    {
        return $this->imageBigImage;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPublishedAt()
    {
        $this->publishedAt = new \Datetime();

        if(!$this->commentedAt) {
            $this->setCommentedAt($this->publishedAt);
        }
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set commentedAt
     *
     * @param \Datetime $commentedAt
     *
     * @return News
     */
    public function setCommentedAt($commentedAt)
    {
        $this->commentedAt = $commentedAt;

        return $this;
    }

    /**
     * Get commentedAt
     *
     * @return \DateTime
     */
    public function getCommentedAt()
    {
        return $this->commentedAt;
    }

    /**
     * Set createdAt
     *
     * @param \Datetime $createdAt
     *
     * @return News
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Gets the number of comments
     *
     * @return integer
     */
    public function getNumComments()
    {
        return $this->numComments;
    }

    /**
     * Sets the number of comments
     *
     * @param integer $numComments
     */
    public function setNumComments($numComments)
    {
        $this->numComments = intval($numComments);
    }

    /**
     * Increments the number of comments by the supplied
     * value.
     *
     * @param  integer $by Value to increment comments by
     *
     * @return integer The new comment total
     */
    public function incrementNumComments($by = 1)
    {
        return $this->numComments += intval($by);
    }

    /**
     * Sets isSeen
     *
     * @param boolean $isSeen
     */
    public function setIsSeen($isSeen)
    {
        $this->isSeen = $isSeen;
    }

    /**
     * Get isSeen.
     *
     * @return boolean
     */
    public function getIsSeen()
    {
        return $this->isSeen;
    }

    /**
     * Sets hasNewComments
     *
     * @param boolean $hasNewComments
     */
    public function setHasNewComments($hasNewComments)
    {
        $this->hasNewComments = $hasNewComments;
    }

    /**
     * Get hasNewComments.
     *
     * @return boolean
     */
    public function getHasNewComments()
    {
        return $this->hasNewComments;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile|null $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get uplaod path for image files relative to root upload path
     *
     * @return string
     */
    public function getUploadPath()
    {
        return '/news';
    }

    /**
     * Get root web path
     *
     * @return string
     */
    public function getRootWebPath()
    {
        return realpath(__DIR__ . '/../../../../web');
    }

    /**
     * Get full upload path relative to root web path
     *
     * @return string
     */
    public function getFullUploadPath()
    {
        return '/uploads' . $this->getUploadPath();
    }

    /**
     * Add galleries
     *
     * @param Gallery $galleries
     * @return News
     */
    public function addGallery(Gallery $galleries)
    {
        if(!$this->galleries->contains($galleries)) {
            $this->galleries[] = $galleries;
        }

        return $this;
    }

    /**
     * Remove galleries
     *
     * @param Gallery $galleries
     */
    public function removeGallery(Gallery $galleries)
    {
        $this->galleries->removeElement($galleries);
    }

    /**
     * Has galleries
     *
     * @return boolean
     */
    public function hasGalleries()
    {
        return count($this->galleries);
    }

    /**
     * Get galleries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * Add videos
     *
     * @param Media $videos
     * @return News
     */
    public function addVideo($videos = null)
    {
        if($videos) {
            if(!$this->videos->contains($videos)) {
                $this->videos[] = $videos;
            }
        }

        return $this;
    }

    /**
     * Add videos
     *
     * @param Media $videos
     * @return News
     */
    public function setVideos($videos = null)
    {
        if($videos) {
            if(!$this->videos->contains($videos)) {
                $this->videos[] = $videos;
            }
        }

        return $this;
    }

    /**
     * Remove videos
     *
     * @param Media $videos
     */
    public function removeVideo(Media $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Has videos
     *
     * @return boolean
     */
    public function hasVideos()
    {
        return count($this->videos);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add events
     *
     * @param Event $events
     * @return News
     */
    public function addEvent(Event $events)
    {
        if(!$this->events->contains($events)) {
            $this->events[] = $events;
        }

        return $this;
    }

    /**
     * Remove events
     *
     * @param Event $events
     */
    public function removeEvent(Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add places
     *
     * @param Place $places
     * @return News
     */
    public function addPlace(Place $places)
    {
        if(!$this->places->contains($places)) {
            $this->places[] = $places;
        }

        return $this;
    }

    /**
     * Remove places
     *
     * @param Place $places
     */
    public function removePlace(Place $places)
    {
        $this->places->removeElement($places);
    }

    /**
     * Get places
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlaces()
    {
        return $this->places;
    }
}
