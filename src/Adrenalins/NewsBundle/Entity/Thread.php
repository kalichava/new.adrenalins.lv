<?php

namespace Adrenalins\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Thread as BaseThread;

/**
 * @ORM\Entity
 * @ORM\Table(name="comments_thread")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Thread extends BaseThread
{
    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

//    /**
//     * @ORM\OneToOne(targetEntity="News", inversedBy="thread", cascade={"persist", "remove"})
//     * @ORM\JoinColumn(name="news_id", referencedColumnName="id")
//     */
//    protected $news;
}