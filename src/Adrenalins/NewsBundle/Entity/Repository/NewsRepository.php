<?php

namespace Adrenalins\NewsBundle\Entity\Repository;

use Adrenalins\NewsBundle\Entity\News;
use Doctrine\ORM\EntityRepository;

/**
 * Class NewsRepository
 *
 * @package Adrenalins\NewsBundle\Entity\Repository
 */
class NewsRepository extends EntityRepository
{
    /**
     * Find active news by type
     * News::TYPE_MINI_FORUM or NEWS::TYPE_PUBLIC
     *
     * @param integer $type
     *
     * @return array
     */
    public function findActiveByType($type)
    {
        $qb = $this
            ->createQueryBuilder('n')
            ->where('n.isActive = true')
            ->andWhere('n.type = :type')
            ->setParameter('type', (int) $type)
            ->orderBy(($type == News::TYPE_MINI_FORUM) ? 'n.commentedAt' : 'n.createdAt', 'DESC');

        return $qb->getQuery();
    }

    /**
     * Find active news by type
     * News::TYPE_MINI_FORUM or NEWS::TYPE_PUBLIC
     *
     * @param integer $type
     * @param integer $limit
     *
     * @return array
     */
    public function findActiveMainByTypeAndLimit($type, $limit)
    {
        $qb = $this
            ->createQueryBuilder('n')
            ->where('n.isActive = true')
            ->andWhere('n.type = :type')
            ->setParameter('type', (int) $type)
            ->orderBy(($type == News::TYPE_MINI_FORUM) ? 'n.commentedAt' : 'n.createdAt', 'DESC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find active news by category and type
     * News::TYPE_MINI_FORUM or NEWS::TYPE_PUBLIC
     *
     * @param integer $type        Type Id
     * @param array   $categoryIds Category Ids
     *
     * @return array
     */
    public function findActiveByTypeAndCategories($type, $categoryIds)
    {
        $qb = $this
            ->createQueryBuilder('n')
            ->leftJoin('n.categories', 'cc')
            ->where('n.isActive = true')
            ->andWhere('n.type = :type')
            ->andWhere('cc.id IN (:categories)')
            ->setParameters(
                [
                    'type'       => (int) $type,
                    'categories' => $categoryIds,
                ]
            )
            ->orderBy('n.createdAt', 'DESC');

        return $qb->getQuery();
    }

}
