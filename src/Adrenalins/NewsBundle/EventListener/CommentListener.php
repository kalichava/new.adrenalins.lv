<?php

namespace Adrenalins\NewsBundle\EventListener;

use Doctrine\ORM\EntityManager;
use FOS\CommentBundle\Event\CommentEvent;
use FOS\CommentBundle\Model\CommentManager;
use Snc\RedisBundle\Client\Phpredis\Client as RedisClient;

/**
 * Class CommentListener
 *
 * @package Adrenalins\NewsBundle\EventListener
 */
class CommentListener
{
    /**
     * @var \Redis
     */
    private $redis;

    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var CommentManager
     */
    private $commentManager;

    /**
     * Constructor.
     *
     * @param CommentManager $commentManager
     * @param EntityManager  $entityManager
     * @param RedisClient    $redis
     */
    public function __construct(CommentManager $commentManager, EntityManager $entityManager, $redis)
    {
        $this->redis = $redis;
        $this->doctrine = $entityManager;
        $this->commentManager = $commentManager;
    }

    /**
     * Increase the thread comments number
     *
     * @param CommentEvent $event
     */
    public function onCommentPersist(CommentEvent $event)
    {
        $comment = $event->getComment();
        $threadId = $comment->getThread()->getId();

        list($name, $id) = explode('-', $threadId);

        $repoName = ($name == 'gallery') ?
            'ApplicationSonataMediaBundle:Gallery' :
            sprintf('%sBundle:%s', ucfirst($name), ucfirst($name));

        $object = $this->doctrine->getRepository($repoName)->find($id);

        if ($object) {

            // we should remove saved seen comments for users
            if ($name == 'news') {
                $object->setCommentedAt(new \DateTime('now'));
                $this->redis->del(sprintf('news:comments:%d', $id));

                if($comment->getAuthor()) {
                    $this->redis->hSet(sprintf('news:comments:%d', $id), $comment->getAuthor()->getId(), false);
                }
            }

            $object->incrementNumComments(1);

            $this->doctrine->persist($object);
            $this->doctrine->flush($object);
        }

    }
}