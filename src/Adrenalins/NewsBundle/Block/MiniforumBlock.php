<?php

namespace Adrenalins\NewsBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Adrenalins\SettingsBundle\Core\SettingsLoader as Settings;
use Doctrine\ORM\EntityManager;
use Snc\RedisBundle\Client\Phpredis\Client as RedisClient;
use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\UserBundle\Entity\User;

/**
 * Class MiniforumBlock
 *
 * @package Adrenalins
 */
class MiniforumBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param EntityManager   $entityManager
     * @param Settings        $settings
     * @param RedisClient     $redis
     */
    public function __construct(
        EngineInterface $templating,
        EntityManager $entityManager,
        Settings $settings,
        $redis
    ) {
        $this->redis = $redis;
        $this->settings = $settings;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $limit = (int) $this->settings->get('miniforum-block-limit', 7);
        $items = [];
        $settings = $blockContext->getSetting('attr');

        if (array_key_exists('user', $settings) && $settings['user'] instanceof User) {
            /** @var \Redis $redis */
            $redis = $this->redis;
            $userId = $settings['user']->getId();

            $news = $this
                ->entityManager
                ->getRepository('NewsBundle:News')
                ->findActiveMainByTypeAndLimit(News::TYPE_MINI_FORUM, $limit);

            /** @var News $item */
            foreach ($news as $item) {
                $newsIsSeen = $redis->hExists(sprintf('news:%d', $item->getId()), $userId);
                $item->setIsSeen($newsIsSeen);

                $commentsKey = sprintf('news:comments:%d', $item->getId());
                if($redis->hExists($commentsKey, $userId)) {
                    $newsHasNewComments = $redis->hGet($commentsKey, $userId);
                    $item->setHasNewComments($newsHasNewComments);
                }

                $items[] = $item;
            }
        }

        return $this->renderResponse(
            $blockContext->getTemplate(),
            [
                'block' => $blockContext->getBlock(),
                'items' => $items
            ],
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'NewsBundle:Block:miniforum.html.twig'
            ]
        );
    }
}
