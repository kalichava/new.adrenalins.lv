<?php

namespace Adrenalins\NewsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Adrenalins\NewsBundle\Entity\NewsCategory;
use Adrenalins\NewsBundle\Entity\NewsCategoryTranslation;

/**
 * Class LoadNewsCategory
 *
 * @package Adrenalins\NewsBundle\DataFixtures\ORM
 */
class LoadNewsCategory extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $types = [
            'Skola',
            'Klubs',
            'Tandēmi',
            'Ekipējums',
            'Sacensības',
            'Paramotori'
        ];

        $locales = ['ru', 'en'];

        $t = 1;
        foreach ($types as $typeTitle) {

            $newsCategory = new NewsCategory();
            $newsCategory->setTitle($typeTitle);

            foreach ($locales as $locale) {

                $trans = new NewsCategoryTranslation();
                $trans->setField('title');
                $trans->setContent($typeTitle);
                $trans->setLocale($locale);

                $newsCategory->addTranslation($trans);

                $manager->persist($newsCategory);
            }

            $manager->flush();

            $this->setReference('news-category-' . $t, $newsCategory);

            $t++;
        }

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}