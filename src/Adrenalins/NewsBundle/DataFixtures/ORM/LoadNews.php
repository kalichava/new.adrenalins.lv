<?php

namespace Adrenalins\NewsBundle\DataFixtures\ORM;

use Faker\Factory;
use Imagine\Gd\Imagine;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Adrenalins\NewsBundle\Entity\News;

/**
 * Class LoadNews
 *
 * @package Adrenalins\NewsBundle\DataFixtures\ORM
 */
class LoadNews extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imagine = new Imagine();

        $faker = Factory::create();

        $dir = '/uploads/news';
        $imagePath = realpath(__DIR__) . '/../../../../../web' . $dir;
        $imageConfig = $this->container->getParameter('Adrenalins.news.image');
        $fixturesCount = 20;

        while ($fixturesCount) {

            $news = new News();
            $news->setTitle($faker->sentence());
            $news->setContent('<p>' . $faker->text(mt_rand(400, 4000)) . '</p>');
            $news->setShortDescription('<p>' . $faker->text(mt_rand(100, 200)) . '</p>');
            $news->setAuthor($this->getReference('user-' . $fixturesCount));
            $news->setPublishedAt(new \DateTime('now'));
            $news->setType((int) rand(0, 1));
            $news->setIsActive(true);

            $image = $faker->image($imagePath, 1600, 1200);
            if ($image) {
                foreach ($imageConfig['dimensions'] as $filePrefix => $size) {
                    $thumb = $imagine->open($image);
                    $thumbName = $filePrefix . '-' . basename($image);
                    $thumbPath = $imagePath . '/' . $thumbName;

                    $imgSize = $thumb->getSize();
                    if ($imgSize->getHeight() > $imgSize->getWidth()) {
                        $resize = $imgSize->heighten($size['width']);
                    } else {
                        $resize = $imgSize->widen($size['width']);
                    }

                    $thumb->resize($resize);
                    $thumb->save($thumbPath, array('jpeg_quality' => 100));

                    $setter = 'setImage' . ucfirst($filePrefix);
                    $news->$setter($dir . '/' . $thumbName);
                }
            }

            $category = $this->getReference('news-category-' . mt_rand(1, 6));
            $news->addCategory($category);

            $manager->persist($news);
            $manager->flush($news);

            $fixturesCount--;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}