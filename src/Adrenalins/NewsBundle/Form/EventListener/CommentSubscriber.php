<?php

namespace Adrenalins\NewsBundle\Form\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\NewsBundle\Entity\Thread;

/**
 * Class CommentSubscriber
 *
 * @package Adrenalins\NewsBundle\Form\EventListener
 */
class CommentSubscriber implements EventSubscriberInterface
{
    /** @var EntityManager */
    protected $em;

    /** @var integer */
    protected $newsId;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::SUBMIT     => 'submit',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $this->newsId = $data['news'];
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        $data = $event->getData();

//        /** @var Thread $thread */
//        $thread = $this
//            ->em
//            ->getRepository('Adrenalins\NewsBundle\Entity\Thread')
//            ->find(News::THREAD_PREFIX . $this->newsId);
//
//        $thread->incrementNumComments();
//        $this->em->persist($thread);
//
//        $data->setThread($thread);

    }
}