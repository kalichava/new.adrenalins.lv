<?php

namespace Adrenalins\NewsBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class NewsType
 *
 * @package Adrenalins\NewsBundle\Form\Type
 */
class NewsType extends AbstractType
{
    protected $type;

    /**
     * @param integer $type Miniforum or News type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                null,
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'user.news.manage.form.label_title'
                    ]
                ]
            )
            ->add(
                'shortDescription',
                'textarea',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'rows'        => 5,
                        'maxlength'   => 200,
                        'placeholder' => 'user.news.manage.form.label_short_description'
                    ]
                ]
            )
            ->add(
                'content',
                'textarea',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'rows'       => 20,
                        'class'      => 'tinymce',
                        'data-theme' => 'simple'
                    ]
                ]
            )
            ->add(
                'file',
                'file',
                [
                    'label'    => 'user.news.manage.form.label_image',
                    'required' => false
                ]
            )
            ->add(
                'galleries',
                null,
                [
                    'label'    => 'user.news.manage.form.label_galleries',
                    'required' => false,
                    'attr'     => [
                        'class'    => 'chosen-select',
                        'multiple' => 'multiple'
                    ]
                ]
            )
            ->add(
                'videos',
                'entity',
                [
                    'label'         => 'user.news.manage.form.label_videos',
                    'attr'          => [
                        'class'    => 'chosen-select',
                        'multiple' => 'multiple'
                    ],
                    'required'      => false,
                    'class'         => 'ApplicationSonataMediaBundle:Media',
                    'query_builder' => function (EntityRepository $repository) {
                        return $repository
                            ->createQueryBuilder('m')
                            ->where('m.providerName IN (:mediaProviders)')
                            ->setParameter(
                                'mediaProviders',
                                [
                                    'sonata.media.provider.vimeo',
                                    'sonata.media.provider.youtube'
                                ]
                            );
                    }
                ]
            )
            ->add(
                'places',
                null,
                [
                    'label'    => 'user.news.manage.form.label_places',
                    'required' => false,
                    'attr'     => [
                        'class'    => 'chosen-select',
                        'multiple' => 'multiple'
                    ]
                ]
            )
            ->add(
                'events',
                null,
                [
                    'label'    => 'user.news.manage.form.label_events',
                    'required' => false,
                    'attr'     => [
                        'class'    => 'chosen-select',
                        'multiple' => 'multiple'
                    ]
                ]
            );

        if($this->type != 'miniforum') {
            $builder->add(
                'categories',
                null,
                [
                    'label'    => 'user.news.manage.form.label_categories',
                    'required' => false,
                    'attr'     => [
                        'size' => 6
                    ]
                ]
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Adrenalins\NewsBundle\Entity\News'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'news';
    }
}