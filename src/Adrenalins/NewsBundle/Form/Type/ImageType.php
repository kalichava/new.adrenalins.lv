<?php

namespace Adrenalins\NewsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Image form type
 */
class ImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'path' => null,
                'file_path' => null,
                'max_width' => null,
                'max_height' => null,
                'min_width' => null,
                'min_height' => null,
                'width' => null,
                'height' => null,
                'data_class' => null
            ));
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_merge(
            [
                'max_width'  => $options['max_width'],
                'max_height' => $options['max_height'],
                'min_width'  => $options['min_width'],
                'min_height' => $options['min_height'],
                'width'      => $options['width'],
                'height'     => $options['height'],
                'img_style'  => $this->generateAttrStyle($options),
                'path'       => $options['path'] ?: '',
                'file_path'  => $options['file_path'] ?: '',
            ],
            $view->vars
        );
    }

    /**
     * Generate style attribute
     *
     * @param array $options
     * @return string
     */
    protected function generateAttrStyle(array $options)
    {
        $styles = array();

        $opts = array(
            'max_width', 'max_height',
            'min_width', 'min_height'
        );

        foreach ($opts as $opt) {
            if (null !== $options[$opt] && $options[$opt] > 0) {
                $styles[] = str_replace('_', '-', $opt) . ': ' . $options[$opt] . 'px';
            }
        }

        return implode('; ', $styles);
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'file';
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'image';
    }
}