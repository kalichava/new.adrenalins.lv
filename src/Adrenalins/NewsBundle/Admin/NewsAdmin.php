<?php

namespace Adrenalins\NewsBundle\Admin;

use Adrenalins\NewsBundle\Entity\News;
use Imagine\Gd\Imagine;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

/**
 * News admin
 */
class NewsAdmin extends Admin
{
    /**
     * @var bool
     */
    public $imageEnabled;

    /**
     * @var bool
     */
    public $imageRequired;

    /**
     * @var array
     */
    public $thumbs;

    /**
     * @var array
     */
    public $dimensions;

    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param array  $imageParameters
     */
    public function __construct($code, $class, $baseControllerName, $imageParameters)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->imageEnabled = $imageParameters['enabled'];
        $this->imageRequired = $imageParameters['required'];
        $this->thumbs = $imageParameters['thumbs'];
        $this->dimensions = $imageParameters['dimensions'];
    }

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('slug', null, ['label' => 'Slug'])
            ->add('title', null, ['label' => 'Title'])
            ->add('categories', null, ['label' => 'Categories'])
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'title',
                null,
                [
                    'label' => 'Title'
                ]
            )
            ->add('type',
                'text',
                [
                    'template' => 'NewsBundle:NewsAdmin:news_type.html.twig',
                    'label' => 'Type',
                    'sortable' => false
                ]
            )
            ->add(
                'createdAt',
                null,
                [
                    'pattern' => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.news.date.format'
                    ),
                    'locale'  => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.news.date.locale'
                    ),
                    'label'   => 'Created at',
                ]
            )
            ->add(
                'updatedAt',
                null,
                [
                    'pattern' => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.news.date.format'
                    ),
                    'locale'  => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.news.date.locale'
                    ),
                    'label'   => 'Updated at',
                ]
            )
            ->add('categories', null, ['label' => 'Categories'])
            ->add('numComments', null, ['label' => 'Comments'])
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => true])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'slug',
                null,
                [
                    'required' => false,
                    'label'    => 'Slug',
                    'help'     => 'This field looks like URL parameter: http://adrenalins.lv/news/{slug}.<br> This parameter will be generated automaticaly if keep it empty.'
                ]
            )
            ->add(
                'title',
                null,
                [
                    'required' => true,
                    'label'    => 'Title'
                ]
            )
            ->add(
                'categories',
                null,
                [
                    'label'    => 'Categories',
                    'required' => false
                ]
            )
            ->add(
                'author',
                null,
                [
                    'label'    => 'Author',
                    'required' => true
                ]
            )
            ->add(
                'shortDescription',
                'textarea',
                [
                    'required' => true,
                    'attr'     => [
                        'maxlength' => 200,
                        'rows' => 5
                    ],
                    'label'    => 'Lead',
                ]
            )
            ->add(
                'content',
                'textarea',
                [
                    'required' => true,
                    'attr'     => [
                        'class' => 'tinymce'
                    ],
                    'label'    => 'Content',
                ]
            )
            ->add(
                'createdAt',
                'genemu_jquerydate',
                [
                    'data'      => ($this->getSubject() && $this->getSubject()->getId()) ? $this->getSubject()->getCreatedAt() : new \Datetime('now'),
                    'label'     => 'Created at',
                    'widget'    => 'single_text',
                    'required'  => true
                ]
            );

        if ($this->imageEnabled) {

            $obj = $this->getSubject();
            $path = $obj->getId() ? $obj->getImage() : '';
            $required = $this->imageRequired && !$obj->getId();

            $config = $this->getConfigurationPool()->getContainer()->getParameter('adrenalins.news.image');

            $formMapper->add(
                'file',
                'image',
                [
                    'label'       => 'Image',
                    'required'    => $required,
                    'path'        => $path,
                    'width'       => 300,
                    'height'      => 200,
                    'constraints' => [
                        new Assert\Image(
                            [
                                'maxSize' => $config['max_size']
                            ]
                        )
                    ]
                ]
            );
        }

        $videosMediaQuery = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('ApplicationSonataMediaBundle:Media')
            ->createQueryBuilder('m')
            ->where('m.providerName IN (:providers)')
            ->setParameter('providers', ['sonata.media.provider.vimeo', 'sonata.media.provider.youtube'])
            ->orderBy('m.createdAt', 'DESC');

        $formMapper
            ->add(
                'videos',
                'sonata_type_model',
                [
                    'query'       => $videosMediaQuery,
                    'multiple'    => true,
                    'required'    => false,
                    'empty_value' => '',
                    'btn_add'     => false
                ]
            )
            ->add(
                'galleries',
                null,
                [
                    'label'    => 'Galleries',
                    'required' => false
                ]
            )
            ->add(
                'type',
                'choice',
                [
                    'choices'  => [
                        News::TYPE_PUBLIC     => 'Public',
                        News::TYPE_MINI_FORUM => 'Miniforum'
                    ],
                    'expanded' => true,
                    'required' => true,
                    'label'    => 'Type',
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label' => 'Is Active'
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = (int) $admin->getRequest()->get('id');

        $menu->addChild(
            $this->trans('News Edit'),
            ['uri' => $admin->generateUrl('edit', ['id' => $id])]
        );

        $menu->addChild(
            $this->trans('News Comments'),
            ['uri' => $admin->generateUrl('adrenalins.admin.comments.list', ['id' => $id])]
        );
    }

    // Start of image processing

    /**
     * @param \Adrenalins\NewsBundle\Entity\News $object
     */
    public function uploadImages($object)
    {
        // 1. Upload original image
        if (false !== $image = $this->uploadFile($object)) {

            // 2. Delete old image if exists
            if ($object->getImage()) {
                $this->deleteFiles($object);
            }

            // 3. Update entity fields
            $object->setImage($image);
            $object->setFile(null);

            // 4. Check if thumbs are needed
            foreach ($this->thumbs as $imageThumb) {

                $setter = 'setImage' . ucfirst($imageThumb);

                $fullImagePath = $object->getRootWebPath() . $image;
                $imagine = new Imagine();
                $original = $imagine->open($fullImagePath);
                $thumb = $imagine->open($fullImagePath);
                $thumbPath = $object->getRootWebPath() . $object->getFullUploadPath()
                    . DIRECTORY_SEPARATOR . $imageThumb . '-' . basename($image);
                $thumbName = $object->getFullUploadPath() . '/' . $imageThumb . '-' . basename($image);

                // 4a. Check if resize is needed
                if (array_key_exists($imageThumb, $this->dimensions)) {

                    if (array_key_exists('width', $this->dimensions[$imageThumb]) && $original->getSize()->getWidth(
                        ) > $this->dimensions[$imageThumb]['width']
                    ) {
                        $thumb->resize($thumb->getSize()->widen($this->dimensions[$imageThumb]['width']));
                    }

                    if (array_key_exists('height', $this->dimensions[$imageThumb]) && $thumb->getSize()->getHeight(
                        ) > $this->dimensions[$imageThumb]['height']
                    ) {
                        $thumb->resize($thumb->getSize()->heighten($this->dimensions[$imageThumb]['height']));
                    }
                }

                // 4b. Save thumbs
                $thumb->save($thumbPath);
                $object->$setter($thumbName);
            }
        }
    }

    /**
     * Upload file
     *
     * @param \Adrenalins\NewsBundle\Entity\News $object
     * @param string                             $uploadPath
     * @param string                             $fileName
     *
     * @return bool|string
     */
    protected function uploadFile($object, $uploadPath = null, $fileName = null)
    {
        $file = $object->getFile();
        if (!$file instanceof UploadedFile) {
            return false;
        }

        if (null === $fileName) {
            if (null === $ext = $file->guessExtension()) {
                $ext = $file->getExtension();
            }

            $fileName = sha1(uniqid(mt_rand(), true));

            if (null !== $ext) {
                $fileName .= '.' . $ext;
            }
        }

        $fullUploadPath = $object->getFullUploadPath($uploadPath);
        $rootPath = $object->getRootWebPath();

        $movePath = $rootPath . $fullUploadPath;

        $file->move($movePath, $fileName);

        return $fullUploadPath . '/' . $fileName;
    }

    /**
     * @param \Adrenalins\NewsBundle\Entity\News $object
     */
    protected function deleteFiles($object)
    {
        $fs = new Filesystem();

        // Remove main file
        if ($object->getImage() !== null && $fs->exists($object->getRootWebPath() . $object->getImage())) {
            $fs->remove($object->getRootWebPath() . $object->getImage());
        }

        // Remove thumbs if any
        foreach ($this->thumbs as $imageThumb) {
            $getter = 'getImage' . ucfirst($imageThumb);
            if ($object->$getter() !== null && $fs->exists($object->getRootWebPath() . $object->$getter())) {
                $fs->remove($object->getRootWebPath() . $object->$getter());
            }
        }
    }

    // End of image processing

    /**
     * @param \Adrenalins\NewsBundle\Entity\News $object
     *
     * @return mixed|void
     */
    public function preUpdate($object)
    {
        if ($this->imageEnabled) {
            $this->uploadImages($object);
        }
    }

    /**
     * @param \Adrenalins\NewsBundle\Entity\News $object
     *
     * @return mixed|void
     */
    public function prePersist($object)
    {
        if ($this->imageEnabled) {
            $this->uploadImages($object);
        }
    }

    /**
     * Delete images before object delete
     *
     * @param mixed $object
     *
     * @return mixed|void
     */
    public function preRemove($object)
    {
        if ($this->imageEnabled) {
            $this->deleteFiles($object);
        }
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setAuthor(
            $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()
        );

        return $instance;
    }
}