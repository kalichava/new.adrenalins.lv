<?php

namespace Adrenalins\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/**
 * Class NewsCategoryAdmin
 *
 * @package Adrenalins\NewsBundle\Admin
 */
class NewsCategoryAdmin extends Admin
{
    protected $maxPerPage = 100;

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('slug', null, ['label' => 'Slug'])
            ->add('title', null, ['label' => 'Title']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'slug',
                null,
                [
                    'label' => 'Slug'
                ]
            )
            ->add(
                'title',
                null,
                [
                    'label' => 'Title'
                ]
            )
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                [
                    'translatable_class' => 'Adrenalins\NewsBundle\Entity\NewsCategory',
                    'fields'             => [
                        'title' => []
                    ]
                ]
            )
            ->add('slug');
    }
}