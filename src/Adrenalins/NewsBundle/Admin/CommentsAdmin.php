<?php

namespace Adrenalins\NewsBundle\Admin;

use Adrenalins\NewsBundle\Entity\Comment;
use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\NewsBundle\Form\EventListener\CommentSubscriber;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class CommentsAdmin
 *
 * @package Adrenalins\NewsBundle\Admin
 */
class CommentsAdmin extends Admin
{
    protected $maxPerPage = 100;

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'author',
                null,
                [
                    'label' => 'Author'
                ]
            )
            ->add(
                'body',
                null,
                [
                    'label' => 'Comment'
                ]
            )
            ->add(
                'createdAt',
                null,
                [
                    'label' => 'Created At'
                ]
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if (!$this->getSubject()->getId()) {
            if ($this->isChild()) {
                $formMapper
                    ->add(
                        'news',
                        'entity',
                        [
                            'data'   => $this->getParent()->getSubject(),
                            'mapped' => false,
                            'class'  => 'Adrenalins\NewsBundle\Entity\News'
                        ],
                        [
                            'admin_code' => 'adrenalins.admin.news'
                        ]
                    );
            } else {
                $formMapper
                    ->add(
                        'news',
                        'entity',
                        [
                            'mapped' => false,
                            'class'  => 'Adrenalins\NewsBundle\Entity\News'
                        ],
                        [
                            'admin_code' => 'adrenalins.admin.news'
                        ]
                    );
            }

            $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
            $formMapper->getFormBuilder()->addEventSubscriber(new CommentSubscriber($em));
        }

        $formMapper->add('body');
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var \Doctrine\ORM\EntityRepository $er */
        $er = $this
            ->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getRepository('NewsBundle:Comment');

        $qb = $er->createQueryBuilder('c')->orderBy('c.createdAt', 'DESC');

        if ($this->isChild()) {

            $id = (int) $this->getRequest()->get('id');

            if ($id) {
                $qb
                    ->leftJoin('c.thread', 't')
                    ->where('t.id = :id')
                    ->setParameter('id', News::THREAD_PREFIX . $id);

            } else {
                return parent::createQuery();
            }
        }

        return new ProxyQuery($qb);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'delete']);
    }

    public function getExportFormats()
    {
        return [];
    }

    /**
     * @param Comment $object
     *
     * @return mixed|void
     */
    public function preRemove($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $thread = $object->getThread();
        $repoName = '';

        if ($thread) {
            $threadData = explode('-', $thread->getId());
            list($module, $id) = $threadData;

            if ($module == 'news') {
                $repoName = 'NewsBundle:News';
            } elseif ($module = 'gallery') {
                $repoName = 'ApplicationSonataMediaBundle:Gallery';
            }

            if ($repoName) {
                $object = $em->getRepository($repoName)->find($id);
                if ($object) {
                    $object->setNumComments($object->getNumComments() - 1);
                    $em->flush($object);
                }
            }
        }
    }
}