<?php

namespace Adrenalins\MenuBundle\Form\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Adrenalins\MenuBundle\Entity\Menu;
use Adrenalins\MenuBundle\Entity\MenuItem;

/**
 * Class MenuSubscriber
 *
 * @package Adrenalins\MenuBundle\Form\EventListener
 */
class MenuSubscriber implements EventSubscriberInterface
{
    /** @var EntityManager */
    protected $em;

    /** @var integer */
    protected $menuId;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::SUBMIT     => 'submit',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();

        $this->menuId = $data['menu'];
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        /** @var MenuItem $data */
        $data = $event->getData();

        /** @var Menu $menu */
        $menu = $this->em->getRepository('MenuBundle:Menu')->find($this->menuId);
        if ($menu) {
            $data->setMenu($menu);
        }
    }
}