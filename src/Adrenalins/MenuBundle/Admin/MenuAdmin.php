<?php

namespace Adrenalins\MenuBundle\Admin;

use Adrenalins\MenuBundle\Entity\Menu;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

/**
 * Menu administer
 */
class MenuAdmin extends Admin
{
    protected $maxPerPage = 100;

    /**
     * {@inheritDoc}
     */
    public function getBatchActions()
    {
        return array();
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'title',
                null,
                array(
                    'sortable' => false,
                )
            )
            ->add(
                'spot',
                null,
                array(
                    'sortable' => false,
                )
            )
            ->add(
                'isActive',
                null,
                array(
                    'editable' => true,
                    'sortable' => false,
                )
            )
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit'   => array(),
                        'delete' => array()
                    )
                )
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'title'
            )
            ->add(
                'spot',
                'choice',
                [
                    'choices' => [
                        Menu::SPOT_HEADER => 'Header',
                        Menu::SPOT_FOOTER => 'Footer'
                    ]
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'required' => false
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = (int) $admin->getRequest()->get('id');

        $menu->addChild(
            $this->trans('Edit'),
            array('uri' => $admin->generateUrl('edit', array('id' => $id)))
        );

        $menu->addChild(
            $this->trans('Items'),
            array('uri' => $admin->generateUrl('adrenalins.admin.menu.items.list', array('id' => $id)))
        );
    }
}