<?php

namespace Adrenalins\MenuBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Pix\SortableBehaviorBundle\Services\PositionHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Adrenalins\MenuBundle\Form\EventListener\MenuSubscriber;

/**
 * Menu administer
 */
class MenuItemAdmin extends Admin
{
    /** @var int */
    public $last_position = 0;

    /** @var ContainerInterface */
    private $container;

    /** @var PositionHandler */
    private $positionService;

    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'position',
        '_sort_order' => 'ASC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->last_position = $this->positionService->getLastPosition($this->getRoot()->getClass());

        $listMapper
            ->addIdentifier(
                'title',
                null,
                [
                    'sortable' => false
                ]
            )
            ->add(
                'position',
                null,
                [
                    'sortable' => true,
                    'template' => 'MenuBundle:MenuAdmin:_sort.html.twig'
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'editable' => true,
                    'sortable' => false
                ]
            )
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => []
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                [
                    'fields'             => [
                        'title' => [
                            'field_type' => 'text'
                        ]
                    ],
                    'required'           => true,
                    'translatable_class' => 'Adrenalins\MenuBundle\Entity\MenuItem'
                ]
            )
            ->add('url')
            ->add(
                'page',
                'entity',
                [
                    'data'        => $this->getSubject()->getPage(),
                    'class'       => 'Adrenalins\PageBundle\Entity\Page',
                    'required'    => false,
                    'empty_value' => ''
                ],
                [
                    'admin_code' => 'adrenalins.admin.pages'
                ]
            )
            ->add('position')
            ->add(
                'menu',
                'entity',
                [
                    'data'   => ($this->getParent()) ? $this->getParent()->getSubject() : null,
                    'mapped' => false,
                    'class'  => 'Adrenalins\MenuBundle\Entity\Menu'
                ],
                [
                    'admin_code' => 'adrenalins.admin.menu'
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'required' => false
                ]
            );

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper->getFormBuilder()->addEventSubscriber(new MenuSubscriber($em));
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var \Doctrine\ORM\EntityRepository $er */
        $er = $this
            ->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getRepository('MenuBundle:MenuItem');

        $qb = $er
            ->createQueryBuilder('c')
            ->orderBy('c.position', 'ASC');

        if ($this->isChild()) {

            $id = (int) $this->getRequest()->get('id');

            if ($id) {
                $qb
                    ->leftJoin('c.menu', 'm')
                    ->where('m.id = :id')
                    ->setParameter('id', $id);

            } else {
                return parent::createQuery();
            }
        }

        return new ProxyQuery($qb);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PositionHandler $positionHandler
     */
    public function setPositionService(PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}