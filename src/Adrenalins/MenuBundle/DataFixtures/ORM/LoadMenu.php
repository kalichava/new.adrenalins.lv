<?php

namespace Adrenalins\MenuBundle\DataFixtures\ORM;

use Adrenalins\MenuBundle\Entity\MenuItem;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Adrenalins\MenuBundle\Entity\Menu;
use Adrenalins\MenuBundle\Entity\MenuItemTranslation;

/**
 * Class LoadMenu
 *
 * @package Adrenalins\MenuBundle\DataFixtures\ORM
 */
class LoadMenu extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = ['ru', 'en'];
        $menusData = $this->getMenusData();

        foreach ($menusData as $menuData) {

            $menu = new Menu();
            $menu->setSpot($menuData['spot']);
            $menu->setTitle($menuData['title']);
            $menu->setIsActive(true);

            $manager->persist($menu);

            if (array_key_exists('items', $menuData)) {
                foreach ($menuData['items'] as $menuItemData) {
                    $menuItem = new MenuItem();
                    $menuItem->setMenu($menu);
                    $menuItem->setTitle($menuItemData['title']);
                    $menuItem->setIsActive(true);

                    if (array_key_exists('url', $menuItemData)) {
                        $menuItem->setUrl($menuItemData['url']);
                    }

                    if ($menuItemData['pageRef']) {
                        $page = $this->getReference($menuItemData['pageRef']);
                        if ($page) {
                            $menuItem->setPage($page);
                        }
                    }

                    foreach ($locales as $locale) {
                        $menuItem->addTranslation(
                            new MenuItemTranslation($locale, 'title', $menuItemData['title'])
                        );
                    }

                    $manager->persist($menuItem);
                }
            }

            $manager->flush();
        }

    }

    private function getMenusData()
    {
        return [
            [
                'title' => 'Header',
                'spot'  => Menu::SPOT_HEADER,
                'items' => [
                    [
                        'title'   => 'About',
                        'pageRef' => 'page-about'
                    ],
                    [
                        'title'   => 'School',
                        'pageRef' => 'page-school-about'
                    ],
                    [
                        'title'   => 'Tandem Flights',
                        'pageRef' => 'page-tandem'
                    ],
                    [
                        'title'   => 'Club',
                        'pageRef' => 'page-club-about'
                    ],
                    [
                        'title'   => 'Media',
                        'pageRef' => '',
                        'url'     => 'media'
                    ],
                    [
                        'title'   => 'News',
                        'pageRef' => '',
                        'url'     => 'news'
                    ],
                    [
                        'title'   => 'Shop',
                        'pageRef' => '',
                        'url'     => 'http://shop.adrenalins.lv/',
                    ],
                    [
                        'title'   => 'Paramotors',
                        'pageRef' => 'page-paramotors-about'
                    ]
                ]
            ],
            [
                'title' => 'Footer',
                'spot'  => Menu::SPOT_FOOTER
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}