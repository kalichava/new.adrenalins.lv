<?php

namespace Adrenalins\MenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 *
 * @package Adrenalins\MenuBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param $name
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($name)
    {
        return $this->render('MenuBundle:Default:index.html.twig', array('name' => $name));
    }
}
