<?php

namespace Adrenalins\MenuBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class MenuItemRepository
 *
 * @package Adrenalins\MenuBundle\Entity\Repository
 */
class MenuItemRepository extends EntityRepository
{
    /**
     * Find active items
     *
     * @param integer $menuId
     *
     * @return array
     */
    public function findActiveItemsByMenu($menuId)
    {
        $qb = $this
            ->createQueryBuilder('i')
            ->where('i.isActive = true')
            ->andWhere('i.menu = :menu')
            ->setParameter('menu', $menuId)
            ->orderBy('i.position');

        return $qb->getQuery()->execute();
    }

}
