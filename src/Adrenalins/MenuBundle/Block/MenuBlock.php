<?php

namespace Adrenalins\MenuBundle\Block;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Adrenalins\MenuBundle\Entity\MenuItem;

/**
 * Class MenuBlock
 *
 * @package Adrenalins\MenuBundle\Block
 */
class MenuBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param Router          $router
     * @param EntityManager   $entityManager
     */
    public function __construct(
        EngineInterface $templating,
        Router $router,
        Request $request,
        EntityManager $entityManager
    ) {
        $this->router = $router;
        $this->request = $request;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        if (array_key_exists('attr', $settings) && array_key_exists('spot', $settings['attr'])) {

            $spot = mb_strtoupper(trim($settings['attr']['spot']));
            $spotId = constant('Adrenalins\MenuBundle\Entity\Menu::SPOT_' . $spot);

            $menu = $this->entityManager
                ->getRepository('MenuBundle:Menu')
                ->findOneBySpot($spotId);

            if ($menu) {

                $items = $this->generateItems($menu->getId());
                $subItems = $this->generateSubItems($items);
                $template = sprintf('MenuBundle:Block:%s.html.twig', trim($settings['attr']['spot']));

                return $this->renderResponse(
                    $template,
                    [
                        'block'    => $blockContext->getBlock(),
                        'items'    => $items,
                        'subItems' => $subItems
                    ],
                    $response
                );

            }
        }

        return $response;
    }

    /**
     * Generate sub menu items
     *
     * @param array $items
     *
     * @return array
     */
    private function generateSubItems(array $items)
    {
        $subItems = [];

        foreach ($items as $parent) {
            if (!$parent['pageParentId']) {
                continue;
            }

            $pageSubItems = $this->entityManager
                ->getRepository('PageBundle:Page')
                ->findActivePagesByParent($parent['pageParentId']);

            foreach ($pageSubItems as $pageSubItem) {

                if (!empty($parent['pageParentSlug'])) {
                    $url = $this->router->generate(
                        'adrenalins_page_child_show',
                        [
                            'parent' => $parent['pageParentSlug'],
                            'slug'   => $pageSubItem->getSlug()
                        ],
                        true
                    );
                } else {
                    $url = $this->router->generate(
                        'adrenalins_page_show',
                        [
                            'slug' => $pageSubItem->getSlug()
                        ],
                        true
                    );
                }

                $isCurrent = ($this->request->getUri() == $url);

                $subItems[$parent['id']][] = [
                    'url'       => $url,
                    'title'     => $pageSubItem->getTitle(),
                    'isCurrent' => $isCurrent
                ];
            }
        }

        return $subItems;
    }

    /**
     * Generate menu items
     *
     * @param integer $menuId
     *
     * @return array
     */
    private function generateItems($menuId)
    {
        $items = [];
        $locale = $this->request->getLocale();

        $menuItems = $this
            ->entityManager
            ->getRepository('MenuBundle:MenuItem')
            ->findActiveItemsByMenu($menuId);

        /** @var MenuItem $menuItem */
        foreach ($menuItems as $menuItem) {
            $url = trim($menuItem->getUrl());
            $page = $menuItem->getPage();

            $pageParent = null;
            $pageParentSlug = null;

            if (!$url && $page) {
                $pageParent = $page->getParent();
                if ($pageParent) {
                    $url = $this->router->generate(
                        'adrenalins_page_child_show',
                        [
                            'parent' => $pageParent->getSlug(),
                            'slug'   => $page->getSlug()
                        ],
                        true
                    );
                    $pageParentSlug = $pageParent->getSlug();
                } else {
                    $url = $this->router->generate('adrenalins_page_show', ['slug' => $page->getSlug()], true);
                }
            } else {
                if($locale != 'lv') {
                    $url = (!mb_stristr($url, $locale) && !mb_stristr($url, 'http') && !mb_stristr($url, 'www')) ?
                        sprintf('/%s/%s', $locale, $url) :
                        $url;
                }

                if(!mb_stristr($url, 'http') && !mb_stristr($url, 'www')) {
                    $url = ($locale != 'lv') ?
                        $this->request->getUriForPath($url) :
                        $this->request->getUriForPath('/' . $url);
                }
            }

            $pathInfo = array_filter(explode('/', $this->request->getPathInfo()));
            $isCurrent = false;

            if ($this->request->get('_route') != 'adrenalins_homepage') {
                if (
                    ($this->request->getUri() == $url) ||
                    (
                        is_array($pathInfo) &&
                        array_key_exists(1, $pathInfo) &&
                        (
                            $pathInfo[1] == $pageParentSlug ||
                            (
                                array_key_exists(2, $pathInfo) &&
                                $pathInfo[1] != $pageParentSlug &&
                                $pathInfo[2] == $pageParentSlug
                            )
                        )
                    )
                ) {
                    $isCurrent = true;
                }
            }

            $items[] = [
                'id'             => $menuItem->getId(),
                'url'            => $url,
                'title'          => $menuItem->getTitle(),
                'isCurrent'      => $isCurrent,
                'pageParentId'   => ($pageParent) ? $pageParent->getId() : null,
                'pageParentSlug' => ($pageParent) ? $pageParent->getSlug() : null
            ];
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'MenuBundle:Block:header.html.twig'
            ]
        );
    }
}
