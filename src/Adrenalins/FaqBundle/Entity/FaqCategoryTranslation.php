<?php

namespace Adrenalins\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="faq_category_translations",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="faq_category_translation_unique_idx", columns={
 *          "locale", "object_id", "field"
 *      })}
 * )
 */
class FaqCategoryTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="FaqCategory", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $object;
}