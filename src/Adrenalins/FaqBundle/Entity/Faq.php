<?php

namespace Adrenalins\FaqBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Adrenalins\FaqBundle\Entity\Repository\FaqRepository")
 * @ORM\Table(name="faq")
 * @Gedmo\TranslationEntity(class="Adrenalins\FaqBundle\Entity\FaqTranslation")
 * @ORM\HasLifecycleCallbacks
 */
class Faq
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\FaqBundle\Entity\FaqCategory", inversedBy="faqs")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $category;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="question", type="text")
     * @Assert\NotBlank
     */
    protected $question;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="answer", type="text")
     * @Assert\NotBlank
     */
    protected $answer;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="FaqTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Faq
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Faq
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set isActive
     *
     * @param bool $isActive
     * @return Faq
     */
    public function setIsActive($isActive)
    {
        $this->isActive = (bool) $isActive;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '' . $this->question;
    }

    /**
     * Set category
     *
     * @param \Adrenalins\FaqBundle\Entity\FaqCategory $category
     * @return Faq
     */
    public function setCategory(FaqCategory $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Adrenalins\FaqBundle\Entity\FaqCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Add translations
     *
     * @param FaqTranslation $translations
     * @return Faq
     */
    public function addTranslation(FaqTranslation $translations)
    {
        if (!$this->translations->contains($translations)) {
            $translations->setObject($this);
            $this->translations->add($translations);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param FaqTranslation $translations
     */
    public function removeTranslation(FaqTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    // Used For Sortable Feature in Sonata Admin class
    public function getClassNameFullPath()
    {
        return get_class($this);
    }

    /**
     * Get class name
     *
     * @return string
     */
    public function getClassName()
    {
        $class = explode('\\', __CLASS__);
        return end($class);
    }
}