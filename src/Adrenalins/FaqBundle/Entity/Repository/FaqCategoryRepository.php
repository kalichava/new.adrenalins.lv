<?php

namespace Adrenalins\FaqBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * FaqCategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FaqCategoryRepository extends EntityRepository
{
    public function getActiveList()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c', 'f')
            ->innerJoin('c.faqs', 'f')
            ->where('c.isActive = TRUE')
            ->andWhere('f.isActive = TRUE')
            ->orderBy('c.position', 'ASC')
        ;

        $query = $qb->getQuery();
        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        return $query->getResult();
    }
}
