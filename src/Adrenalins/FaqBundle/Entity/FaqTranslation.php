<?php

namespace Adrenalins\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="faq_translations",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="faq_translation_unique_idx", columns={
 *          "locale", "object_id", "field"
 *      })}
 * )
 */
class FaqTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Faq", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $object;
}