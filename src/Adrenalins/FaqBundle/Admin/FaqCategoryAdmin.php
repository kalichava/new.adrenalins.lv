<?php

namespace Adrenalins\FaqBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Faq category administer
 */
class FaqCategoryAdmin extends Admin
{
    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'title',
                null,
                array(
                    'sortable' => false,
                )
            )
            ->add(
                'slug',
                null,
                array(
                    'sortable' => false,
                )
            )
            ->add(
                'isActive',
                null,
                array(
                    'editable' => true,
                    'sortable' => false,
                )
            )
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    )
                )
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                array(
                    'translatable_class' => 'Adrenalins\FaqBundle\Entity\FaqCategory',
                    'fields'             => array(
                        'title' => array()
                    )
                )
            )
            ->add('slug')
            ->add('isActive');
    }

    /**
     * {@inheritDoc}
     */
    public function getBatchActions()
    {
        return array();
    }

    /**
     * {@inheritDoc}
     */
    public function getExportFormats()
    {
        return array();
    }
}
