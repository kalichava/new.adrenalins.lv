<?php

namespace Adrenalins\FaqBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Faq administer
 */
class FaqAdmin extends Admin
{
    public $last_position = 0;
    protected $maxPerPage = 100;

    /**
     * {@inheritDoc}
     */
    public function getBatchActions()
    {
        return array();
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var \Doctrine\ORM\EntityRepository $er */
        $er = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')
            ->getRepository('Adrenalins\FaqBundle\Entity\Faq');

        $qb = $er->createQueryBuilder('f')->orderBy('f.position', 'ASC');

        return new ProxyQuery($qb);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        // Get Last Position For Sortable Feature
        $this->last_position = $this->getLastPosition();

        $listMapper
            ->add('up', 'text', array('template' => 'FaqBundle:FaqAdmin:sort_up.html.twig', 'label' => ' '))
            ->add('down', 'text', array('template' => 'FaqBundle:FaqAdmin:sort_down.html.twig', 'label' => ' '))
            ->add(
                'question',
                null,
                array(
                    'sortable' => false,
                )
            )
            ->add(
                'category',
                null,
                array(
                    'sortable' => false,
                )
            )
            ->add(
                'isActive',
                null,
                array(
                    'editable' => true,
                    'sortable' => false,
                )
            )
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    )
                )
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('category');

        $i18nFields = array(
            'question' => array(),
            'answer' => array(
                'attr' => array(
                    'class' => 'tinymce'
                )
            )
        );

        $formMapper
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                array(
                    'translatable_class' => 'Adrenalins\FaqBundle\Entity\Faq',
                    'fields' => $i18nFields
                )
            );

        $formMapper->add('isActive', null, array('required' => false));
    }

    /**
     * @return int
     */
    public function getLastPosition()
    {
        $result = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager')->getRepository('FaqBundle:Faq')
            ->getLastPosition();

        return $result;
    }

}