<?php
namespace Adrenalins\FaqBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;


class SortableController extends Controller
{
    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function upAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $type = urldecode($type);
        $entity = $em->getRepository($type)->findOneById($id);

        if (!$entity) {
            throw new \Exception("There is no object for ({$type}) class and id ({$id})");
        }

        $position = $entity->getPosition();
        if ($position > 0) {
            $entity->setPosition($position - 1);

            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function downAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $type = urldecode($type);

        $repo = $em->getRepository($type);
        $entity = $repo->findOneById($id);

        if (!$entity) {
            throw new \Exception("There is no object for ({$type}) class and id ({$id})");
        }

        $position = $entity->getPosition();
        if ($position < $repo->createQueryBuilder('p')->select('MAX(p.position)')->getQuery()->getSingleScalarResult()) {
            $entity->setPosition($position + 1);

            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }
}