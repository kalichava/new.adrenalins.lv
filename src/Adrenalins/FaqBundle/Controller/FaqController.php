<?php

namespace Adrenalins\FaqBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FaqController
 *
 * @package Adrenalins\FaqBundle\Controller
 */
class FaqController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine();
        $pageRepo = $em->getRepository('PageBundle:Page');

        $parent = null;
        $pageSlug = trim(strip_tags($request->get('pageSlug')));
        $parentPageSlug = trim(strip_tags($request->get('parentPageSlug')));

        if ($parentPageSlug) {
            $parent = $pageRepo->findOneBySlug($parentPageSlug);
            if (!$parent) {
                throw $this->createNotFoundException('Not found parent page by slug - ' . $parentPageSlug);
            }
        }

        $page = $pageRepo->findActivePage($pageSlug, $parent);

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        $faqCategory = $em->getRepository('FaqBundle:FaqCategory')->findOneBySlug(ucfirst($parentPageSlug));

        if (!$faqCategory) {
            throw $this->createNotFoundException('Not found faq category by slug - ' . ucfirst($parentPageSlug));
        }

        $topics = $em
            ->getRepository('FaqBundle:Faq')
            ->findActiveListByCategory($faqCategory->getId());

        return $this->render(
            'FaqBundle::index.html.twig',
            [
                'page'   => $page,
                'topics' => $topics
            ]
        );
    }
}