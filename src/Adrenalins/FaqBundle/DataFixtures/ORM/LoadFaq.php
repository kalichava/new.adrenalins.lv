<?php

namespace Adrenalins\FaqBundle\DataFixtures\ORM;

use Faker\Factory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Adrenalins\FaqBundle\Entity\Faq;
use Adrenalins\FaqBundle\Entity\FaqTranslation;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class LoadFaq
 *
 * @package Adrenalins\NewsBundle\DataFixtures\ORM
 */
class LoadFaq extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = ['ru', 'en'];

        $faker['lv'] = Factory::create('en_EN');
        $faker['en'] = Factory::create('en_EN');
        $faker['ru'] = Factory::create('ru_RU');

        $fixturesCount = 20;

        while ($fixturesCount) {

            $faq = new Faq();
            $faq->setAnswer('<p>'.$faker['lv']->text(mt_rand(100,200)).'</p>');
            $faq->setQuestion($faker['lv']->sentence());
            $faq->setCategory($this->getReference('faq-category-' . mt_rand(1,2)));
            $faq->setPosition($fixturesCount);
            $faq->setIsActive((bool)rand(0, 1));

            foreach ($locales as $locale) {

                $trans = new FaqTranslation();
                $trans->setField('question');
                $trans->setContent($faker[$locale]->sentence());
                $trans->setLocale($locale);
                $faq->addTranslation($trans);

                $trans = new FaqTranslation();
                $trans->setField('answer');
                $trans->setContent('<p>'.$faker[$locale]->text(mt_rand(100,200)).'</p>');
                $trans->setLocale($locale);
                $faq->addTranslation($trans);

                $manager->persist($faq);
            }

            $manager->persist($faq);

            $fixturesCount--;
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}