<?php

namespace Adrenalins\FaqBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Adrenalins\FaqBundle\Entity\FaqCategory;
use Adrenalins\FaqBundle\Entity\FaqCategoryTranslation;

/**
 * Class LoadFaqCategory
 *
 * @package Adrenalins\FaqBundle\DataFixtures\ORM
 */
class LoadFaqCategory extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $categories = [
            'School',
            'Paramotors'
        ];

        $locales = ['ru', 'en'];

        $t = 1;
        foreach($categories as $categoryTitle) {

            $faqCategory = new FaqCategory();
            $faqCategory->setTitle($categoryTitle);

            foreach ($locales as $locale) {

                $trans = new FaqCategoryTranslation();
                $trans->setField('title');
                $trans->setContent($categoryTitle);
                $trans->setLocale($locale);

                $faqCategory->addTranslation($trans);

                $manager->persist($faqCategory);
            }

            $manager->flush();

            $this->setReference('faq-category-' . $t, $faqCategory);

            $t++;
        }

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}