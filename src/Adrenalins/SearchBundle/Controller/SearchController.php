<?php

namespace Adrenalins\SearchBundle\Controller;

use Adrenalins\NewsBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use IAkumaI\SphinxsearchBundle\Search\Sphinxsearch;
use SphinxClient;

/**
 * Class SearchController
 *
 * @package Adrenalins\SearchBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * @var SphinxClient $sphinxClient
     */
    protected $sphinxClient;

    /**
     * @var Sphinxsearch $sphinxSearch
     */
    protected $sphinxSearch;

    /**
     * @var $breadcrumbs
     */
    protected $breadcrumbs;

    /**
     * @var $breadcrumbsHelper
     */
    protected $breadcrumbsHelper;

    /**
     * @var string $currentLocale
     */
    protected $currentLocale = 'lv';

    /**
     * Search index action
     *
     * @param Request $request
     *
     * @throws \Exception
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // services
        $this->breadcrumbs = $this->get("white_october_breadcrumbs");
        $this->breadcrumbsHelper = $this->container->get("white_october_breadcrumbs.helper");
        $this->sphinxSearch = $this->get('iakumai.sphinxsearch.search');

        // common data
        $this->currentLocale = $request->getLocale();
        $userIsAuthenticated = (null === $this->getUser()) ? false : true;
        $isXmlHttpRequest = $request->isXmlHttpRequest();
        $query = $request->query->get('query');
        $searchQueryStatus = ((strlen($query) > 0) ? true : false);
        $totalPages = 0;

        // data for searchd
        $searchLimit = (int) $this->get('adrenalins.settings')->get('search-query-limit', 20);
        $currentPage = ((int) $request->query->get('page') > 1) ? (int) $request->query->get('page') : 1;
        $offset = (int) ($searchLimit * ($currentPage - 1));

        // working with search
        $this->sphinxClient = $this->sphinxSearch->getClient();
        $this->resetAll();
        $this->setDefaults($offset, $searchLimit, 'created_at');

        $result = $this->sphinxClient->Query(
            $this->sphinxClient->escapeString($query),
            $this->getSearchIndexesAsString()
        );

        if (strlen($this->sphinxClient->GetLastError())) {
            throw new \Exception($this->sphinxClient->GetLastError());
        }

        $docs = [];
        if ((int) $result['total_found'] > 0) {
            $totalPages = ceil($result['total_found'] / $searchLimit);

            $i = 0;
            foreach ($this->getSearchIndexes() as $index) {
                $idsByType = $this->getObjectIds($result, $index);

                $repoName = '';
                switch ($index) {
                    case 'news':
                        $repoName = 'NewsBundle:News';
                        break;

                    case 'static_page':
                        $repoName = 'PageBundle:Page';
                        break;

                    case 'gallery':
                        $repoName = 'ApplicationSonataMediaBundle:Gallery';
                        break;
                }

                if (count($idsByType) > 0) {

                    $em = $this->getDoctrine()->getRepository($repoName);
                    $objects = $em->findById($idsByType);

                    foreach ($objects as $object) {
                        if($index == 'news' && $object->getType() == News::TYPE_MINI_FORUM && !$userIsAuthenticated) {
                            continue;
                        }

                        $docs = $this->processObjects($searchQueryStatus, $index, $object, $query, $docs, $i);

                        $i++;

                        unset($object);
                    }
                }

                unset($em, $objects);
            }

            if (count($docs) > 0) {
                uasort($docs, 'self::sortDocsByCreatedAt');
            }

            // @todo in future:
            // 1. see todo at generateBreadcrumbs method
            // 2. see '$docs[$iteration]['objectUrl'] = $objectUrl;'

            // @todo now:
            // 1. generate link for object
        }

        $nextPageCount = $result['total_found'] - ($searchLimit);
        if ($totalPages > 0 && $currentPage > 1) {
            $nextPageCount = $result['total_found'] - ($offset * $currentPage);
        }

        if ($nextPageCount >= $searchLimit) {
            $nextPageCount = $searchLimit;
        }

        $nextPageIndex = ($currentPage == 0 || $currentPage == 1) ? 2 : ($currentPage + 1);
        if ($isXmlHttpRequest) {
            return new JsonResponse(
                [
                    'nextPageCount' => $nextPageCount,
                    'showMore'      => $nextPageCount > 0,
                    'nextPageIndex' => $nextPageIndex,
                    'docs'          => $docs,
                    'searchQuery'   => $query
                ]
            );
        }

        return $this->render(
            'SearchBundle:Search:index.html.twig',
            [
                'page'           => $this->getPage(),
                'totalFound'     => $result['total_found'],
                'nextPageCount'  => $nextPageCount,
                'showMore'       => $nextPageCount > 0,
                'nextPageIndex'  => $nextPageIndex,
                'rawSearchQuery' => $query,
                'searchQuery'    => (($searchQueryStatus) ? '"' . $query . '"' : ''),
                'docs'           => $docs
            ]
        );
    }

    /**
    * Get page
    *
    * @return Page
    */
    private function getPage()
    {
        /** @var PageRepository $pageRepo */
        $pageRepo = $this->getDoctrine()->getRepository('PageBundle:Page');

        /** @var Page $page */
        $page = $pageRepo->findActivePage('search', null);

        return $page;
    }

    /**
     * Full reset for SphinxClient
     */
    public function resetAll()
    {
        $this->sphinxClient->ResetFilters();
        $this->sphinxClient->ResetGroupBy();
        $this->sphinxClient->ResetOverrides();
    }

    /**
     * Set defaults for SphinxClient
     *
     * @param int    $offset
     * @param int    $limit
     * @param string $sortField
     */
    public function setDefaults($offset = 0, $limit = 100, $sortField = 'created_at')
    {
        $this->sphinxClient->setMatchMode(SPH_MATCH_EXTENDED2);
        $this->sphinxClient->SetRankingMode(SPH_RANK_NONE);
        $this->sphinxClient->SetLimits($offset, $limit);
        $this->sphinxClient->setSortMode(SPH_SORT_ATTR_DESC, $sortField);
    }

    /**
     * Get highlighted string
     *
     * @doc http://sphinxsearch.com/docs/current.html#api-func-buildexcerpts
     *
     * @param $string
     * @param $query
     *
     * @return mixed
     */
    public function highlightString($string, $index, $query)
    {
        if ($index != 'news' && $index != 'gallery') {
            $index = $index . '_' . $this->currentLocale;
        }

        $parsedStringAsArray = $this->sphinxClient->BuildExcerpts(
            [$string],
            $index,
            $query,
            [
                'before_match'    => '<span class="highlight">',
                'after_match'     => '</span>',
                'html_strip_mode' => 'strip',
                'chunk_separator' => '...',
                'limit'           => 512,
                'around'          => 6,
                'exact_phrase'    => false,
                'weight_order'    => true,
            ]
        );

        return reset($parsedStringAsArray);
    }

    /**
     * Process objects action
     *
     * @param bool $useHighlight
     * @param      $index
     * @param      $object
     * @param      $query
     * @param      $docs
     * @param      $iteration
     *
     * @return mixed
     */
    public function processObjects($useHighlight = true, $index, $object, $query, $docs, $iteration)
    {
        switch ($index) {
            case 'gallery':
                $title = $object->getName();
                $content = '';
                break;

            default:
                $title = $object->getTitle();
                if ($index == 'news') {
                    $content = $object->getShortDescription();
                } else {
                    $content = $this->truncateText($object->getContent(), 200, true);
                }
                break;
        }

        if ($useHighlight) {
            $title = $this->highlightString($title, $index, $query);
            $content = $this->highlightString($content, $index, $query);
        }

        $objectUrl = '#';
        switch ($index) {
            case 'news';
                if ($object->getType() == News::TYPE_MINI_FORUM) {
                    $objectUrl = $this->generateUrl(
                        'adrenalins_miniforum_show',
                        [
                            'slug' => $object->getSlug()
                        ]
                    );
                } else {
                    $objectUrl = $this->generateUrl(
                        'adrenalins_news_show',
                        [
                            'slug' => $object->getSlug()
                        ]
                    );
                }
                break;

            case 'gallery';
                $objectUrl = $this->generateUrl(
                    'sonata_media_gallery_view',
                    [
                        'id' => $object->getId()
                    ]
                );
                break;

            case 'static_page';
                if (null !== $object->getParent()) {
                    $objectUrl = $this->generateUrl(
                        'adrenalins_page_child_show',
                        [
                            'parent' => $object->getParent()->getSlug(),
                            'slug'   => $object->getSlug()
                        ]
                    );
                } else {
                    $objectUrl = $this->generateUrl('adrenalins_page_show', ['slug' => $object->getSlug()]);
                }
                break;
        }

        $docs[$iteration]['id'] = $object->getId();
        $docs[$iteration]['objectUrl'] = $objectUrl;
        $docs[$iteration]['objectType'] = $index;
        $docs[$iteration]['title'] = $title;
        $docs[$iteration]['content'] = $content;
        $docs[$iteration]['breadcrumbs'] = $this->generateBreadcrumbs($object, $index);
        $docs[$iteration]['createdAt'] = $object->getCreatedAt();

        return $docs;
    }

    /**
     * Generate breadcrumbs
     *
     * @param $object
     * @param $index
     *
     * @return mixed
     */
    public function generateBreadcrumbs($object, $index)
    {
        $translator = $this->get('translator');

        switch ($index) {
            case 'news':
                $this->breadcrumbs->addItem($translator->trans('news.index.tab.publicNews'));
                break;

            case 'gallery':
                $this->breadcrumbs->addItem($translator->trans('search.breadcrumbs.media'));
                $this->breadcrumbs->addItem($translator->trans('search.breadcrumbs.media.photo'));
                $this->breadcrumbs->addItem($object->getName());
                break;

            case 'static_page':
                $this->breadcrumbs->addItem(($object->getParent()) ? $object->getParent()->getTitle() : $object->getTitle());
                break;
        }

        $breadcrumbsHtml = $this->breadcrumbsHelper->breadcrumbs();
        $this->breadcrumbs->clear();

        return $breadcrumbsHtml;
    }

    /**
     * Get search indexes as array
     *
     * @return array
     */
    public function getSearchIndexes()
    {
        $indexes = [
            'news',
            'gallery',
            'static_page',
        ];

        return $indexes;
    }

    /**
     * Get search indexes as string
     *
     * @return string
     */
    public function getSearchIndexesAsString()
    {
        $indexesAsString = '';
        foreach ($this->getSearchIndexes() as $index) {
            if ($index != 'news' && $index != 'gallery') {
                $indexesAsString .= $index . '_' . $this->currentLocale . ' ';
            } else {
                $indexesAsString .= $index . ' ';
            }
        }

        return trim($indexesAsString);
    }

    /**
     * @param $result
     * @param $keyToSearch
     *
     * @return array
     */
    public function getObjectIds($result, $keyToSearch)
    {
        $objectIds = [];
        if (array_key_exists('matches', $result)) {
            array_walk(
                $result['matches'],
                function (&$value, $index) use (&$objectIds, $keyToSearch) {
                    if ($value['attrs']['object_type'] == $keyToSearch) {
                        $objectIds[] = $value['attrs']['object_id'];
                    }
                }
            );
        }

        return $objectIds;
    }

    /**
     * Text truncate
     *
     * @param        $value
     * @param int    $length
     * @param bool   $preserve
     * @param string $separator
     *
     * @return string
     */
    public function truncateText($value, $length = 30, $preserve = false, $separator = '...')
    {
        if (mb_strlen($value, 'UTF-8') > $length) {
            if ($preserve) {
                if (false !== ($breakpoint = mb_strpos($value, ' ', $length, 'UTF-8'))) {
                    $length = $breakpoint;
                }
            }

            return rtrim(mb_substr($value, 0, $length, 'UTF-8')) . $separator;
        }

        return $value;
    }

    /**
     * Sort documents by date
     *
     * @param $a
     * @param $b
     *
     * @return int
     */
    public function sortDocsByCreatedAt($a, $b)
    {
        if ($a['createdAt'] == $b['createdAt']) {
            return 0;
        }

        return ($a['createdAt'] > $b['createdAt']) ? -1 : 1;
    }

}
