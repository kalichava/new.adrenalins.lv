<?php

namespace Adrenalins\TranslationBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;

/**
 * Class TranslationController
 *
 * @package Adrenalins\TranslationBundle\Controller
 */
class TranslationController extends CoreController
{
    /**
     * @return Response
     * @throws \Exception
     */
    public function listAction()
    {
        $translateManager = $this->get('adrenalins.listener.translation');
        $translateManager->onKernelRequest();

        if (null !== $translateManager) {
            $locales = $translateManager->collection;

            $data = [];
            $default = $this->container->getParameter('locale', 'en');
            $missing = [];

            foreach ($data as $d) {
                if (!isset($locales[$d['locale']])) {
                    $locales[$d['locale']] = [
                        'entries' => [],
                        'data' => []
                    ];
                }
                if (is_array($d['entries'])) {
                    $locales[$d['locale']]['entries'] = array_merge($locales[$d['locale']]['entries'], $d['entries']);
                    $locales[$d['locale']]['data'][$d['filename']] = $d;
                }
            }

            // Search query
            if ($searchKey = $this->get('request')->get('search', null)) {
                foreach ($locales as $locale => $localeInfo) {
                    if (isset($localeInfo['entries'])) {
                        // Iteration entries in locale
                        foreach ($localeInfo['entries'] as $key => $value) {
                            if (stripos($key, $searchKey) === false && stripos($value, $searchKey) === false) {
                                unset ($locales[$locale]['entries'][$key]);
                            }
                        }
                    }
                }
            }

            $keys = array_keys($locales);
            $empty = empty($locales[$default]['entries']);
            $pagination = [];

            if (!$empty) {
                foreach ($keys as $locale) {
                    if ($locale != $default) {
                        foreach ($locales[$default]['entries'] as $key => $val) {
                            if (!isset($locales[$locale]['entries'][$key])) {
                                $missing[$key] = 1;
                            }
                        }
                    }
                }

                $paginator = $this->get('knp_paginator');

                $pagination = $paginator->paginate(
                    $locales[$default]['entries'],
                    $this->get('request')->query->get('page', 1),
                    20
                );

                $pagination->setTemplate('TranslationBundle:Translation:pagination.html.twig');
            }


            return $this->render(
                'TranslationBundle:Translation:list.html.twig',
                [
                    'base_template' => $this->getBaseTemplate(),
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
                    'locales' => $locales,
                    'default' => $default,
                    'missing' => $missing,
                    'empty' => $empty,
                    'pagination' => $pagination
                ]
            );
        }

        throw new \Exception('Something went wrong!');
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $response = [
            'isSuccessfuly' => true,
        ];

        if ($request->isXmlHttpRequest()) {
            $translateManager = $this->get('adrenalins.listener.translation');
            $translateManager->onKernelRequest();
            if (null !== $translateManager) {
                $key = $request->request->get('key');
                $locales = $translateManager->collection;

                foreach ($locales as $data) {
                    if (isset($data['entries'][$key])) {
                        $response = [
                            'isSuccessfuly' => false,
                            'message' => 'The source already exists. Please update it instead.',
                        ];
                    } else {
                        $translateManager->addTranslation(
                            $key,
                            $request->request->get('locale')
                        );
                    }
                }

                return new JsonResponse($response);
            }
        }

        throw new \Exception('Something went wrong!');
    }

    public function updateAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            $translateManager = $this->get('adrenalins.listener.translation');
            $translateManager->onKernelRequest();

            if (null !== $translateManager) {
                $oldData = '';

                $key = (string)$request->request->get('key');
                $locale = (string)$request->request->get('locale');
                $value = (string)$request->request->get('val');

                if (isset($key) && isset($locale) && isset($value)) {
                    if (array_key_exists($locale, $translateManager->collection) &&
                        array_key_exists('entries', $translateManager->collection[$locale]) &&
                        is_array($translateManager->collection[$locale]['entries']) &&
                        array_key_exists($key, $translateManager->collection[$locale]['entries'])
                    ) {

                        $oldData = $translateManager->collection[$locale]['entries'][$key];
                    }

                    $translateManager->updateTranslation($key, $locale, $value);
                }

                $response = [
                    'isSuccessfuly' => true,
                    'oldata' => $oldData,
                ];

                return new JsonResponse($response);
            }
        }

        throw new \Exception('Something went wrong!');
    }

    public function removeAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            $translateManager = $this->get('adrenalins.listener.translation');
            $translateManager->onKernelRequest();

            if (null !== $translateManager) {
                $translateManager->removeTranslation((string)$request->request->get('key'));

                $response = [
                    'isSuccessfuly' => true,
                ];

                return new JsonResponse($response);
            }
        }

        throw new \Exception('Something went wrong!');
    }
}
