var translation = new function () {

    this.openNewEditor = function () {
        $('.alert').hide();
        $('#newTranslation').show();
        $('#key').focus()

    };

    this.closeNewEditor = function () {
        $('.alert').hide();
        $('#newTranslation').hide()
    };

    this.deleteTranslation = function (id, key) {
        $.ajax({
            type: "POST",
            url: removeUrl,
            data: {'key': key},
            success: function (data) {
                $('#entry-' + id).remove();
            }
        });
    };

    this.showEditor = function (id) {
        $('#current-' + id).hide();
        $('#editor-' + id).show();
        $('#editor-' + id + ' input[type=text]').focus();
    };

    this.hideEditor = function (id) {
        $('#current-' + id).show();
        $('#editor-' + id).hide();
    };

    this.saveTranslation = function (index, key, locale, defaultLocale) {

        var newVal = $('#editor-' + index + ' input').val();
        $.ajax({
            type: "POST",
            url: updateUrl,
            data: {'key': key, 'locale': locale, 'val': newVal},
            success: function (data) {
                $('#save-' + index).val('Save');
                $('#current-' + index + '-content span.value').text(newVal);
                translation.hideEditor(index);
                if (defaultLocale.length) {

                    if ($('#default-current-' + index + '-' + defaultLocale + '-content').text().trim() != newVal) {
                        $('div#current-' + index).find('span.label-warning').hide();
                    } else {
                        $('div#current-' + index).find('span.label-warning').show();
                    }
                }
            }
        });
    };

    this.submitNewTranslation = function () {
        var form = $('#newTranslation');
        $('.alert').hide();

        $.ajax({
            url: form.attr('action'),
            dataType: 'json',
            data: form.serializeArray(),
            type: 'POST',
            success: function (response) {
                if (response && response.isSuccessfuly) {
                    $('#key').val('').focus();
                    form.find('input').val('');
                    $('#success-translation').html('New translation inserted successfully').show();
                    form.hide();
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                } else {
                    if (response && response.message) {
                        $('#error-translation').html(response.message).show();
                    }
                }
            }
        });

        return false;
    }

    this.openSearchForm = function (e) {
        e.preventDefault();
        $('form#searchTranslation').show();
    }

    this.closeSearchForm = function (e) {
        e.preventDefault();
        $('form#searchTranslation').hide();
    }

    this.initSearchForm = function () {
        $('.sonata-actions .search-form').click(this.openSearchForm);
        $('form#searchTranslation input.btn-close').click(this.closeSearchForm);
    }

}();

$(document).ready(function () {
    $('.add-new-translation').click(function (e) {
        e.preventDefault();
        translation.openNewEditor();
    });

    $('#newTranslation').submit(function (e) {
        e.preventDefault();
        translation.submitNewTranslation();
    });

    $('.close-new-translation').click(function (e) {
        e.preventDefault();
        translation.closeNewEditor();
    });

    translation.initSearchForm();
});
