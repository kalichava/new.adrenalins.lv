<?php
namespace Adrenalins\TranslationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class TranslationAdmin
 *
 * @package Adrenalins\TranslationBundle\Admin
 */
class TranslationAdmin extends Admin
{
    protected $baseRouteName = 'translation';
    protected $baseRoutePattern = 'translation';
}