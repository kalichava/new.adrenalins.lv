<?php

namespace Adrenalins\UserBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Adrenalins\UserBundle\Entity\User;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseUserAdmin;

/**
 * Class UserAdmin
 *
 * @package Adrenalins\UserBundle\Admin
 */
class UserAdmin extends BaseUserAdmin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_per_page'   => 100,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
            ->add('enabled', null, ['editable' => true])
            ->add('createdAt')
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => []
                    ]
                ]
            )
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('email')
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var User $user */
        $user = $this->getSubject();
        $fileFieldOptions = ['required' => false, 'label' => 'Avatar'];

        if ($user->getId()) {
            $fileFieldOptions['help'] = '<img src="' . $user->getAvatar() . '" class="admin-preview" />';
        }

        $formMapper
            ->with('General')
                ->add('username', null, ['label' => 'Username'])
                ->add('email', 'text', ['required' => false, 'label' => 'Email'])
                ->add('plainPassword', 'text', [
                    'label'    => 'Password',
                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId()))
                ])
                ->add('createdAt', 'genemu_jquerydate', ['required' => false, 'widget' => 'single_text', 'years' => range(1945, date('Y') + 10), 'label' => 'Created at'])
                ->add('enabled')
                ->add('isVisibleInContacts', null, ['required' => false, 'label' => 'Visible in contacts'])
            ->end()
            ->with('Profile')
                ->add('file', 'file', $fileFieldOptions)
                ->add('firstname', null, ['label' => 'First name'])
                ->add('lastname', null, ['label' => 'Last name'])
                ->add('status', 'text', ['required' => false, 'label' => 'Status'])
                ->add('smsMail', 'text', ['required' => false, 'label' => 'SMS Mail'])
                ->add('website', null, ['label' => 'Website'])
                ->add('biography', 'textarea', ['required' => false, 'label' => 'Biography'])
                ->add('dateOfBirth', 'genemu_jquerydate', ['required' => false, 'widget' => 'single_text', 'years' => range(1945, date('Y') + 10), 'label' => 'Date of birth'])
                ->add('phone', null, ['label' => 'Phone'])
            ->end()
            ->with('Groups')
                ->add('groups')
            ->end()
            ->with('Social')
                ->add('facebookUid')
                ->add('facebookName')
                ->add('twitterUid')
                ->add('twitterName')
                ->add('gplusUid')
                ->add('gplusName')
            ->end()
        ;
    }

    /**
     * @param User $object
     *
     * @return mixed|void
     */
    public function preUpdate($object)
    {
        $object->upload();

        parent::preUpdate($object);
    }

    /**
     * @param User $object
     *
     * @return mixed|void
     */
    public function prePersist($object)
    {
        $object->upload();

        parent::prePersist($object);
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setStatus('Pilot');

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $formBuilder = $this->getFormContractor()->getFormBuilder( $this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getBatchActions()
    {
        return [];
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }
}
