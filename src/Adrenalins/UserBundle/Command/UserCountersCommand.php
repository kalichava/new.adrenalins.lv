<?php

namespace Adrenalins\UserBundle\Command;

use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\UserBundle\Entity\User;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class NewsCountCommand
 *
 * @package Adrenalins\NewsletterBundle\Command
 */
class UserCountersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('adrenalins:users:update-counters')
            ->setDescription('Update user news, video, gallery counters');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine');

        $users = $dm
            ->getRepository('UserBundle:User')
            ->createQueryBuilder('u')
            ->getQuery()
            ->getResult();

        /** @var User $user */
        foreach ($users as $user) {
            $videosNum = $this->countUserVideos($dm, $user->getId());
            $galleriesNum = $this->countUserGalleries($dm, $user->getId());
            $publicNewsNum = $this->countUserPublicNews($dm, $user->getId());

            $user->setNewsNum($publicNewsNum);
            $user->setVideosNum($videosNum);
            $user->setGalleriesNum($galleriesNum);

            $dm->getManager()->flush($user);
        }
    }

    /**
     * @param         $dm
     * @param integer $userId
     *
     * @return mixed
     */
    protected function countUserPublicNews($dm, $userId)
    {
        return $dm
            ->getRepository('NewsBundle:News')
            ->createQueryBuilder('n')
            ->select('COUNT(n)')
            ->where('n.type = :type')
            ->andWhere('n.author = :authorId')
            ->setParameters(
                [
                    'type'     => News::TYPE_PUBLIC,
                    'authorId' => $userId
                ]
            )
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param         $dm
     * @param integer $userId
     *
     * @return mixed
     */
    protected function countUserGalleries($dm, $userId)
    {
        return $dm
            ->getRepository('ApplicationSonataMediaBundle:Gallery')
            ->createQueryBuilder('g')
            ->select('COUNT(g)')
            ->where('g.enabled = true')
            ->andWhere("g.context IN ('users', 'photo')")
            ->andWhere('g.author = :authorId')
            ->setParameters(
                [
                    'authorId' => $userId
                ]
            )
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param         $dm
     * @param integer $userId
     *
     * @return mixed
     */
    protected function countUserVideos($dm, $userId)
    {
        return $dm
            ->getRepository('ApplicationSonataMediaBundle:Media')
            ->createQueryBuilder('v')
            ->select('COUNT(v)')
            ->where('v.enabled = true')
            ->andWhere('v.providerName IN (:providers)')
            ->andWhere('v.author = :authorId')
            ->setParameters(
                [
                    'authorId'  => $userId,
                    'providers' => ['sonata.media.provider.vimeo', 'sonata.media.provider.youtube']
                ]
            )
            ->getQuery()
            ->getSingleScalarResult();
    }
}