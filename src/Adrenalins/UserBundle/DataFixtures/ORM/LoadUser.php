<?php

namespace Adrenalins\UserBundle\DataFixtures\ORM;

use Adrenalins\UserBundle\Entity\UserParaglider;
use Faker\Factory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Adrenalins\UserBundle\Entity\User;

/**
 * Class LoadUser
 *
 * @package Adrenalins\UserBundle\DataFixtures\ORM
 */
class LoadUser extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('lv_LV');

        $imageDir = '/uploads/users';
        $imagePath = realpath(__DIR__) . '/../../../../../web' . $imageDir;

        // super admin
        $superAdmin = new User();
        $superAdmin->setUsername('admin');
        $superAdmin->setFirstname('Anton');
        $superAdmin->setLastname('Kasperovich');
        $superAdmin->setEmail('super@admin.com');
        $superAdmin->setPlainPassword('password');
        $superAdmin->setEnabled(true);
        $superAdmin->setIsVisibleInContacts(true);
        $superAdmin->setSuperAdmin(true);
        $superAdmin->setBeganToFly(new \DateTime('now'));
        $superAdmin->setRoles(['ROLE_SUPER_ADMIN']);

        $avatar = $faker->image($imagePath, 140, 140, 'people');
        $avatarPath = $imageDir . '/' . basename($avatar);
        $superAdmin->setAvatar($avatarPath);

        $manager->persist($superAdmin);
        $manager->flush($superAdmin);

        $this->createUserParaglider($manager, $superAdmin);

        $this->addReference('admin-user', $superAdmin);

        $c = 1;
        for ($i = 1; $i <= 22; $i++) {

            $avatar = $faker->image($imagePath, 140, 140, 'people');
            $avatarPath = $imageDir . '/' . basename($avatar);

            $user = new User();
            $user->setUsername('user #' . $i);
            $user->setFirstname($faker->firstName);
            $user->setLastname($faker->lastName);
            $user->setEmail('user' . $i . '@adrenalins.lv');
            $user->setPlainPassword('password');
            $user->setAvatar($avatarPath);
            $user->setEnabled(true);
            $user->setBeganToFly(new \DateTime('now'));
            $user->setRoles([User::ROLE_DEFAULT]);
            $user->setIsVisibleInContacts((int) rand(0, 1));

            $manager->persist($user);
            $manager->flush($user);

            $this->createUserParaglider($manager, $user);

            $this->setReference('user-' . $c, $user);
            $c++;
        }
    }

    /**
     * @param ObjectManager $manager
     * @param User          $user
     */
    private function createUserParaglider(ObjectManager $manager, User $user)
    {
        $userParaglider = new UserParaglider();
        $userParaglider->setUser($user);
        $userParaglider->setModel(null);

        $manager->persist($userParaglider);
        $manager->flush($userParaglider);

        unset($userParaglider);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
