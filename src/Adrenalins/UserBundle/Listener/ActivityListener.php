<?php

namespace Adrenalins\UserBundle\Listener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Adrenalins\UserBundle\Entity\User;

class ActivityListener {

	protected $context;
	protected $entityManager;

	public function __construct(SecurityContext $context, EntityManager $entityManager)
	{
		$this->context = $context;
		$this->entityManager = $entityManager;
	}

	/**
	 * Update the user "lastActivity" on each request
	 * 
	 * @param FilterControllerEvent $event
	 */
	public function onCoreController(FilterControllerEvent $event)
	{
		if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
			return;
		}

		if ($this->context->getToken()) {
			$user = $this->context->getToken()->getUser();

			$delay = new \DateTime();
			$delay->setTimestamp(strtotime('5 minutes ago'));

			if ($user instanceof User && $user->getLastActivity() < $delay) {
				$user->isActiveNow();
				$this->entityManager->flush($user);
			}
		}
	}
}