<?php

namespace Adrenalins\UserBundle\Entity\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Adrenalins\UserBundle\Entity\User;

/**
 * Class UserRepository
 *
 * @package Adrenalins\UserBundle\Entity\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function findActivePilots()
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->select('u, up')
            ->leftJoin('u.paragliders', 'up', Expr\Join::WITH, 'up.model IS NOT NULL')
            ->where('u.id != 1')
            ->andWhere('u.enabled = true')
            ->orderBy('u.createdAt', 'ASC');

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function findVisibleInContacts()
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->where('u.enabled = true')
            ->andWhere('u.isVisibleInContacts = true')
            ->orderBy('u.createdAt', 'ASC');

        return $qb->getQuery()->execute();
    }

    public function getActive()
    {
        // As may have notice, the delay is redondant, for the sake of the example, i didn't defined its value in my bundle configuration, but it would be better to do so.
        $delay = new \DateTime();
        $delay->setTimestamp(strtotime('5 minutes ago'));

        $qb = $this->createQueryBuilder('u')
            ->where('u.lastActivity > :delay')
            ->setParameter('delay', $delay)
        ;

        return $qb->getQuery()->getResult();
    }
}