<?php

namespace Adrenalins\UserBundle\Entity;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Adrenalins\UserBundle\Entity\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(name="email", column=@ORM\Column(nullable=true)),
 *     @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(name="email_canonical", nullable=true, unique=false))
 * })
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Variable for file uploading
     *
     * @var UploadedFile|null
     *
     * @Assert\File(
     *     maxSize = "8M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $avatar;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $status = 'Pilot';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $smsMail;

    /**
     * @ORM\Column(name="began_fly_from", type="datetime", nullable=true)
     */
    protected $beganToFly;

    /**
     * @ORM\Column(name="last_activity", type="datetime", nullable=true)
     */
    protected $lastActivity;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $galleriesNum = 0;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $videosNum = 0;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $newsNum = 0;

    /**
     * @ORM\Column(name="visible_in_contacts", type="boolean", nullable=false)
     */
    protected $isVisibleInContacts = false;

    /**
     * @ORM\OneToMany(targetEntity="UserParaglider", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $paragliders;

    public function __construct()
    {
        parent::__construct();

        $this->paragliders = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->getFirstname(), $this->getLastname());
    }

    /**
     * Get full name of user
     *
     * @return string
     */
    public function getFullName()
    {
        return (string) $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile|null $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        if(!$this->avatar) {
            return '/img/140x140/4.jpg';
        }

        return $this->avatar;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $smsMail
     *
     * @return $this
     */
    public function setSmsMail($smsMail)
    {
        $this->smsMail = $smsMail;

        return $this;
    }

    /**
     * @return string
     */
    public function getSmsMail()
    {
        return $this->smsMail;
    }

    /**
     * Set beganToFly
     *
     * @param integer $beganToFly
     * @return User
     */
    public function setBeganToFly($beganToFly)
    {
        $this->beganToFly = $beganToFly;

        return $this;
    }

    /**
     * Get beganToFly
     *
     * @return string
     */
    public function getBeganToFly()
    {
        return $this->beganToFly;
    }

    /**
     * Set last activity when user online
     * 
     * @return boolean
     */
    public function isActiveNow()
    {
        $this->setLastActivity(new \DateTime());
    }

    /**
     * Set lastActivity
     *
     * @param integer $lastActivity
     * @return User
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return string
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * Add paragliders
     *
     * @param UserParaglider $paragliders
     *
     * @return User
     */
    public function addParaglider(UserParaglider $paragliders)
    {
        $paragliders->setUser($this);
        $this->paragliders->add($paragliders);

        return $this;
    }

    /**
     * Remove paragliders
     *
     * @param UserParaglider $paragliders
     */
    public function removeParaglider(UserParaglider $paragliders)
    {
        $this->paragliders->removeElement($paragliders);
    }

    /**
     * Get paragliders
     *
     * @return Collection
     */
    public function getParagliders()
    {
        return $this->paragliders;
    }

    /**
     * Get paragliders
     *
     * @return string
     */
    public function getParaglidersAsString()
    {
        $paragliders = [];
        foreach($this->getParagliders() as $paraglider) {
            $paragliders[] = trim($paraglider->getModel());
        }

        $paragliders = array_filter($paragliders);

        return (!empty($paragliders)) ? implode(', ', $paragliders) : '';
    }

    /**
     * @return integer
     */
    public function getNewsNum()
    {
        return $this->newsNum;
    }

    /**
     * @param integer $newsNum
     */
    public function setNewsNum($newsNum)
    {
        $this->newsNum = $newsNum;
    }

    /**
     * @return integer
     */
    public function getGalleriesNum()
    {
        return $this->galleriesNum;
    }

    /**
     * @param integer $galleriesNum
     */
    public function setGalleriesNum($galleriesNum)
    {
        $this->galleriesNum = $galleriesNum;
    }

    /**
     * @return integer
     */
    public function getVideosNum()
    {
        return $this->videosNum;
    }

    /**
     * @param integer $videosNum
     */
    public function setVideosNum($videosNum)
    {
        $this->videosNum = $videosNum;
    }

    /**
     * @param boolean $isVisibleInContacts
     */
    public function setIsVisibleInContacts($isVisibleInContacts)
    {
        $this->isVisibleInContacts = $isVisibleInContacts;
    }

    /**
     * @return boolean
     */
    public function getIsVisibleInContacts()
    {
        return $this->isVisibleInContacts;
    }

    /**
     * Upload avatar
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        try {
            $this->getFile()->move(
                $this->getRootWebPath() . $this->getFullUploadPath(),
                $this->getFile()->getClientOriginalName()
            );

            $this->avatar = $this->getFullUploadPath() .'/'. $this->getFile()->getClientOriginalName();

            $imagine = new Imagine();
            $imagine
                ->open($this->getRootWebPath() . $this->avatar)
                ->thumbnail(new Box(140, 140), \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND)
                ->save($this->getRootWebPath() . $this->avatar);

            $this->file = null;

            return $this->avatar;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get uplaod path for image files relative to root upload path
     *
     * @return string
     */
    public function getUploadPath()
    {
        return '/users';
    }

    /**
     * Get root web path
     *
     * @return string
     */
    public function getRootWebPath()
    {
        return realpath(__DIR__ . '/../../../../web');
    }

    /**
     * Get full upload path relative to root web path
     *
     * @return string
     */
    public function getFullUploadPath()
    {
        return '/uploads' . $this->getUploadPath();
    }
}
