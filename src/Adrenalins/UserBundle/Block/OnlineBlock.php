<?php

namespace Adrenalins\UserBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class OnlineBlock
 *
 * @package Adrenalins\UserBundle\Block
 */
class OnlineBlock extends BaseBlockService
{
    protected $router;
    protected $templating;
    protected $entityManager;

    /**
     * @param EngineInterface $templating
     * @param Router          $router
     * @param EntityManager   $entityManager
     */
    public function __construct(EngineInterface $templating, Router $router, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $users = $this->entityManager->getRepository('UserBundle:User')->getActive();
        $online = '';

        if(!empty($users)) {
            foreach($users as $user) {
                $url = $this->router->generate('adrenalins_user_public_profile', ['username' => $user->getUsername()]);
                $online .= sprintf('<a href="%s">%s</a>, ', $url, $user->getFullName());
            }

            $online = rtrim($online, ', ');

            return $this->renderResponse(
                $blockContext->getTemplate(),
                [
                    'block'  => $blockContext->getBlock(),
                    'count'  => count($users),
                    'online' => $online,
                ],
                $response
            );

        } else {
            return new Response(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'UserBundle:Block:online.html.twig'
            ]
        );
    }
}
