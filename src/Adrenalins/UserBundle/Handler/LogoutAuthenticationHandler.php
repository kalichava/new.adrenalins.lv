<?php

namespace Adrenalins\UserBundle\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

/**
 * Class LogoutAuthenticationHandler
 *
 * @package Adrenalins\UserBundle\Handler
 */
class LogoutAuthenticationHandler implements LogoutSuccessHandlerInterface
{
    /** @var Router */
    private $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        // redirect the user to where they were before the login process begun.
        $refererUrl = $request->headers->get('referer');
        if (!$refererUrl) {
            $refererUrl = $this->router->generate('adrenalins_homepage', [], true);
        }

        return new RedirectResponse($refererUrl);
    }
}