<?php

namespace Adrenalins\UserBundle\Controller;

use Adrenalins\NewsBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ContentController
 *
 * @package Adrenalins\UserBundle\Controller
 */
class ContentController extends Controller
{
    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function createLinkAction(Request $request)
    {
        $em = $this->getDoctrine();
        $data = [
            'link'  => '',
            'items' => []
        ];

        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $link = trim(strip_tags($request->get('linkName')));
        $module = trim(strip_tags($request->get('module')));
        $objectId = (int) $request->get('objectId');

        $validModules = ['news', 'video', 'place', 'event', 'gallery'];
        if (!in_array($module, $validModules) || !in_array($link, $validModules)) {
            throw new \Exception('Module ' . $module . ' or link ' . $link . ' not allowed.');
        }

        $data['link'] = $link;

        $object = $em
            ->getRepository($this->getRepositoryNameByLinkName($module))
            ->findOneBy(
                [
                    'id'     => (int) $request->get('objectId'),
                    'author' => $user
                ]
            );

        if ($object) {

            if ($request->isMethod('POST')) {

                $linkObjects = $request->get('linkObjects');

                if (is_array($linkObjects) && !empty($linkObjects)) {
                    $linked = [];

                    foreach ($linkObjects as $linkObjectId) {

                        $linkObject = $em
                            ->getRepository($this->getRepositoryNameByLinkName($link))
                            ->find((int) $linkObjectId);

                        if ($linkObject) {
                            $linked[] = [
                                'id'       => $linkObject->getId(),
                                'name'     => $linkObject->getName(),
                                'link'     => $link,
                                'module'   => $module,
                                'objectId' => $object->getId()
                            ];

                            call_user_func_array([$object, 'add' . ucfirst($link)], [$linkObject]);
                            call_user_func_array([$linkObject, 'add' . ucfirst($module)], [$object]);

                            $em->getManager()->persist($linkObject);
                            $em->getManager()->persist($object);
                            $em->getManager()->flush();
                        }

                    }

                    return JsonResponse::create(
                        [
                            'linked'  => $linked,
                            'success' => true
                        ]
                    );
                }

            } else {

                $selected = [];
                $criteria = [];

                if ($link == 'video') {
                    $criteria['enabled'] = true;
                    $criteria['providerName'] = ['sonata.media.provider.vimeo', 'sonata.media.provider.youtube'];

                    foreach($object->getVideos() as $item) {
                        $selected[] = $item->getId();
                    }
                } elseif ($link == 'gallery') {
                    foreach($object->getGalleries() as $item) {
                        $selected[] = $item->getId();
                    }

                    $criteria['enabled'] = true;
                } elseif ($link == 'news') {
                    $criteria['type'] = News::TYPE_PUBLIC;
                    $criteria['isActive'] = true;

                    foreach($object->getNews() as $item) {
                        $selected[] = $item->getId();
                    }
                } elseif ($link == 'event') {
                    $criteria['isActive'] = true;

                    foreach($object->getEvents() as $item) {
                        $selected[] = $item->getId();
                    }
                } elseif ($link == 'place') {
                    $criteria['isActive'] = true;

                    foreach($object->getPlaces() as $item) {
                        $selected[] = $item->getId();
                    }
                } else {
                    $criteria['isActive'] = true;
                }

                $repo = $em->getRepository($this->getRepositoryNameByLinkName($link));
                $items = $repo->findBy($criteria, ['createdAt' => 'DESC']);

                $data['items'] = $items;
                $data['selected'] = $selected;
            }

        }

        return $this->render(
            'UserBundle:Create:_popup_link.html.twig',
            $data
        );
    }

    /**
     * Remove user content links between his objects
     *
     * @param Request $request
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function removeLinkAction(Request $request)
    {
        $em = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $link = trim(strip_tags($request->get('linkName')));
        $module = trim(strip_tags($request->get('module')));
        $status = false;

        $validModules = ['news', 'video', 'place', 'event', 'gallery'];
        if (!in_array($module, $validModules) || !in_array($link, $validModules)) {
            throw new \Exception('Module ' . $module . ' or link ' . $link . ' not allowed.');
        }

        $object = $em
            ->getRepository($this->getRepositoryNameByLinkName($module))
            ->findOneBy(
                [
                    'id'     => (int) $request->get('objectId'),
                    'author' => $user
                ]
            );

        if ($object) {

            $linkObject = $em
                ->getRepository($this->getRepositoryNameByLinkName($link))
                ->find((int) $request->get('linkId'));

            if ($linkObject) {
                call_user_func_array([$object, 'remove' . ucfirst($link)], [$linkObject]);

                $em->getManager()->persist($object);
                $em->getManager()->flush();

                $status = true;
            }
        }

        return new JsonResponse(
            [
                'success' => $status
            ]
        );
    }

    /**
     * @param string $name
     *
     * @return string $repoName
     */
    private function getRepositoryNameByLinkName($name)
    {
        $repoName = '';

        switch ($name) {
            case 'news':
                $repoName = 'NewsBundle:News';
                break;

            case 'video':
                $repoName = 'ApplicationSonataMediaBundle:Media';
                break;

            case 'place':
                $repoName = 'PlaceBundle:Place';
                break;

            case 'event':
                $repoName = 'EventsBundle:Event';
                break;

            case 'gallery':
                $repoName = 'ApplicationSonataMediaBundle:Gallery';
                break;
        }

        return $repoName;
    }
}
