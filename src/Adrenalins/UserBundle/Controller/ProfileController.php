<?php

namespace Adrenalins\UserBundle\Controller;

use Adrenalins\NewsBundle\Entity\News;
use FOS\UserBundle\Model\UserInterface;
use Adrenalins\UserBundle\Form\Type\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ProfileController
 *
 * @package Adrenalins\UserBundle\Controller
 */
class ProfileController extends Controller
{
    public function showProfileAction(Request $request)
    {
        $em = $this->getDoctrine();

        $pageSlug = 'pilots';
        $parentPageSlug = 'club';
        $username = trim(strip_tags($request->get('username')));

        /** @var PageRepository $pageRepo */
        $pageRepo = $em->getRepository('PageBundle:Page');

        /** @var Page $parent */
        $parent = $pageRepo->findOneBySlug($parentPageSlug);

        if (!$parent) {
            throw $this->createNotFoundException('Not found parent page by slug - ' . $parentPageSlug);
        }

        /** @var Page $page */
        $page = $pageRepo->findActivePage($pageSlug, $parent);

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        $user = $em->getRepository('UserBundle:User')->findOneByUsername($username);

        if (!$user) {
            throw $this->createNotFoundException('Not found user by username - ' . $username);
        }

        $news = $em->getRepository('NewsBundle:News')->findBy(
            [
                'type'     => News::TYPE_PUBLIC,
                'author'   => $user,
                'isActive' => true
            ],
            [
                'publishedAt' => 'DESC'
            ]
        );

        $galleries = $em->getRepository('ApplicationSonataMediaBundle:Gallery')->findBy(
            [
                'author'  => $user,
                'context' => [
                    'users',
                    'photo'
                ],
                'enabled' => true
            ],
            [
                'createdAt' => 'DESC'
            ]
        );

        $videos = $em->getRepository('ApplicationSonataMediaBundle:Media')->findBy(
            [
                'author'       => $user,
                'providerName' => [
                    'sonata.media.provider.vimeo',
                    'sonata.media.provider.youtube'
                ],
                'enabled'      => true
            ],
            [
                'createdAt' => 'DESC'
            ]
        );

        $miniForum = $em->getRepository('NewsBundle:News')->findBy(
            [
                'type'     => News::TYPE_MINI_FORUM,
                'author'   => $user,
                'isActive' => true
            ],
            [
                'publishedAt' => 'DESC'
            ]
        );

        $events = $em->getRepository('EventsBundle:Event')->findBy(
            [
                'author'   => $user,
                'isActive' => true
            ],
            [
                'createdAt' => 'DESC'
            ]
        );

        $places = $em->getRepository('PlaceBundle:Place')->findBy(
            [
                'author'   => $user,
                'isActive' => true
            ],
            [
                'createdAt' => 'DESC'
            ]
        );

        return $this->render(
            'UserBundle:Profile:show.html.twig',
            [
                'page'      => $page,
                'user'      => $user,
                'news'      => $news,
                'galleries' => $galleries,
                'miniForum' => $miniForum,
                'videos'    => $videos,
                'places'    => $places,
                'events'    => $events,
            ]
        );
    }

    /**
     * Edit user profile
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editProfileAction(Request $request)
    {
        $em = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createForm(new ProfileType(), $user);

        if ($request->isMethod("POST")) {

            $form->submit($request);

            if ($form->isValid()) {

                foreach($user->getParagliders() as $k => $paraglider) {
                    if(is_null($paraglider->getModel()) || $paraglider->getModel() == '') {
                        $em->getManager()->remove($paraglider);
                        $user->getParagliders()->remove($k);
                    }
                }

                $user->upload();

                $currentPassword = $form['currentPassword']->getData();
                $newPassword = $form['newPassword']->getData();
                if ($currentPassword && $newPassword) {
                    $user->setPlainPassword($newPassword);
                }

                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($form->getData());

                $this->get('session')->getFlashBag()->set('success', 'profile.flash.updated');

                return new RedirectResponse($this->generateUrl('sonata_user_profile_edit'));
            } else {
                $flashMessage = 'profile.flash.error';
                $formErrors = $this->getErrorMessages($form);
                $this->get('session')->getFlashBag()->set('error', $flashMessage);
            }
        }

        return $this->render(
            'UserBundle:Profile:edit.html.twig',
            [
                'user'       => $user,
                'form'       => $form->createView(),
                'formErrors' => (isset($formErrors) ? $formErrors : null)
            ]
        );
    }

    /**
     * @param Form $form
     *
     * @return array
     */
    private function getErrorMessages($form)
    {
        $errors = array();

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }
}
