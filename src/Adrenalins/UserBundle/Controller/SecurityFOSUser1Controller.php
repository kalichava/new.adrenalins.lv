<?php

namespace Adrenalins\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\UserBundle\Controller\SecurityFOSUser1Controller as SecurityController;

/**
 * Class SecurityFOSUser1Controller
 *
 * @package Sonata\UserBundle\Controller
 *
 * @author Hugo Briand <briand@ekino.com>
 */
class SecurityFOSUser1Controller extends SecurityController
{
    public function loginAction()
    {
        $url = $this->container->get('router')->generate('adrenalins_homepage') . '#login';

        return new RedirectResponse($url);
    }
}