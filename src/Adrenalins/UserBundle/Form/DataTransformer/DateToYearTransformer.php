<?php

namespace Adrenalins\UserBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DateToYearTransformer implements DataTransformerInterface
{
    /**
     * @param \Datetime $datetime
     *
     * @throws TransformationFailedException
     *
     * @return integer
     */
    public function transform($datetime)
    {
        if (null === $datetime) {
            return "";
        }

        return $datetime->format('Y');
    }

    /**
     * @param mixed $year
     *
     * @throws TransformationFailedException
     *
     * @return mixed|object
     */
    public function reverseTransform($year)
    {
        if (!$year) {
            return null;
        }

        return new \DateTime($year . '-01-01');
    }

}