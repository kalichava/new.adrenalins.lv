<?php

namespace Adrenalins\UserBundle\Form\Type;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Adrenalins\UserBundle\Form\DataTransformer\DateToYearTransformer;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\UserBundle\Model\UserInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

/**
 * Class ProfileType
 *
 * @package Adrenalins\UserBundle\Form\Type
 */
class ProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                null,
                [
                    'label'    => 'user.profile.edit.form.label_email',
                    'required' => false
                ]
            )
            ->add(
                'file',
                'file',
                [
                    'label'    => 'user.profile.edit.form.label_avatar',
                    'required' => false
                ]
            )
            ->add(
                'currentPassword',
                'password',
                [
                    'label'       => 'user.profile.edit.form.label_current_password',
                    'mapped'      => false,
                    'required'    => false,
                    'constraints' => [
                        new UserPassword(['message' => 'user.profile.edit.form.current_is_not_valid'])
                    ]
                ]
            )
            ->add(
                'newPassword',
                'repeated',
                [
                    'type' => 'password',
                    'mapped' => false,
                    'invalid_message' => 'user.profile.edit.form.invalid_password_message',
                    'options' => [
                        'attr' => [
                            'maxlength' => 50
                        ]
                    ],
                    'required' => false,
                    'first_options'  => [
                        'label' => 'user.profile.edit.form.label_password'
                    ],
                    'second_options' => [
                        'label' => 'user.profile.edit.form.label_password_repeat'
                    ]
                ]
            )
            ->add(
                'biography',
                'textarea',
                [
                    'label'    => 'user.profile.edit.form.label_about',
                    'required' => false,
                    'attr'     => [
                        'rows' => 3
                    ]
                ]
            )
            ->add(
                'phone',
                null,
                [
                    'label'    => 'user.profile.edit.form.label_phone',
                    'required' => false
                ]
            )
            ->add(
                'smsMail',
                null,
                [
                    'label'    => 'user.profile.edit.form.label_smsMail',
                    'required' => false
                ]
            )
            ->add(
                'website',
                'text',
                [
                    'label'    => 'user.profile.edit.form.label_website',
                    'required' => false
                ]
            )
            ->add(
                'paragliders',
                'collection',
                [
                    'type'         => new ProfileParagliderType(),
                    'label'        => false,
                    'allow_add'    => true,
                    'by_reference' => false
                ]
            );

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();

            if (empty($data['currentPassword']) && empty($data['newPassword']['first']) && empty($data['newPassword']['second'])) {
                $form->add(
                    'currentPassword',
                    'password',
                    [
                        'label'       => 'user.profile.edit.form.label_current_password',
                        'mapped'      => false,
                        'required'    => false
                    ]
                );
            }
        });

        $years = range(1990, date('Y'));

        $builder->add(
            $builder
                ->create(
                    'beganToFly',
                    'choice',
                    [
                        'label'    => 'user.profile.edit.form.label_paraglider_beganToFly',
                        'required' => true,
                        'choices'  => array_combine($years, $years)
                    ]
                )
                ->addModelTransformer(new DateToYearTransformer())
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Adrenalins\UserBundle\Entity\User'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_profile';
    }
}
