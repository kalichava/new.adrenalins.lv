<?php

namespace Adrenalins\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProfileParagliderType
 *
 * @package Adrenalins\UserBundle\Form\Type
 */
class ProfileParagliderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'model',
                null,
                [
                    'label'    => false,
                    'attr'     => [
                        'class'       => 'float-left',
                        'placeholder' => 'user.profile.edit.form.label_paraglider_model'
                    ],
                    'required' => false
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Adrenalins\UserBundle\Entity\UserParaglider'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_profile_paraglider';
    }
}
