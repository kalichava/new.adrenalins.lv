<?php

namespace Adrenalins\SettingsBundle\Twig;

use Adrenalins\SettingsBundle\Core\SettingsLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Twig core extension
 */
class CoreExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Construct
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return array(
            'get_setting' => new \Twig_Function_Method($this, 'getSetting')
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return array(
            'trans_entity' => new \Twig_Filter_Method($this, 'transEntity')
        );
    }

    /**
     * Get setting
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function getSetting($key, $default = null)
    {
        return $this->container->get('adrenalins.settings')->get($key, $default);
    }


    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'settings_extension';
    }
}