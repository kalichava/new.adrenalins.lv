<?php

namespace Adrenalins\SettingsBundle\EventListener\Doctrine;

use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;

class SettingsFormListener implements EventSubscriberInterface
{
    private $factory;
    protected $settingsService;
    protected $setting;

    /**
     * @param FormFactoryInterface $factory
     * @param EntityManager $om
     */
    public function __construct(FormFactoryInterface $factory, $settingsService, $setting)
    {
        $this->factory = $factory;
        $this->settingsService = $settingsService;
        $this->setting = $setting;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SUBMIT   => 'preSubmit',
            FormEvents::PRE_SET_DATA => 'preSetData',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        return;
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();


        if (array_key_exists('additional_choices', $data) && $data['additional_choices']) {
            $additionalChoices = json_decode($data['additional_choices'], true);
            $choice = $data['choice'];
            $form->add(
                'choice',
                'choice',
                array(
                    'label' => 'adrenalins.settings.choice',
                    'mapped' => false,
                    'required' => false,
                    'attr' => array(
                        'class' => 'settings-choice select-allow-add'
                    ),
                    'choices' => array_merge($this->settingsService->getChoices($this->setting), $additionalChoices),
                    'data' => $choice
                )
            );
        }

        return;
    }
}