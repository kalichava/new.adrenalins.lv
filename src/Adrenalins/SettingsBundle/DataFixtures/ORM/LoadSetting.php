<?php

namespace Adrenalins\SettingsBundle\FixtureExample;

use Adrenalins\SettingsBundle\Entity\Setting;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadSetting
 *
 * @package Adrenalins\SettingsBundle\FixtureExample
 */
class LoadSetting extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getSettings() as $option) {

            $setting = new Setting();
            $setting->setKey($option['key']);
            $setting->setType($option['type']);

            if (array_key_exists('description', $option)) {
                $setting->setDescription($option['description']);
            }

            switch ($option['type']) {
                case 'text':
                    $setting->setValue($option['value']);

                    break;
                case 'select':
                    $setting->setValue($option['value']);

                    break;
                case 'file':
                    $setting->setPath($option['filename']);

                    break;
                default:
                    break;
            }

            $manager->persist($setting);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return [
            [
                'key'         => 'news-per-page',
                'type'        => 'text',
                'value'       => 12,
                'description' => 'Кол-во новостей на страницу'
            ],
            [
                'key'         => 'news-block-limit',
                'type'        => 'text',
                'value'       => 6,
                'description' => 'Кол-во новостей в блоке на главной'
            ],
            [
                'key'         => 'miniforum-block-limit',
                'type'        => 'text',
                'value'       => 7,
                'description' => 'Кол-во новостей в блоке минифорум на главной'
            ],
            [
                'key'         => 'events-block-limit',
                'type'        => 'text',
                'value'       => 5,
                'description' => 'Кол-во грядущих событий в блоке на главной'
            ],
            [
                'key'         => 'search-query-limit',
                'type'        => 'text',
                'value'       => 20,
                'description' => 'Кол-во отображаемых обьектов на странице поиска'
            ],
            [
                'key'         => 'slides-output-type',
                'type'        => 'select',
                'value'       => json_encode(
                    [
                        'options'       => [
                            'photo' => 'Photo',
                            'video' => 'Video',
                        ],
                        'chosen_option' => ['photo' => 'Photo']
                    ]
                ),
                'description' => 'Тип вывода слайдера на главной, фото или видео'
            ],
            [
                'key'         => 'slides-video-url',
                'type'        => 'text',
                'value'       => 'http://vjs.zencdn.net/v/oceans.mp4',
                'description' => 'Ссылка на видео файл для отображения на главной странице сайта'
            ],
            [
                'key'         => 'feedback-tandem-places',
                'type'        => 'select',
                'value'       => json_encode(
                    [
                        'options'       => [
                            'riga_rumbula' => 'Rīga (Rumbula)',
                            'jurkalne'     => 'Jurkalne',
                            'jekabpils'    => 'Jēkabpils',
                        ],
                        'chosen_option' => [
                            'riga_rumbula' => 'Rīga (Rumbula)'
                        ]
                    ]
                ),
                'description' => 'Места где мы летаем (тандем)'
            ],
            [
                'key'         => 'feedback-school-mails-to',
                'type'        => 'text',
                'value'       => 'mihail@adrenalins.lv,krisjanis@adrenalins.lv',
                'description' => 'Ящики для отправки обратной связи по вопросам школы (через запятую)'
            ],
            [
                'key'         => 'feedback-tandem-mails-to',
                'type'        => 'text',
                'value'       => 'dzintars@adrenalins.lv',
                'description' => 'Ящики для отправки обратной связи по вопросам тандемов (через запятую)'
            ],
            [
                'key'         => 'contacts-map-lat-lon',
                'type'        => 'text',
                'value'       => '56.88851838952163, 24.212756752967834',
                'description' => 'Точка lat&lon для страницы контактов'
            ],
        ];
    }
}
