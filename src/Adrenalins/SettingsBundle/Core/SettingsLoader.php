<?php

namespace Adrenalins\SettingsBundle\Core;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Cache\Cache;

/**
 * Settings loader
 *
 * In cache provider must use namespace (Example: "settings_")
 * Each cache provider in Doctrine system have a namespace option
 *
 * @todo: Use static cache?
 */
class SettingsLoader
{
    const DEFAULT_LIFE_TIME = 3600;

    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var Cache
     */
    protected $cacheProvider;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Construct
     *
     * @param ContainerInterface $container
     * @param string             $managerType
     * @param Cache              $cacheProvider
     * @param string             $class
     */
    public function __construct(ContainerInterface $container, $managerType, Cache $cacheProvider, $class)
    {
        $this->container = $container;
        $managerService = ($managerType == 'odm') ? 'doctrine_mongodb' : 'doctrine';

        $this->om = $this->container->get($managerService)->getManager();
        $this->cacheProvider = $cacheProvider;
        $this->class = $class;
    }

    /**
     * Get setting value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->load($key, $default);
    }

    /**
     * Load setting value
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    protected function load($key, $default)
    {
        if ($this->cacheProvider->contains($key)) {
            // Load from cache storage
            return $this->cacheProvider->fetch($key);
        } else {
            // Load from database storage

            $setting = $this->om->getRepository($this->class)
                ->findOneBy(array('key' => $key));

            if ($setting) {
                $value = $setting->getValue();

                if ($setting->getType() == 'file') {
                    $value = $setting->getWebPath();
                }

                if ($setting->getType() == 'select') {
                    $val = json_decode($setting->getValue(), true);
                    $value = $val['chosen_option'] ? array_shift($val['chosen_option']) : '';
                }

                $this->save($key, $value);
                return $value;
            }
        }

        return $default;
    }

    /**
     * Save setting value
     *
     * @param string $key
     * @param mixed  $value
     * @param int    $lifeTime
     *
     * @return bool
     */
    protected function save($key, $value, $lifeTime = self::DEFAULT_LIFE_TIME)
    {
        if ($this->cacheProvider->contains($key)) {
            // Remove old value
            $this->cacheProvider->delete($key);
        }

        // Set value to cache
        return $this->cacheProvider->save($key, $value, $lifeTime);
    }

    /**
     * Remove setting from cache storage
     *
     * @param string $key
     *
     * @return bool
     */
    public function remove($key)
    {
        if ($this->cacheProvider->contains($key)) {
            return $this->cacheProvider->delete($key);
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        $types = $this->container->getParameter('adrenalins.settings.types');

        return $types;
    }


    /**
     * @param $setting
     *
     * @return mixed
     * @throws \Exception
     */
    public function getChoices($setting)
    {
        $result = array();

        if ($setting->getType() == 'select' && $setting->getValue()) {
            $value = json_decode($setting->getValue(), true);

            $result = $value['options'];
        }

        return $result;
    }

    /**
     * @param $setting
     *
     * @return mixed
     * @throws \Exception
     */
    public function getChosenOption($setting)
    {
        $val = '';

        if ($setting->getType() == 'select' && $setting->getValue()) {
            $value = json_decode($setting->getValue(), true);
            $value = $value['chosen_option'];

            if ($value) {
                $val = key($value);
            }
        }

        return $val;
    }


}