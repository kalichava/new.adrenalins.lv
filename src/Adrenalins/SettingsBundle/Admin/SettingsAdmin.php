<?php

namespace Adrenalins\SettingsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Adrenalins\SettingsBundle\Entity\Setting;
use Adrenalins\SettingsBundle\EventListener\Doctrine\SettingsFormListener;

/**
 * Setting administer
 */
class SettingsAdmin extends Admin
{
    protected $baseRouteName = 'settings';
    protected $baseRoutePattern = 'settings';
    protected $classnameLabel = 'settings';

    protected $maxPerPage = 100;

    public function getIcon()
    {
        return 'bundles/settings/icons/settings.png';
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'SettingsBundle:Setting:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getBatchActions()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'key',
                null,
                [
                    'label' => 'Key',
                    'sortable' => false
                ]
            )
            ->add(
                'value',
                null,
                [
                    'label' => 'Value',
                    'sortable' => false,
                    'template' => 'SettingsBundle:Setting:value.html.twig'
                ]
            )
            ->add(
                'description',
                null,
                [
                    'label' => 'Description',
                    'sortable' => false
                ]
            )
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit' => [],
                        'delete' => []
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $allowAdd = $container->getParameter('adrenalins.settings.select_allow_add');
        $settingsService = $container->get('adrenalins.settings');
        $subject = $this->getSubject();

        $types = $settingsService->getTypes();
        if (!$subject->getId() && $allowAdd !== true) {
            unset($types['select']);
        }

        if ($subject->getId()) {
            $types = [
                $subject->getType() => $types[$subject->getType()]
            ];
        }

        $formMapper
            ->add(
                'key',
                null,
                [
                    'label' => 'Key'
                ]
            )
            ->add(
                'type',
                'choice',
                [
                    'label' => 'Type',
                    'choices' => $types,
                    'read_only' => (bool) $subject->getId(),
                    'attr' => [
                        'class' => 'settings-type'
                    ]
                ]
            )
            ->add(
                'text',
                'text',
                [
                    'label' => 'Text',
                    'mapped' => false,
                    'required' => false,
                    'data' => $subject->getId() && $subject->getType() == 'text' ? $subject->getValue() : '',
                    'attr' => [
                        'class' => 'settings-text',
                    ]
                ]
            )
            ->add(
                'file',
                'file',
                [
                    'label' => 'File',
                    'required' => false,
                    'attr' => [
                        'class' => 'settings-file'
                    ]
                ]
            )
        ;

        if ($allowAdd || $subject->getId()) {
            $allowAddClass = $allowAdd ? 'select-allow-add' : '';
            $formMapper
                ->add(
                    'choice',
                    'choice',
                    [
                        'label' => 'Choice',
                        'mapped' => false,
                        'required' => false,
                        'attr' => [
                            'class' => 'settings-choice '.$allowAddClass
                        ],
                        'choices' => $settingsService->getChoices($this->getSubject()),
                        'data' => $settingsService->getChosenOption($this->getSubject())
                    ]
                )
            ;

            if ($allowAdd) {
                $formMapper
                    ->add(
                        'additional_choices',
                        'hidden',
                        [
                            'mapped' => false,
                            'attr' => [
                                'class' => 'settings-additional-choices'
                            ]
                        ]
                    )
                ;
            }
        }

        $formMapper
            ->add(
                'description',
                null,
                [
                    'label' => 'Description',
                    'required' => false
                ]
            )
        ;

        $factory = $formMapper->getFormBuilder()->getFormFactory();
        $formMapper->getFormBuilder()->addEventSubscriber(new SettingsFormListener($factory, $settingsService, $subject));
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist($object)
    {
        $data = $this->getRequest()->request->all();
        $data = array_shift($data);

        $settingsService = $this->getConfigurationPool()->getContainer()->get('adrenalins.settings');

        if ($data['type'] == 'text' && $data['text']) {
            $object->setValue($data['text']);
        }

        if ($data['type'] == 'select' && $data['choice']) {
            $choices = array_merge($settingsService->getChoices($object), json_decode($data['additional_choices'], true));
            $chosenOption = [$data['choice'] => $choices[$data['choice']]];
            $object->setValue(
                json_encode(
                    [
                        'options' => $choices,
                        'chosen_option' => $chosenOption
                    ]
                )
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate($object)
    {
        $settingsService = $this->getConfigurationPool()->getContainer()->get('adrenalins.settings');

        $data = $this->getRequest()->request->all();
        $data = array_shift($data);

        if ($object->getPath() && $data['type'] != 'file') {
            $object->removeUpload();
            $object->setPath(null);
        }

        if ($data['type'] == 'text') {
            $object->setValue($data['text']);
        }

        if ($data['type'] == 'select') {
            $choices = $settingsService->getChoices($object);
            if (array_key_exists('additional_choices', $data) && $data['additional_choices']) {
                $choices = array_merge($choices, json_decode($data['additional_choices'], true));
            }

            $chosenOption = $data['choice'] ? [$data['choice'] => $choices[$data['choice']]] : '';
            $object->setValue(
                json_encode(
                    [
                        'options' => $choices,
                        'chosen_option' => $chosenOption
                    ]
                )
            );
        }

        if ($data['type'] == 'file') {
            $object->setValue(null);
        }

        /** @var $object Setting */
        $this->getConfigurationPool()
            ->getContainer()
            ->get('adrenalins.settings')
            ->remove($object->getKey());
    }
}
