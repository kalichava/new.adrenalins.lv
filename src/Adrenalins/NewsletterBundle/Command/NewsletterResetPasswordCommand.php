<?php

namespace Adrenalins\NewsletterBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class NewsletterPasswordResetCommand
 *
 * @package Adrenalins\NewsletterBundle\Command
 */
class NewsletterResetPasswordCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('adrenalins:newsletter:reset-password')
            ->setDescription('Send user password via sms and email');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $dm = $container->get('doctrine');
        $mailer = $container->get('mailer');

        $users = $dm
            ->getRepository('UserBundle:User')
            ->createQueryBuilder('u')
            ->where('u.username NOT IN (:usernames)')
            ->andWhere('u.id >= :idFrom')
            ->setParameters(
                [
                    'usernames' => [
                        'adrenalins.lv',
                        'dz',
                        'kalichava',
                        'arturiosan'
                    ]
                ]
            )
            ->getQuery()
            ->getResult();

        foreach ($users as $user) {

            $email = trim($user->getEmailCanonical());
            $smsEmail = trim($user->getSmsMail());

            $smsMessage = null;
            $mailMessage = null;

            $password = $this->generatePassword(5);

            if($email || $smsEmail) {
                $userManager = $container->get('fos_user.user_manager');
                $user->setPlainPassword($password);
                $userManager->updateUser($user);
            }

            if ($email) {

                $body = '
                <p>Sveiks, '. $user->getFirstname() .' '. $user->getLastname() .'!</p>
                <p>Tavs lietotājvārds priekš Adrenalins.lv mājas lapai ir "'. $user->getUsernameCanonical() .'" un parole "'. $password .'".</p>
                <p>Ja vēlies nomainīt tos, tad izdari to patstāvīgi savā profilā.Tavs Adrenalins.lv</p>
                ';

                try {
                    $mailMessage = \Swift_Message::newInstance()
                        ->setSubject('Tavs lietotājvārds un parole Adrenalins.lv mājas lapai')
                        ->setFrom('info@adrenalins.lv')
                        ->setTo($email)
                        ->setBody($body, 'text/html');

                    $mailer->send($mailMessage);
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

            }

            if ($smsEmail) {

                $body = 'Sveiks, '. $user->getFirstname() .' '. $user->getLastname() .'!Tavs prieksh miniforuma ir "'. $user->getUsernameCanonical() .'" un parole "'. $password .'". Tu vari nomainit tos sava profilaa. Tavs Adrenalins.lv';

                try {
                    $smsMessage = \Swift_Message::newInstance()
                        ->setSubject('Adrenalins.lv login/password')
                        ->setFrom('info@adrenalins.lv')
                        ->setTo($smsEmail)
                        ->setBody($body);

                    $mailer->send($smsMessage);
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

            }

            unset($userManager);
        }
    }

    /**
     * @param int $length
     *
     * @return string
     */
    private function generatePassword($length = 5)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}