<?php

namespace Adrenalins\NewsletterBundle\Admin;

use Adrenalins\NewsletterBundle\Entity\History;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class NewsletterHistoryAdmin
 *
 * @package Adrenalins\NewsletterBundle\Admin
 */
class NewsletterHistoryAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'addedAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('email', null, ['label' => 'Email'])
            ->add('mailing', null, ['label' => 'Mailing', 'sortable' => false])
            ->add('addedAt', null, ['label' => 'Added at'])
            ->add('sendedAt', null, ['label' => 'Sended at'])
            ->add(
                'status',
                'text',
                [
                    'template' => 'NewsletterBundle:Admin:list_status.html.twig',
                    'label'    => 'Status',
                    'sortable' => false
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('email', null, ['label' => 'Email'])
            ->add('status', 'doctrine_orm_string', [], 'choice', ['label' => 'Status', 'choices' => History::getStatusList()])
            ->add('mailing', null, ['label' => 'Mailing']);
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}