<?php

namespace Adrenalins\NewsletterBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class NewsletterContactAdmin
 *
 * @package Adrenalins\NewsletterBundle\Admin
 */
class NewsletterContactAdmin extends Admin
{
    protected $maxPerPage = 100;

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('fullName', 'text', ['label' => 'Name'])
            ->add('email', 'email', ['label' => 'Email'])
            ->add('phone', 'text', ['label' => 'Phone'])
            ->add('groups', null, ['label' => 'Groups'])
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => true])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('fullName', null, ['label' => 'Name'])
            ->add('phone', null, ['label' => 'Phone'])
            ->add('email', null, ['label' => 'Email'])
            ->add('groups', null, ['label' => 'Groups'])
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'email',
                'email',
                [
                    'label'    => 'Email',
                    'required' => true
                ]
            )
            ->add(
                'fullName',
                null,
                [
                    'label'    => 'Name',
                    'required' => false
                ]
            )
            ->add(
                'age',
                null,
                [
                    'label'    => 'Age',
                    'required' => false
                ]
            )
            ->add(
                'phone',
                null,
                [
                    'label'    => 'Phone',
                    'required' => false
                ]
            )
            ->add(
                'groups',
                null,
                [
                    'label'    => 'Groups',
                    'required' => true
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label' => 'Is Active'
                ]
            );
    }
}
