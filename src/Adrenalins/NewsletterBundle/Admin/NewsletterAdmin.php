<?php

namespace Adrenalins\NewsletterBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class NewsletterAdmin
 *
 * @package Adrenalins\NewsletterBundle\Admin
 */
class NewsletterAdmin extends Admin
{
    protected $maxPerPage = 100;

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, ['label' => 'Title'])
            ->add('group', null, ['label' => 'Groups'])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'send'   => [
                            'template' => 'NewsletterBundle:Admin:list__action_send.html.twig'
                        ],
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('title', null, ['label' => 'Title']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'title',
                null,
                [
                    'label' => 'Title',
                    'required' => true
                ]
            )
            ->add(
                'body',
                'textarea',
                [
                    'attr'  => [
                        'class' => 'tinymce'
                    ],
                    'label' => 'Body'
                ]
            )
            ->add(
                'senderName',
                'text',
                [
                    'label'    => 'Sender name',
                    'required' => true
                ]
            )
            ->add(
                'senderEmail',
                'email',
                [
                    'label'    => 'Sender email',
                    'required' => true
                ]
            )
            ->add(
                'group',
                null,
                [
                    'label'    => 'Groups',
                    'required' => false
                ]
            )
            ->add(
                'isUnsubscribeUrlEnable',
                null,
                [
                    'label'    => 'Include unsubscribe text',
                    'required' => false
                ]
            )
            ->add(
                'contacts',
                'sonata_type_model_autocomplete',
                [
                    'label'       => 'E-mails',
                    'help'        => 'E-mails loaded from Newsletter Contacts section, Adrenalins database',
                    'multiple'    => true,
                    'required'    => false,
                    'property'    => 'email',
                    'placeholder' => 'Autocomplete list',
                    'attr'        => [
                        'style' => 'width: 995px;'
                    ]
                ]
            )
            ->add(
                'customContacts',
                'text',
                [
                    'label'    => 'Custom E-mails',
                    'help'     => 'E-mails that NOT exists in Adrenalins contacts database',
                    'required' => false,
                    'attr'     => [
                        'style'                => 'width: 995px;',
                        'data-sonata-select2'  => 'false',
                        'data-isCustomSelect2' => 'true'
                    ]
                ]
            )
            ->add(
                'sendAtDatetime',
                'sonata_type_datetime_picker',
                [
                    'label'           => 'When to send',
                    'format'          => 'dd.MM.yyyy, HH:mm',
                    'required'        => false,
                    'dp_use_seconds'  => false,
                    'dp_side_by_side' => true
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('send', $this->getRouterIdParameter() . '/send');
    }

    /**
     * @TODO: find better solution?
     */
	public function prePersist($object)
    {
        if ($object->getCustomContacts() === NULL) {
            $object->setCustomContacts('');
        }
    }
}