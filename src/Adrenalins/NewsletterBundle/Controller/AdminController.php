<?php

namespace Adrenalins\NewsletterBundle\Controller;

use Mailgun\Mailgun;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Adrenalins\NewsletterBundle\Entity\Mailing;
use Adrenalins\NewsletterBundle\Entity\History;

/**
 * Class AdminController
 *
 * @package Adrenalins\NewsletterBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * Send newsletter mailing
     *
     * @param  Request $request
     *
     * @return RedirectResponse
     */
    public function sendAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $repo = $dm->getRepository('NewsletterBundle:Contact');
        $translator = $this->get('translator');

        $id = (int) $request->get($this->admin->getIdParameter());
        /** @var Mailing $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $contacts = $repo->findActiveByGroups($object->getGroupIds());

        // Append contacts from adrenalins database
        foreach ($object->getContacts() as $mailingContact) {
            $contacts[] = [
                'email'    => $mailingContact->getEmail(),
                'fullName' => $mailingContact->getFullName()
            ];
        }

        // Append custom contacts which not exist in adrenalins database
        $customEmails = explode(',', $object->getCustomContacts());
        foreach ($customEmails as $customEmail) {
            $contacts[] = [
                'email'    => $customEmail,
                'fullName' => null
            ];
        }

        if (!empty($contacts)) {
            $unsubscribeLabel = '';

            $unsubscribeMessage = $translator->trans('newsletter.unsubscribe_message');
            preg_match('/%(.*)%/', $unsubscribeMessage, $matches);
            if (!empty($matches) && array_key_exists(1, $matches)) {
                $unsubscribeLabel = $matches;
            }

            $mg = new Mailgun($this->container->getParameter('mailgun_key'));
            $domain = $this->container->getParameter('mailgun_domain');

            $batchMsg = $mg->BatchMessage($domain);
            $batchMsg->setSubject($object->getTitle());
            $batchMsg->setFromAddress($object->getSenderEmail(), ["full_name" => $object->getSenderName()]);

            /** @var \DateTime $sendAt */
            $sendAt = $object->getSendAtDateTime();
            if ($sendAt instanceof \DateTime && $sendAt->getTimestamp() >= time()) {
                $batchMsg->setDeliveryTime($sendAt->format('Y-m-d H:i:s'), 'Europe/Riga');
            }

            $batchMsg->setTestMode(false);

            $body = $object->getBody();

            if ($object->getIsUnsubscribeUrlEnable() && !empty($unsubscribeLabel)) {
                $unsubscribeUrl = $this->generateUrl(
                    'adrenalins_newsletter_unsubscribe',
                    [
                        'email' => '%recipient_email%'
                    ],
                    true
                );

                $body .= str_replace(
                    $unsubscribeLabel[0],
                    sprintf('<p><a href="%s">%s</a></p>', urldecode($unsubscribeUrl), $unsubscribeLabel[1]),
                    $unsubscribeMessage
                );
            }

            $batchMsg->setHtmlBody($body);
            $batchMsg->setTextBody(trim(html_entity_decode(strip_tags($body))));

            $hIds = [];
            foreach ($contacts as $contact) {
                if ($contact['email']) {
                    $history = new History();
                    $history->setEmail($contact['email']);
                    $history->setMailing($object->getTitle());
                    $history->setStatus(History::STATUS_PENDING);

                    $dm->getManager()->persist($history);
                    $dm->getManager()->flush($history);

                    $hIds[] = $history->getId();

                    $name = trim($contact['fullName']);
                    $batchMsg->addToRecipient($contact['email'], ($name) ? ["full_name" => $name] : []);

                    unset($history);
                }

            }

            $batchMsg->finalize();

            // Link history contacts to sended message ids
            $dm->getRepository('NewsletterBundle:History')->batchUpdateMessageId($hIds, $batchMsg->getMessageIds());
        }

        $this->addFlash('sonata_flash_success', 'Newsletter successfully added in queue');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}