<?php

namespace Adrenalins\NewsletterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Adrenalins\NewsletterBundle\Entity\History;
use Adrenalins\NewsletterBundle\Entity\Contact;
use Adrenalins\NewsletterBundle\Entity\Repository\ContactRepository;

/**
 * Class NewsletterController
 *
 * @package Adrenalins\NewsletterBundle\Controller
 */
class NewsletterController extends Controller
{
    /**
     * Webhook processing action
     *
     * @param Request $request
     *
     * @return Response
     */
    public function webhookAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $status = trim(strip_tags($request->get('event')));
        $messageId = trim($request->request->get('Message-Id'));
        $recipient = trim(strip_tags($request->get('recipient')));

        /** @var History $history */
        $history = $dm
            ->getRepository('NewsletterBundle:History')
            ->searchByEmailAndMessageId($recipient, $messageId);

        if ($history) {
            $sendedAt = new \DateTime('now');
            $sendedAt->setTimestamp((int) $request->get('timestamp'));

            $history->setStatus($status);
            $history->setSendedAt($sendedAt);

            $dm->getManager()->flush($history);
        }

        return Response::create(null);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function unsubscribeAction(Request $request)
    {
        $dm = $this->getDoctrine();
        /** @var ContactRepository $repo */
        $repo = $dm->getRepository('NewsletterBundle:Contact');
        $email = urldecode($request->get('email'));
        $translator = $this->get('translator');

        /** @var Contact $contact */
        $contacts = $repo->findActiveByEmail($email);
        if (!$contacts) {
            throw  $this->createNotFoundException('Not found email - ' . $email);
        }

        foreach ($contacts as $contact) {
            $contact->setIsActive(false);

            $dm->getManager()->flush();
        }

        $this->get('session')->getFlashBag()->add(
            'notice',
            $translator->trans('newsletter.unsubscribe_message.success')
        );

        return new RedirectResponse($this->generateUrl('adrenalins_homepage'));
    }
}