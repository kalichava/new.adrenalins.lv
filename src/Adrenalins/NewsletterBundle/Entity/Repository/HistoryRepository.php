<?php

namespace Adrenalins\NewsletterBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class HistoryRepository
 *
 * @package Adrenalins\NewsletterBundle\Entity\Repository
 */
class HistoryRepository extends EntityRepository
{
    /**
     * Search contacts in history by message id
     *
     * @param string $email
     * @param string $messageId
     *
     * @return array
     */
    public function searchByEmailAndMessageId($email, $messageId)
    {
        $qb = $this->createQueryBuilder('h');

        $qb
            ->where('h.email = :email')
            ->andWhere($qb->expr()->like('h.messageId', $qb->expr()->literal('%' . $messageId . '%')))
            ->setParameter('email', $email);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Link MailGun message ids to history contact ids
     *
     * @param array $ids        Contact Ids
     * @param array $messageIds Message Ids
     *
     * @return array
     */
    public function batchUpdateMessageId(array $ids, array $messageIds)
    {
        $qb = $this
            ->createQueryBuilder('h')
            ->update()
            ->set('h.messageId', ':messageId')
            ->where('h.id IN (:ids)')
            ->setParameters(
                [
                    'ids'       => $ids,
                    'messageId' => serialize($messageIds)
                ]
            );

        return $qb->getQuery()->execute();
    }
}