<?php

namespace Adrenalins\NewsletterBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ContactRepository
 *
 * @package Adrenalins\NewsletterBundle\Entity
 */
class ContactRepository extends EntityRepository
{
    /**
     * Find active contacts by email
     *
     * @param string $email Contact email
     *
     * @return array
     */
    public function findActiveByEmail($email)
    {
        $qb = $this
            ->createQueryBuilder('c')
            ->where('c.isActive = :isActive')
            ->andWhere('c.email = :email')
            ->setParameters(
                [
                    'email' => $email,
                    'isActive' => true
                ]
            );

        return $qb->getQuery()->getResult();
    }

    /**
     * Find active contacts by groups
     *
     * @param array $groupIds
     *
     * @return array
     */
    public function findActiveByGroups(array $groupIds)
    {
        $qb = $this
            ->createQueryBuilder('c')
            ->join('c.groups', 'g')
            ->where('c.isActive = :isActive')
            ->andWhere('g.id IN (:groupIds)')
            ->setParameters(
                [
                    'isActive' => true,
                    'groupIds' => $groupIds
                ]
            );

        return $qb->getQuery()->getArrayResult();
    }
}