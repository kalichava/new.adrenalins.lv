<?php

namespace Adrenalins\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Adrenalins\NewsletterBundle\Entity\Mailing
 *
 * @ORM\Table(name="newsletter_mailing")
 * @ORM\Entity(repositoryClass="Adrenalins\NewsletterBundle\Entity\Repository\MailingRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Mailing
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $title
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string $body
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="body", type="text")
     */
    protected $body;

    /**
     * @var string $senderEmail
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(name="senderEmail", type="string", length=255)
     */
    protected $senderEmail = 'info@adrenalins.lv';

    /**
     * @var string $senderName
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="senderName", type="string", length=255)
     */
    protected $senderName = 'Adrenalins.lv';

    /**
     * @var \Datetime $createdAt
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \Datetime $updatedAt
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var integer $totalCount
     *
     * @ORM\Column(name="totalCount", type="integer")
     */
    protected $totalCount = 0;

    /**
     * @var integer $sentCount
     *
     * @ORM\Column(name="sentCount", type="integer")
     */
    protected $sentCount = 0;

    /**
     * @var integer $errorsCount
     *
     * @ORM\Column(name="errorsCount", type="integer")
     */
    protected $errorsCount = 0;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\NewsletterBundle\Entity\ContactGroup")
     */
    protected $group;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\NewsletterBundle\Entity\Contact")
     * @ORM\JoinTable(name="newsletter_mailing_contacts")
     */
    protected $contacts;

    /**
     * @var string $customContacts
     *
     * @ORM\Column(name="custom_contacts", type="text")
     */
    protected $customContacts;

    /**
     * @var boolean $isUnsubscribeUrlEnable
     *
     * @ORM\Column(name="is_unsubscribe_url_enable", type="boolean", options={"default" = false})
     */
    protected $isUnsubscribeUrlEnable = false;

    /**
     * @var \Datetime $sendAtDatetime
     *
     * @ORM\Column(name="sendAtDatetime", type="datetime", nullable=true, options={"default" = NULL})
     */
    protected $sendAtDatetime = null;

    /**
     * Stringifier
     *
     * @return string title or uniqueid value just for sonata admin bundle integration
     */
    public function __toString()
    {
        return $this->getTitle() != null ? $this->getTitle() : uniqid();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->group = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title title
     *
     * @return null
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body body
     *
     * @return null
     *
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist()
     *
     * @return null
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @return null
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * sender email getter
     *
     * @return string senderEmail
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * sender email setter
     *
     * @param string $value sender email
     *
     * @return null
     */
    public function setSenderEmail($value)
    {
        $this->senderEmail = $value;
    }

    /**
     * sender name getter
     *
     * @return string senderName
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * sender email getter
     *
     * @param string $value sender name
     *
     * @return null
     */
    public function setSenderName($value)
    {
        $this->senderName = $value;

        return $this;
    }

    /**
     * total count setter
     *
     * @param integer $value total count value
     *
     * @return null
     */
    public function setTotalCount($value)
    {
        $this->totalCount = $value;
    }

    /**
     * total count setter
     *
     * @return integer totalcount
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * error count setter
     *
     * @param integer $value errors count
     *
     * @return null
     */
    public function setErrorsCount($value)
    {
        $this->errorsCount = $value;
    }

    /**
     * error count getter
     *
     * @return integer $value errors count
     */
    public function getErrorsCount()
    {
        return $this->errorsCount;
    }

    /**
     * error count setter
     *
     * @param integer $value errors count
     *
     * @return null
     */
    public function setSentCount($value)
    {
        $this->sentCount = $value;
    }

    /**
     * error count getter
     *
     * @return integer errors count
     *
     */
    public function getSentCount()
    {
        return $this->sentCount;
    }

    /**
     * Set group
     *
     * @param ContactGroup $group
     *
     * @return Mailing
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Add group
     *
     * @param ContactGroup $group
     *
     * @return Mailing
     */
    public function addGroup(ContactGroup $group)
    {
        $this->group[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param ContactGroup $group
     */
    public function removeGroup(ContactGroup $group)
    {
        $this->group->removeElement($group);
    }

    /**
     * Get group
     *
     * @return Collection
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Get group ids
     *
     * @return array Ids
     */
    public function getGroupIds()
    {
        $ids = [];
        foreach ($this->getGroup() as $group) {
            $ids[] = $group->getId();
        }

        return $ids;
    }

    /**
     * Set isUnsubscribeUrlEnable
     *
     * @param boolean $isUnsubscribeUrlEnable
     *
     * @return $this
     */
    public function setIsUnsubscribeUrlEnable($isUnsubscribeUrlEnable)
    {
        $this->isUnsubscribeUrlEnable = $isUnsubscribeUrlEnable;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsUnsubscribeUrlEnable()
    {
        return $this->isUnsubscribeUrlEnable;
    }

    /**
     * Set sendAtDatetime
     *
     * @param \DateTime $sendAtDatetime
     *
     * @return $this
     */
    public function setSendAtDatetime($sendAtDatetime)
    {
        $this->sendAtDatetime = $sendAtDatetime;

        return $this;
    }

    /**
     * Get sendAtDatetime
     *
     * @return \DateTime
     */
    public function getSendAtDatetime()
    {
        return $this->sendAtDatetime;
    }

    /**
     * Get contacts
     *
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Adds contacts.
     *
     * @param Contact $contact
     *
     * @return $this
     */
    public function addContact(Contact $contact)
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
        }

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param Contact $contact
     */
    public function removeContact(Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Set customContacts
     *
     * @param string $customContacts customContacts
     *
     * @return null
     */
    public function setCustomContacts($customContacts)
    {
        $this->customContacts = $customContacts;
    }

    /**
     * Get customContacts
     *
     * @return string
     */
    public function getCustomContacts()
    {
        return $this->customContacts;
    }
}
