<?php

namespace Adrenalins\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class History
 *
 * @ORM\Table(name="newsletter_history")
 * @ORM\Entity(repositoryClass="Adrenalins\NewsletterBundle\Entity\Repository\HistoryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class History
{
    const STATUS_PENDING = 'pending';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_ERROR = 'error';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @var string $mailing
     *
     * @ORM\Column(name="mailing", type="string", length=255, nullable=false)
     */
    protected $mailing;

    /**
     * @var string $messageId
     *
     * @ORM\Column(name="message_id", type="string", length=255, nullable=true)
     */
    protected $messageId;

    /**
     * @var \Datetime $sendedAt
     *
     * @ORM\Column(name="addedAt", type="datetime")
     */
    protected $addedAt;

    /**
     * @var \Datetime $sendedAt
     *
     * @ORM\Column(name="sendedAt", type="datetime", nullable=true, options={"default" = NULL})
     */
    protected $sendedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $status = self::STATUS_PENDING;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get email
     *
     * @return string email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email email
     *
     * @return History
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get messageId
     *
     * @return string messageId
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Set messageId
     *
     * @param string $messageId Message ID
     *
     * @return History
     */
    public function setMessageId($messageId)
    {
        $this->$messageId = $messageId;

        return $this;
    }

    /**
     * Get mailing
     *
     * @return string mailing
     */
    public function getMailing()
    {
        return $this->mailing;
    }

    /**
     * Set mailing
     *
     * @param string $mailing Mailing name
     *
     * @return History
     */
    public function setMailing($mailing)
    {
        $this->mailing = $mailing;

        return $this;
    }

    /**
     * Set addedAt
     *
     * @ORM\PrePersist()
     *
     * @return null
     */
    public function setAddedAt()
    {
        $this->addedAt = new \DateTime("now");
    }

    /**
     * Get addedAt
     *
     * @return \DateTime
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * Set sendedAt
     *
     * @param \DateTime $sendedAt
     *
     * @return History
     */
    public function setSendedAt($sendedAt)
    {
        $this->sendedAt = $sendedAt;

        return $this;
    }

    /**
     * Get sendedAt
     *
     * @return \DateTime
     */
    public function getSendedAt()
    {
        return $this->sendedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return History
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ERROR     => 'error',
            self::STATUS_PENDING   => 'pending',
            self::STATUS_DELIVERED => 'delivered',
        ];
    }
}
