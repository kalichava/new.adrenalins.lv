<?php

namespace Adrenalins\PlaceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PlaceType
 *
 * @package Adrenalins\EventsBundle\Form\Type
 */
class PlaceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                null,
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'user.places.manage.form.placeholder_title'
                    ]
                ]
            )
            ->add(
                'description',
                'textarea',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'rows'        => 3,
                        'placeholder' => 'user.places.manage.form.placeholder_description'
                    ]
                ]
            )
            ->add(
                'latitude',
                'text',
                [
                    'attr'     => [
                        'placeholder' => 'user.places.manage.form.placeholder_latitude'
                    ],
                    'label'    => false,
                    'required' => true
                ]
            )
            ->add(
                'longitude',
                'text',
                [
                    'attr'     => [
                        'placeholder' => 'user.places.manage.form.placeholder_longitude'
                    ],
                    'label'    => false,
                    'required' => true
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Adrenalins\PlaceBundle\Entity\Place'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'place';
    }
}