<?php

namespace Adrenalins\PlaceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Places admin
 */
class PlaceAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('title', null, ['label' => 'Title'])
            ->add('latitude', null, ['label' => 'Lat'])
            ->add('longitude', null, ['label' => 'Long'])
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, ['label' => 'Title'])
            ->add('latitude', null, ['label' => 'Latitude'])
            ->add('longitude', null, ['label' => 'Longitude'])
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => true])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'author',
                null,
                [
                    'label'    => 'Author',
                    'required' => false
                ]
            );

        $i18nFields = [
            'title'   => [
                'required' => true,
                'label'    => 'Title',
            ],
            'description' => [
                'required' => true,
                'attr'     => [
                    'class' => 'tinymce'
                ],
                'label'    => 'Description'
            ]
        ];

        $formMapper
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                [
                    'label'              => 'Translations',
                    'translatable_class' => 'Adrenalins\PlaceBundle\Entity\Place',
                    'fields'             => $i18nFields
                ]
            )
            ->add(
                'latitude',
                null,
                [
                    'label' => 'Latitude'
                ]
            )
            ->add(
                'longitude',
                null,
                [
                    'label' => 'Longitude'
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label' => 'Is Active'
                ]
            );
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setAuthor(
            $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()
        );

        return $instance;
    }
}