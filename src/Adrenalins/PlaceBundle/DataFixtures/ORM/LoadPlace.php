<?php

namespace Adrenalins\PlaceBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Adrenalins\PlaceBundle\Entity\Place;
use Adrenalins\PlaceBundle\Entity\PlaceTranslation;
use Faker;

/**
 * Class LoadPlace
 *
 * @package Adrenalins\PlaceBundle\DataFixtures\ORM
 */
class LoadPlace extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = ['ru', 'en'];
        $placesData = $this->getPlacesData();

        foreach ($placesData as $placeData) {

            $place = new Place();
            $place->setTitle($placeData['title']);
            $place->setDescription($placeData['description']);
            $place->setLatitude($placeData['latitude']);
            $place->setLongitude($placeData['longitude']);
            $place->setIsActive(true);

            foreach($locales as $locale) {
                $place->addTranslation(
                    new PlaceTranslation($locale, 'title', $placeData['title'])
                );

                $place->addTranslation(
                    new PlaceTranslation($locale, 'description', $placeData['description'])
                );
            }

            $manager->persist($place);
            $manager->flush();
        }

    }

    private function getPlacesData()
    {
        $faker = Faker\Factory::create();

        return [
            [
                'title'       => 'Krustpils',
                'description' => $faker->text(),
                'latitude'    => '56.53485000',
                'longitude'   => '25.88854800'
            ],
            [
                'title'       => 'Babite',
                'description' => $faker->text(),
                'latitude'    => '56.93347800',
                'longitude'   => '23.73815100'
            ],
            [
                'title'       => 'Sabile',
                'description' => $faker->text(),
                'latitude'    => '57.04880100',
                'longitude'   => '22.57240700'
            ],
            [
                'title'       => 'Jurkalne',
                'description' => $faker->text(),
                'latitude'    => '56.75442300',
                'longitude'   => '21.06107000'
            ],
            [
                'title'       => 'Rumbula',
                'description' => $faker->text(),
                'latitude'    => '56.88851500',
                'longitude'   => '24.21280700'
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}