<?php

namespace Adrenalins\PlaceBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="places_translations",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="locale_translation_unique_idx", columns={
 *          "locale", "object_id", "field"
 *      })}
 * )
 */
class PlaceTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $object;

    /**
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($locale = null, $field = null, $value = null)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }
}