<?php

namespace Adrenalins\PlaceBundle\Entity;

use Adrenalins\NewsBundle\Entity\News;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Adrenalins\UserBundle\Entity\User;
use Adrenalins\EventsBundle\Entity\Event;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Adrenalins\PlaceBundle\Entity\PlaceTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="places")
 * @Gedmo\TranslationEntity(class="Adrenalins\PlaceBundle\Entity\PlaceTranslation")
 * @ORM\HasLifecycleCallbacks
 */
class Place
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    protected $author;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=8, nullable=false)
     */
    protected $longitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8, nullable=false)
     */
    protected $latitude;

    /**
     * @ORM\OneToMany(targetEntity="PlaceTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var Collection
     */
    protected $translations;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\NewsBundle\Entity\News")
     */
    protected $news;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\EventsBundle\Entity\Event")
     */
    protected $events;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    protected $videos;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
     */
    protected $galleries;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    protected $isActive = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Place
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Place
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return (string) $this->description;
    }

    /**
     * Set author
     *
     * @param User $author
     *
     * @return Place
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Place
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Place
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Add translations
     *
     * @param PlaceTranslation $trans
     *
     * @return Place
     */
    public function addTranslation(PlaceTranslation $trans)
    {
        if (!$this->translations->contains($trans)) {
            $trans->setObject($this);
            $this->translations->add($trans);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param PlaceTranslation $translations
     */
    public function removeTranslation(PlaceTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Place
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add galleries
     *
     * @param Gallery $galleries
     * @return Place
     */
    public function addGallery(Gallery $galleries)
    {
        if(!$this->galleries->contains($galleries)) {
            $this->galleries[] = $galleries;
        }

        return $this;
    }

    /**
     * Remove galleries
     *
     * @param Gallery $galleries
     */
    public function removeGallery(Gallery $galleries)
    {
        $this->galleries->removeElement($galleries);
    }

    /**
     * Has galleries
     *
     * @return boolean
     */
    public function hasGalleries()
    {
        return count($this->galleries);
    }

    /**
     * Get galleries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * Add videos
     *
     * @param Media $videos
     * @return Place
     */
    public function addVideo(Media $videos)
    {
        if(!$this->videos->contains($videos)) {
            $this->videos[] = $videos;
        }

        return $this;
    }

    /**
     * Remove videos
     *
     * @param Media $videos
     */
    public function removeVideo(Media $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Has videos
     *
     * @return boolean
     */
    public function hasVideos()
    {
        return count($this->videos);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add events
     *
     * @param Event $events
     * @return Place
     */
    public function addEvent(Event $events)
    {
        if(!$this->events->contains($events)) {
            $this->events[] = $events;
        }

        return $this;
    }

    /**
     * Remove events
     *
     * @param Event $events
     */
    public function removeEvent(Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add news
     *
     * @param News $news
     * @return Place
     */
    public function addNews(News $news)
    {
        if(!$this->news->contains($news)) {
            $this->news[] = $news;
        }

        return $this;
    }

    /**
     * Remove news
     *
     * @param News $news
     */
    public function removeNews(News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNews()
    {
        return $this->news;
    }
}
