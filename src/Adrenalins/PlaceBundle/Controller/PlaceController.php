<?php

namespace Adrenalins\PlaceBundle\Controller;

use Adrenalins\PlaceBundle\Entity\Place;
use Adrenalins\PlaceBundle\Entity\PlaceTranslation;
use Adrenalins\PlaceBundle\Form\Type\PlaceType;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class PlaceController
 *
 * @package Adrenalins\PlaceBundle\Controller
 */
class PlaceController extends Controller
{
    /**
     * Show single place
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');
        $page = $this->getPage();

        /** @var Place $place */
        $place = $dm->getRepository('PlaceBundle:Place')->find($id);
        if (!$place) {
            throw $this->createNotFoundException('Not found place by id - ' . $id);
        }

        return $this->render(
            'PlaceBundle::show.html.twig',
            [
                'page'  => $page,
                'place' => $place
            ]
        );
    }

    /**
     * @return Response
     */
    public function placesAction()
    {
        $em = $this->getDoctrine();
        $page = $this->getPage();
        $translator = $this->get('translator');

        $places = $em
            ->getRepository('PlaceBundle:Place')
            ->findBy(
                [
                    'isActive' => true
                ]
            );

        $jsonPlaces = [];
        /** @var Place $place */
        foreach ($places as $place) {
            $newsCount = count($place->getNews());
            $videosCount = count($place->getVideos());
            $galleriesCount = count($place->getGalleries());

            $jsonPlaces[] = [
                $this->generateUrl('adrenalins_places_show', ['id' => $place->getId()], true),
                $place->getTitle(),
                $place->getLatitude(),
                $place->getLongitude(),
                $place->getId(),
                $place->getDescription(),
                $translator->trans('places.show.map.tooltip.galleries'),
                $galleriesCount,
                $translator->trans('places.show.map.tooltip.videos'),
                $videosCount,
                $translator->trans('places.show.map.tooltip.news'),
                $newsCount,
            ];
        }

        return $this->render(
            'PlaceBundle:Club:places.html.twig',
            [
                'page'       => $page,
                'jsonPlaces' => json_encode($jsonPlaces)
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $id = (int) $request->get('id');
        $page = null;

        if ($id) {
            /** @var Place $place */
            $place = $dm
                ->getRepository('PlaceBundle:Place')
                ->findOneBy(
                    [
                        'id'     => $id,
                        'author' => $user
                    ]
                );

            if (!$place) {
                throw $this->createNotFoundException('Not found place by id - ' . $id);
            }
        } else {
            $page = $this->getPage();

            $place = new Place();
            $place->setAuthor($user);
            $place->setIsActive(true);
        }

        $form = $this->createForm(new PlaceType(), $place);

        if ($request->isMethod("POST")) {
            $form->submit($request);

            if ($form->isValid()) {
                $manager = $dm->getManager();

                $locales = array_diff(
                    $this->container->getParameter('available_locales'),
                    [$this->container->getParameter('locale')]
                );

                if ($place->getCreatedAt()) {
                    /** @var PlaceTranslation $trans */
                    foreach ($place->getTranslations() as $trans) {
                        $trans->setContent($form[$trans->getField()]->getData());
                        $manager->persist($trans);
                    }

                    $redirectUrl = $this->generateUrl('adrenalins_user_places_edit', ['id' => $id]);
                    $flashMessage = 'places.flash.edited';
                } else {
                    foreach ($locales as $locale) {
                        $place->addTranslation(
                            new PlaceTranslation(
                                $locale,
                                'title',
                                $form['title']->getData()
                            )
                        );

                        $place->addTranslation(
                            new PlaceTranslation(
                                $locale,
                                'description',
                                $form['description']->getData()
                            )
                        );
                    }

                    $redirectUrl = $this->generateUrl('adrenalins_user_places_create');
                    $flashMessage = 'places.flash.created';
                }

                $manager->persist($place);
                $manager->flush();

                $this->get('session')->getFlashBag()->set('success', $flashMessage);

                return new RedirectResponse($redirectUrl . '#content-table');
            } else {
                $flashMessage = 'places.flash.error';
                $formErrors = $this->getErrorMessages($form);
                $this->get('session')->getFlashBag()->set('error', $flashMessage);
            }
        }

        /** @var Place $places */
        $places = $dm
            ->getRepository('PlaceBundle:Place')
            ->findBy(
                [
                    'author'   => $user,
                    'isActive' => true
                ],
                [
                    'createdAt' => 'DESC'
                ]
            );

        return $this->render(
            'PlaceBundle:Create:index.html.twig',
            [
                'id'         => $id,
                'page'       => $page,
                'user'       => $user,
                'form'       => $form->createView(),
                'places'     => $places,
                'latitude'   => ($place->getId()) ? $place->getLatitude() : '56.888515',
                'longitude'  => ($place->getId()) ? $place->getLongitude() : '24.212807',
                'formErrors' => (isset($formErrors) ? $formErrors : null)
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');

        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var Place $news */
        $place = $dm
            ->getRepository('PlaceBundle:Place')
            ->findOneBy(
                [
                    'id'     => $id,
                    'author' => $user
                ]
            );

        if (!$place) {
            throw $this->createNotFoundException('Not found places by id - ' . $id);
        }

        $place->setIsActive(false);

        $dm->getManager()->flush();

        $this->get('session')->getFlashBag()->set('notice', 'places.flash.deleted');

        return new RedirectResponse($this->generateUrl('adrenalins_user_places_create'));
    }

    /**
     * @return Page
     */
    private function getPage()
    {
        /** @var PageRepository $pageRepo */
        $pageRepo = $this->getDoctrine()->getRepository('PageBundle:Page');

        $pageSlug = 'places';
        $parentPageSlug = 'club';

        /** @var Page $parent */
        $parent = $pageRepo->findOneBySlug($parentPageSlug);

        if (!$parent) {
            throw $this->createNotFoundException('Not found parent page by slug - ' . $parentPageSlug);
        }

        /** @var Page $page */
        $page = $pageRepo->findActivePage($pageSlug, $parent);

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        return $page;
    }

    /**
     * @param Form $form
     *
     * @return array
     */
    private function getErrorMessages(Form $form)
    {
        $errors = array();

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }
}
