<?php

namespace Adrenalins\EmailTemplateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Email templates admin
 */
class EmailTemplateAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('keyword')
            ->add('description')
            ->add('subject');
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('keyword')
            ->add('description')
            ->add('subject')
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit' => [],
                    ],
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $emailTemplate = $this->getSubject();

        $help = '';
        $variables = unserialize($emailTemplate->getVariables());
        if ($variables) {
            $help = '<b>Available variables</b>:<br />'.implode('<br />', $variables);
        }

        $i18nFields = [
            'subject' => [

            ],
            'content' => [
                'attr' => ['class' => 'tinymce'],
            ],
        ];

        $formMapper
            ->add(
                'keyword',
                null,
                [
                    'disabled' => $this->isDebugMode() ? false : true,
                ]
            )
            ->add(
                'description',
                null,
                [
                    'disabled' => $this->isDebugMode() ? false : true,
                ]
            );

        $formMapper
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                [
                    'translatable_class' => 'Adrenalins\EmailTemplateBundle\Entity\EmailTemplate',
                    'fields'             => $i18nFields,
                ]
            );

        $formMapper->setHelps(['translations' => $help]);
    }

    /**
     * {@inheritDoc}
     */
    public function getBatchActions()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function configureRoutes(RouteCollection $route)
    {
        $route->remove('delete');

        if (!$this->isDebugMode()) {
            $route->remove('create');
        }
    }

    /**
     * Is debug mode enabled
     *
     * @return bool
     */
    private function isDebugMode()
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->getParameter('kernel.debug');
    }
}