<?php

namespace Adrenalins\EmailTemplateBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping AS ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="email_templates", indexes={
 *      @ORM\Index(name="email_template_key_idx", columns={"keyword"})
 * })
 * @Gedmo\TranslationEntity(class="Adrenalins\EmailTemplateBundle\Entity\EmailTemplateTranslation")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("keyword")
 */
class EmailTemplate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="keyword", type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max="255"
     * )
     */
    protected $keyword;

    /**
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max="255"
     * )
     */
    protected $description;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="subject", type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max="255"
     * )
     */
    protected $subject;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank
     */
    protected $content;

    /**
     * @ORM\Column(name="variables", type="string", length=255, nullable=true)
     */
    protected $variables;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="EmailTemplateTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return EmailTemplate
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return EmailTemplate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailTemplate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailTemplate
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set variables
     *
     * @param string $variables
     * @return EmailTemplate
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * Get variables
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '' . $this->description;
    }

    /**
     * Set translations
     *
     * @param ArrayCollection $translations
     * @return Faq
     */
    public function setTranslations(ArrayCollection $translations)
    {
        $this->translations = $translations;

        return $this;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Adds translations.
     *
     * @param EmailTemplateTranslation $translation
     *
     * @return $this
     */
    public function addTranslation(EmailTemplateTranslation $translation)
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setObject($this);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Adrenalins\EmailTemplateBundle\Entity\EmailTemplateTranslation $translations
     */
    public function removeTranslation(\Adrenalins\EmailTemplateBundle\Entity\EmailTemplateTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }
}