<?php

namespace Adrenalins\FeedbackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Adrenalins\FeedbackBundle\Form\Type\SmsType;
use Adrenalins\FeedbackBundle\Form\Type\FeedbackType;
use Adrenalins\NewsletterBundle\Entity\Contact;

/**
 * Class DefaultController
 *
 * @package Adrenalins\FeedbackBundle\Controller
 */
class FeedbackController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response|static
     */
    public function smsAction(Request $request)
    {
        $status = false;
        $message = '';

        $emails = $this->get('adrenalins.settings')
            ->get('feedback-school-mails-to', 'krisjanis@adrenalins.lv,mihail@adrenalins.lv');

        $emailsTo = explode(',', $emails);

        $form = $this->createForm(new SmsType(), null, ['csrf_protection' => false]);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            $vars = [
                'type' => 'School',
                'vars' => [
                    'smsId' => $data['email']
                ]
            ];

            $message = $this->generateMessage('Adrenalins.lv - SMS School', $emailsTo, $vars);

            try {
                $this->createSmsContact('School', $data['email']);

                $status = $this->get('mailer')->send($message);
                $message = $this->get('translator')->trans('sms.message.sent.successfully');
            } catch (\Exception $e) {
                $status = false;
            }
        }

        return JsonResponse::create(
            [
                'success' => $status,
                'message' => $message
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response|static
     */
    public function sendAction(Request $request)
    {
        $type = $request->get('type', 'school');
        $options = [];

        if (!in_array($type, ['club', 'school', 'tandem', 'paramotors'])) {
            throw $this->createNotFoundException('Not found type - ' . $type);
        }

        if ($type == 'tandem') {
            $placesSetting = $this->getDoctrine()
                ->getRepository('SettingsBundle:Setting')
                ->findOneByKey('feedback-tandem-places');

            if ($placesSetting) {
                $placesValues = json_decode($placesSetting->getValue(), true);
                if (!empty($placesValues)) {
                    $options['places'] = $placesValues['options'];
                }
            }
        }

        $form = $this->createForm(new FeedbackType($type, $options), null, ['csrf_protection' => false]);
        $form->handleRequest($request);

        $status = false;
        $message = '';

        if ($form->isValid()) {
            list($message, $status) = call_user_func_array([$this, 'process' . ucfirst($type)], [$form->getData()]);
        }

        return JsonResponse::create(
            [
                'success' => $status,
                'message' => $message
            ]
        );
    }

    /**
     * Process club feedback
     *
     * @param array $data Form Data
     *
     * @return array
     */
    private function processClub($data)
    {
        $emails = $this->get('adrenalins.settings')
            ->get('feedback-school-mails-to', 'krisjanis@adrenalins.lv,mihail@adrenalins.lv');

        $emailsTo = explode(',', $emails);

        $vars = [
            'type' => 'Club',
            'vars' => [
                'email'    => $data['email'],
                'smsId'    => $data['smsId'],
                'fullName' => (!is_null($data['fullname'])) ? $data['fullname'] : $data['smsFullName']
            ]
        ];

        if(is_null($data['email']) && !is_null($data['smsFullName']) && !is_null($data['smsId'])) {
            $this->createSmsContact('Club', $data['smsId'], $data['smsFullName']);
        } else {
            $this->createContact('Club', $data['email'], $data['fullname']);
        }

        $message = $this->generateMessage('Adrenalins.lv - Club', $emailsTo, $vars);

        try {
            $status = $this->get('mailer')->send($message);
            $message = $this->get('translator')->trans('feedback.club.message.sent.successfully');
        } catch (\Exception $e) {
            $status = false;
        }

        return [$message, $status];
    }

    /**
     * Process paramotors feedback
     *
     * @param array $data Form Data
     *
     * @return array
     */
    private function processParamotors($data)
    {
        $emails = $this->get('adrenalins.settings')
            ->get('feedback-school-mails-to', 'krisjanis@adrenalins.lv,mihail@adrenalins.lv');

        $emailsTo = explode(',', $emails);

        $vars = [
            'type' => 'Paramotors',
            'vars' => [
                'email' => $data['email']
            ]
        ];

        $this->createContact('Paramotors', $data['email']);

        $message = $this->generateMessage('Adrenalins.lv - Paramotors', $emailsTo, $vars);

        try {
            $status = $this->get('mailer')->send($message);
            $message = $this->get('translator')->trans('feedback.paramotors.message.sent.successfully');
        } catch (\Exception $e) {
            $status = false;
        }

        return [$message, $status];
    }

    /**
     * Process tandem feedback
     *
     * @param array $data Form Data
     *
     * @return array
     */
    private function processTandem($data)
    {
        $email = $this->get('adrenalins.settings')->get('feedback-tandem-mails-to', 'dzintars@adrenalins.lv');

        $vars = [
            'type' => 'Tandem',
            'vars' => [
                'email'    => $data['email'],
                'fullName' => $data['fullname']
            ]
        ];

        $this->createContact('Tandem', $data['email'], $data['fullname']);

        $message = $this->generateMessage('Adrenalins.lv - Tandem', [$email], $vars);

        try {
            $status = $this->get('mailer')->send($message);
            $message = $this->get('translator')->trans('feedback.tandem.message.sent.successfully');
        } catch (\Exception $e) {
            $status = false;
        }

        return [$message, $status];
    }

    /**
     * Process school feedback
     *
     * @param array $data Form Data
     *
     * @return array
     */
    private function processSchool($data)
    {
        $emails = $this->get('adrenalins.settings')
            ->get('feedback-school-mails-to', 'krisjanis@adrenalins.lv,mihail@adrenalins.lv');

        $emailsTo = explode(',', $emails);

        $vars = [
            'type' => 'School',
            'vars' => [
                'age'      => $data['age'],
                'phone'    => $data['phone'],
                'email'    => $data['email'],
                'fullName' => $data['fullname']
            ]
        ];

        $this->createContact('School', $data['email'], $data['fullname'], $data['age'], $data['phone']);

        $message = $this->generateMessage('Adrenalins.lv - School', $emailsTo, $vars);

        try {
            $status = $this->get('mailer')->send($message);
            $message = $this->get('translator')->trans('feedback.school.message.sent.successfully');
        } catch (\Exception $e) {
            $status = false;
        }

        return [$message, $status];
    }

    /**
     * Generate message
     *
     * @param string $subject Mail Subject
     * @param array  $emails  Emails
     * @param array  $vars    Variables
     *
     * @return \Swift_Mime_MimePart
     */
    private function generateMessage($subject, array $emails, array $vars)
    {
        $body = $this->renderView('FeedbackBundle:Mail:feedback.txt.twig', $vars);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('info@adrenalins.lv')
            ->setTo($emails)
            ->setBody($body);

        return $message;
    }

    /**
     * Create new newsletter contact
     *
     * @param string $groupName
     * @param string $email
     * @param string $fullName
     * @param string $age
     * @param string $phone
     *
     * @return bool
     */
    private function createContact($groupName, $email, $fullName = '', $age = '', $phone = '')
    {
        $dm = $this->getDoctrine();
        $group = $dm->getRepository('NewsletterBundle:ContactGroup')->findOneByName($groupName);

        $contact = new Contact();

        $contact
            ->setAge($age)
            ->setEmail($email)
            ->setPhone($phone)
            ->setFullName(trim($fullName))
            ->setIsActive(true);

        $contact->addGroup($group);

        $em = $dm->getManager();
        $em->persist($contact);
        $em->flush();

        return true;
    }

    /**
     * Create new contact in SMS system
     * 
     * @return bool
     */
    private function createSmsContact($groupName, $smsId, $fullName = '')
    {
        switch ($groupName) {
            case 'School':
                $groupId = 4; // Studenti
                break;

            case 'Club':
                $groupId = 2; // Piloti
                break;
            
            default:
                $groupId = 4; // Studenti
                break;
        }

        $dbh = new \PDO(
            'mysql:host=localhost;dbname='. $this->container->getParameter('sms_database_name'),
            $this->container->getParameter('sms_database_user'),
            $this->container->getParameter('sms_database_password'),
            [
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ]
        );

        $contact = $dbh->prepare("INSERT INTO students (`name`, `sms_email`, `group_id`, `date_created`) VALUES (:name, :sms_email, :group_id, :created_at)");
        $contact->execute([':name' => $fullName, ':sms_email' => $smsId, ':group_id' => $groupId, ':created_at' => date('Y-m-d H:i:s')]);

        return true;
    }
}
