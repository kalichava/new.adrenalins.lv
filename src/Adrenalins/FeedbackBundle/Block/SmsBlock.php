<?php

namespace Adrenalins\FeedbackBundle\Block;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Response;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Adrenalins\FeedbackBundle\Form\Type\SmsType;

/**
 * Class SmsBlock
 *
 * @package Adrenalins\FeedbackBundle\Block
 */
class SmsBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param FormFactory     $formFactory
     */
    public function __construct(EngineInterface $templating, FormFactory $formFactory)
    {
        $this->templating = $templating;
        $this->formFactory = $formFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $form = $this->formFactory->create(new SmsType());

        return $this->renderResponse(
            $blockContext->getTemplate(),
            [
                'form'  => $form->createView(),
                'block' => $blockContext->getBlock()
            ],
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'FeedbackBundle:Block:sms.html.twig'
            ]
        );
    }
}