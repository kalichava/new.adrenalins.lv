<?php

namespace Adrenalins\FeedbackBundle\Block;

use Adrenalins\SettingsBundle\Entity\Setting;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Adrenalins\FeedbackBundle\Form\Type\FeedbackType;

/**
 * Class FeedbackBlock
 *
 * @package Adrenalins\FeedbackBundle\Block
 */
class FeedbackBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param FormFactory     $formFactory
     * @param EntityManager   $entityManager
     */
    public function __construct(EngineInterface $templating, FormFactory $formFactory, EntityManager $entityManager)
    {
        $this->templating = $templating;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $attr = $blockContext->getSetting('attr');
        $type = (array_key_exists('type', $attr)) ? $attr['type'] : 'school';
        $places = [];
        $template = sprintf('FeedbackBundle:Block:%s.html.twig', $type);

        if ($type == 'tandem') {

            /** @var Setting $placesSetting */
            $placesSetting = $this->entityManager
                ->getRepository('SettingsBundle:Setting')
                ->findOneByKey('feedback-tandem-places');

            if ($placesSetting) {
                $placesValues = json_decode($placesSetting->getValue(), true);
                if (!empty($placesValues)) {
                    $places = $placesValues['options'];
                }
            }
        }

        $form = $this->formFactory->create(new FeedbackType($type, ['places' => $places]));

        return $this->renderResponse(
            $template,
            [
                'form'  => $form->createView(),
                'block' => $blockContext->getBlock()
            ],
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'FeedbackBundle:Block:feedback.html.twig'
            ]
        );
    }
}