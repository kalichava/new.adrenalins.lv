<?php

namespace Adrenalins\FeedbackBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FeedbackType
 *
 * @package Adrenalins\FeedbackBundle\Form\Type
 */
class FeedbackType extends AbstractType
{
    private $type;
    private $options;

    /**
     * @param string $type
     * @param array  $options
     */
    public function __construct($type, $options)
    {
        $this->type = $type;
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                'email',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'feedback.' . $this->type . '.form.placeholder_email'
                    ]
                ]
            )
            ->add(
                'captcha',
                'genemu_captcha',
                [
                    'attr' => [
                        'class' => 'capcha'
                    ]
                ]
            );

        // Only paramotors without fullName field

        if ($this->type != 'paramotors') {
            $builder->add(
                'fullname',
                null,
                [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'maxlength'   => 50,
                        'placeholder' => 'feedback.' . $this->type . '.form.placeholder_fullname'
                    ]
                ]
            );
        }

        if ($this->type == 'school') {
            $builder->add(
                'age',
                null,
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'class'       => 'one-half',
                        'maxlength'   => 2,
                        'placeholder' => 'feedback.' . $this->type . '.form.placeholder_age'
                    ]
                ]
            );

            $builder->add(
                'phone',
                null,
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'maxlength'   => 15,
                        'placeholder' => 'feedback.' . $this->type . '.form.placeholder_phone'
                    ]
                ]
            );
        } elseif ($this->type == 'club') {
            $builder
                ->add(
                    'smsCaptcha',
                    'genemu_captcha',
                    [
                        'attr' => [
                            'class' => 'capcha'
                        ]
                    ]
                )
                ->add(
                    'smsId',
                    'text',
                    [
                        'label'    => false,
                        'required' => true,
                        'attr'     => [
                            'placeholder' => 'feedback.' . $this->type . '.form.placeholder_sms_id'
                        ]
                    ]
                )
                ->add(
                    'smsFullName',
                    'text',
                    [
                        'label'    => false,
                        'required' => true,
                        'attr'     => [
                            'maxlength'   => 50,
                            'placeholder' => 'feedback.' . $this->type . '.form.placeholder_fullname'
                        ]
                    ]
                );

            $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSetClubData']);
        }

    }

    /**
     * Pre set Club two forms data
     *
     * @param FormEvent $event
     */
    public function onPreSetClubData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (!empty($data['fullName']) || !empty($data['email'])) {
            $form->remove('smsCaptcha');
        } elseif (!empty($data['smsId']) || !empty($data['smsFullName'])) {
            $form->remove('captcha');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'virtual'         => false,
                'csrf_protection' => false
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'feedback';
    }
}