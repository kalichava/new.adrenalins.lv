<?php

namespace Adrenalins\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TandemCardType
 *
 * @package Adrenalins\PageBundle\Form\Type
 */
class TandemCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'discountCode',
                'text',
                [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'page.tandem.store.form.discount.field.code',
                    ],
                ]
            )
            ->add(
                'clientFullName',
                'text',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'page.tandem.store.form.field.name',
                    ],
                ]
            )
            ->add(
                'clientEmail',
                'email',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'page.tandem.store.form.field.email',
                    ],
                ]
            )
            ->add(
                'clientPhone',
                'text',
                [
                    'label'    => false,
                    'required' => true,
                    'attr'     => [
                        'placeholder' => 'page.tandem.store.form.field.phone',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Adrenalins\StoreBundle\Entity\TandemTransaction',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'tandem_card';
    }
}