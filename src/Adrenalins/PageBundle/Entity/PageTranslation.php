<?php

namespace Adrenalins\PageBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_translations",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="locale_translation_unique_idx", columns={
 *          "locale", "object_id", "field"
 *      })}
 * )
 */
class PageTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $object;
}