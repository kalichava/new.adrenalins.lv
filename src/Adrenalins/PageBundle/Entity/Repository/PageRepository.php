<?php

namespace Adrenalins\PageBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class PageRepository
 *
 * @package Adrenalins\PageBundle\Entity\Repository
 */
class PageRepository extends EntityRepository
{
    /**
     * Find active page
     *
     * @param string $slug
     * @param object $parent
     *
     * @return array
     */
    public function findActivePage($slug, $parent)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.isActive = true')
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1)
            ->orderBy('p.position');

        if ($parent) {
            $qb
                ->andWhere('p.parent = :parent')
                ->setParameter('parent', $parent->getId());
        } else {
            $qb->andWhere('p.parent IS NULL');
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Find active page
     *
     * @param string $slug
     *
     * @return array
     */
    public function findActivePageWithoutParent($slug)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.isActive = :active')
            ->andWhere('p.slug = :slug')
            ->andWhere('p.parent IS NULL')
            ->setParameters(
                [
                    'slug'   => $slug,
                    'active' => true
                ]
            )
            ->setMaxResults(1)
            ->orderBy('p.position');

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Find active pages by parent
     *
     * @param integer $parentId Parent page id
     *
     * @return array
     */
    public function findActivePagesByParent($parentId)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.isActive = true')
            ->andWhere('p.parent = :parent')
            ->setParameter('parent', $parentId)
            ->orderBy('p.position');

        return $qb->getQuery()->execute();
    }
}
