<?php

namespace Adrenalins\PageBundle\Admin;

use Adrenalins\PageBundle\Entity\Page;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Pix\SortableBehaviorBundle\Services\PositionHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PageAdmin
 *
 * @package Adrenalins\PageBundle\Admin
 */
class PageAdmin extends Admin
{
    /** @var int */
    public $last_position = 0;

    /** @var ContainerInterface */
    private $container;

    /** @var PositionHandler */
    private $positionService;

    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'position',
        '_sort_order' => 'ASC',
    ];

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->last_position = $this->positionService->getLastPosition($this->getRoot()->getClass());

        $listMapper
            ->addIdentifier('slug')
            ->add('title')
            ->add('parent')
            ->add(
                'priority',
                null,
                [
                    'template' => 'PageBundle:PageAdmin:_sort.html.twig',
                ]
            )
            ->add('layout')
            ->add(
                'isActive',
                null,
                [
                    'editable' => true
                ]
            )
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => []
                    ]
                ]
            );
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'title',
                'doctrine_orm_callback',
                [
                    'callback' => [$this, 'getTitleFilter']
                ]
            )
            ->add(
                'slug',
                'doctrine_orm_callback',
                [
                    'callback' => [$this, 'getSlugFilter']
                ]
            )
            ->add('layout')
            ->add('isActive');
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();

        $i18nFields = [
            'title'           => [
                'field_type' => 'text'
            ],
            'content'         => [
                'field_type' => 'textarea',
                'attr'       => [
                    'class' => 'tinymce'
                ]
            ],
            'leftSideContent' => [
                'field_type' => ($object->getLayout() == '2columns') ? 'textarea' : 'hidden',
                'attr'       => [
                    'class'  => 'tinymce',
                    'hidden' => ($object->getLayout() == 'default')
                ]
            ],
            'seo_keywords'    => [
                'field_type' => 'text',
                'required'   => false
            ],
            'seo_description' => [
                'field_type' => 'textarea',
                'required'   => false
            ]
        ];

        if ($this->getConfigurationPool()->getContainer()->getParameter(
                'Adrenalins.page.admin.show_seo_fields'
            ) == false
        ) {
            $i18nFields['seo_keywords']['field_type'] = 'hidden';
            $i18nFields['seo_description']['field_type'] = 'hidden';
        }

        $em = $this->modelManager->getEntityManager('PageBundle:Page');
        $query = $em
            ->createQueryBuilder('p')
            ->select('p')
            ->from('PageBundle:Page', 'p');

        $parentQuery = clone $query;
        $parentQuery->where('p.parent IS NULL')->orderBy('p.position', 'ASC');

        $cover = $object->getCover();
        $coverUrl = $cover ? sprintf('<a href="%s" target="_blank">%s</a>', $cover, $cover) : '';

        $formMapper
            ->add(
                'slug',
                'text',
                [
                    'required' => true
                ]
            )
            ->add(
                'file',
                'file',
                [
                    'required' => false,
                    'help' => $coverUrl
                ]
            )
            ->add(
                'parent',
                'sonata_type_model',
                [
                    'query'       => $parentQuery,
                    'required'    => false,
                    'empty_value' => '',
                    'btn_add'     => false
                ]
            )
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                [
                    'required'           => true,
                    'translatable_class' => 'Adrenalins\PageBundle\Entity\Page',
                    'fields'             => $i18nFields
                ]
            )
            ->add(
                'layout',
                'choice',
                [
                    'choices' => [
                        'default'  => 'default',
                        '2columns' => '2columns'
                    ],
                    'data'    => $object->getLayout()
                ]
            )
            ->add('isActive');
    }

    public function getTitleFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }

        /**
         * @var $queryBuilder QueryBuilder
         */
        $queryBuilder
            ->leftJoin(sprintf('%s.translations', $alias), 't')
            ->where('t.field = :titleField')
            ->andWhere('t.content LIKE :value')
            ->setParameters(['titleField' => 'title', 'value' => '%' . $value['value'] . '%']);
    }

    public function getSlugFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }

        /**
         * @var $queryBuilder QueryBuilder
         */
        $queryBuilder
            ->leftJoin(sprintf('%s.translations', $alias), 's')
            ->where('s.field = :slugField')
            ->andWhere('s.content LIKE :value')
            ->setParameters(['slugField' => 'slug', 'value' => '%' . $value['value'] . '%']);
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    /**
     * @param Page $object
     *
     * @return mixed|void
     */
    public function preUpdate($object)
    {
        $object->upload();
    }

    /**
     * @param Page $object
     *
     * @return mixed|void
     */
    public function prePersist($object)
    {
        $object->upload();
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param PositionHandler $positionHandler
     */
    public function setPositionService(PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return [];
    }
}
