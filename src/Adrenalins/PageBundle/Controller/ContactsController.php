<?php

namespace Adrenalins\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Adrenalins\PageBundle\Entity\Page;

/**
 * Class ContactsController
 *
 * @package Adrenalins\PageBundle\Controller
 */
class ContactsController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $pageSlug = 'contacts';

        /** @var Page $page */
        $page = $em->getRepository('PageBundle:Page')
            ->findActivePage($pageSlug, '');

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        $users = $em
            ->getRepository('UserBundle:User')
            ->findVisibleInContacts();

        return $this->render(
            'PageBundle:Contacts:index.html.twig',
            [
                'page'  => $page,
                'users' => $users
            ]
        );
    }
}