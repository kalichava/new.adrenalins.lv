<?php

namespace Adrenalins\PageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpKernel\Exception\FlattenException;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class CustomExceptionController
 *
 * @package Adrenalins\PageBundle\Controller
 */
class CustomExceptionController extends ExceptionController
{
    protected $container;

    /**
     * @param Request              $request
     * @param FlattenException     $exception
     * @param DebugLoggerInterface $logger
     * @param string               $_format
     *
     * @return RedirectResponse|Response
     */
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null, $_format = 'html')
    {
        $code = $exception->getStatusCode();
        if($code == 404) {
            $session = $this->container->get('session');
            $translator = $this->container->get('translator');

            $session->getFlashBag()->add('notice', $translator->trans('error.404.message'));

            return new RedirectResponse($this->container->get('router')->generate('adrenalins_homepage', [], UrlGeneratorInterface::ABSOLUTE_PATH));
        }

        return parent::showAction($request, $exception, $logger, $_format);
    }

    public function setContainer($container)
    {
        $this->container = $container;
    }
}