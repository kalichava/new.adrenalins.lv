<?php

namespace Adrenalins\PageBundle\Controller;

use Hashids\Hashids;
use Symfony\Component\Form\Form;
use FOS\UserBundle\Model\UserInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Adrenalins\StoreBundle\Entity\TandemTransaction;
use Adrenalins\StoreBundle\Entity\TandemDiscountCode;
use Adrenalins\PageBundle\Form\Type\TandemCardType;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;
use Adrenalins\EmailTemplateBundle\Entity\EmailTemplate;
use Adrenalins\NewsletterBundle\Entity\Contact as NewsletterContact;
use Adrenalins\NewsletterBundle\Entity\ContactGroup as NewsletterContactGroup;

/**
 * Class TandemController
 *
 * @package Adrenalins\PageBundle\Controller
 */
class TandemController extends Controller
{
    /**
     * Tandem page output
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        $dm = $this->getDoctrine();

        /** @var PageRepository $repo */
        $repo = $dm->getRepository('PageBundle:Page');
        $parent = null;
        $gallery = null;

        $slug = trim(strip_tags($request->get('slug')));

        $page = $repo->findActivePage($slug, $parent);
        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - '.$slug);
        }

        $content = $page->getContent();

        preg_match('/%%gallery_(.*)%%/iU', $page->getContent(), $match);
        if (!empty($match) && array_key_exists(1, $match)) {
            $galleryId = (int) $match[1];
            if ($galleryId) {
                /** @var Gallery $gallery */
                $gallery = $this->getDoctrine()
                    ->getRepository('ApplicationSonataMediaBundle:Gallery')
                    ->find($galleryId);
            }

            $content = preg_replace('/%%gallery_(.*)%%/iU', '', $content);
        }

        $translator = $this->get('translator');
        $transaction = new TandemTransaction();
        $form = $this->getTandemTransactionForm($transaction);

        // Show status of Tandem card payment
        if ($request->isMethod('POST')) {

            // Security check if request was sent NOT from LatCard
            $orderId = (int) $request->get('ORDER_ID');
            $merchantId = $this->container->getParameter('latcard_merchant_id');
            $latCardToken = sha1($merchantId.$orderId.$request->get('AMOUNT').$request->get('DT'));

            if ($request->get('INDEX') == $latCardToken) {
                if ($request->get('STATUS') == "ERROR" || $request->get('STATUS') == "FAILED") {
                    $status = 'error';
                    $flashMessage = 'page.tandem.store.order.return.status.error';
                } elseif ($request->get('STATUS') == "PRE_AUTH") {
                    $status = 'pending';
                    $flashMessage = 'page.tandem.store.order.return.status.pending';
                } elseif ($request->get('STATUS') == "SUCCEEDED") {
                    /** @var TandemTransaction $successTransaction */
                    $successTransaction = $dm->getRepository('StoreBundle:TandemTransaction')->findOneById($orderId);
                    if ($successTransaction) {
                        $status = 'success';
                        $invoiceUrl = $this->generateUrl(
                            'adrenalins_page_tandem_card_print',
                            [
                                'code' => $successTransaction->getCode(),
                            ]
                        );

                        $flashMessage = $translator->trans(
                            "page.tandem.store.order.return.status.success",
                            [
                                '%url%'  => $invoiceUrl,
                                '%code%' => $successTransaction->getCode(),
                            ]
                        );
                    }
                }

                if (!empty($status) && !empty($flashMessage)) {
                    $this->get('session')->getFlashBag()->set($status, $flashMessage);
                }
            }
        }

        return $this->render(
            'PageBundle:Tandem:show.html.twig',
            [
                'page'    => $page,
                'form'    => $form->createView(),
                'parent'  => $parent,
                'content' => $content,
                'gallery' => $gallery,
            ]
        );
    }

    /**
     * Tandem buy page output
     *
     * @param Request $request
     *
     * @return Response
     */
    public function buyAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $manager = $dm->getManager();
        $translator = $this->get('translator');

        $transaction = new TandemTransaction();
        $transaction->setPaymentDate(new \DateTime('now'));

        $form = $this->getTandemTransactionForm($transaction);
        $form->submit($request);

        if (!$form->isValid()) {
            $formErrors = $this->getErrorMessages($form);
            $flashMessage = 'page.tandem.store.form.error';
            $this->get('session')->getFlashBag()->set('error', $flashMessage);

            return new RedirectResponse($this->generateUrl('adrenalins_page_tandem'));
        }

        $flightQty = 1;

        $hashIds = new Hashids('adrenalins_tandem_cards_store', 12, 'abcdefghij1234567890');
        $transaction->setCode($hashIds->encode(time(), $flightQty));

        $price = $this->calculateTandemCardPriceWithDiscount(
            $dm,
            $transaction,
            (float) $this->get('adrenalins.settings')->get('tandem-card-price'),
            trim(strip_tags($form['discountCode']->getData()))
        );

        $transaction->setAmount($flightQty * $price);

        $manager->persist($transaction);
        $manager->flush();

        $endPoint = $this->container->getParameter('latcard_end_point');
        $soapLogin = $this->container->getParameter('latcard_soap_login');
        $soapPassword = $this->container->getParameter('latcard_soap_password');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $soapLogin.':'.$soapPassword);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpCode == '200') {

            $orderDescription = $translator
                ->trans(
                    'page.tandem.store.order_description',
                    [
                        '%qty%'  => $flightQty,
                        '%code%' => $transaction->getCode(),
                    ]
                );

            $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><PayReqOne/>');
            $xml->addChild('ORDER_ID', $transaction->getId());
            $xml->addChild('ORDER_DESC', $orderDescription);
            $xml->addChild('FIRST_NAME', $transaction->getClientFirstName());
            $xml->addChild('LAST_NAME', $transaction->getClientLastName());
            $xml->addChild('ADDRESS', 'Client address');
            $xml->addChild('CITY', 'Client city');
            $xml->addChild('EMAIL', $form['clientEmail']->getData());

            $xml->addChild('AMOUNT', $transaction->getAmount());
            $xml->addChild('MERCHANT', $this->container->getParameter('latcard_merchant_id'));
            $xml->addChild('TERMINAL', $this->container->getParameter('latcard_terminal'));
            $xml->addChild('ZIP_CODE', $this->container->getParameter('latcard_zip_code'));
            $xml->addChild('COUNTRY_CODE', $this->container->getParameter('latcard_country_code'));
            $xml->addChild('CURRENCY', $this->container->getParameter('latcard_currency'));
            $xml->addChild('LANG', $request->getLocale());
            $xml->addChild('TYPE_PAYMENT', $this->container->getParameter('latcard_type_payment'));

            $client = new \SoapClient(
                $endPoint,
                [
                    'trace'      => false,
                    'login'      => $soapLogin,
                    'password'   => $soapPassword,
                    'exceptions' => true,
                ]
            );

            // returns html code with JS redirect to payment page
            return Response::create($client->start($xml->asXML()));
        }

        return new RedirectResponse($this->generateUrl('adrenalins_page_tandem'));
    }

    /**
     * Save transaction after successful LatCard payment
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function latcardTransactionNotifyAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $manager = $dm->getManager();

        $logger = $this->get('adrenalins.payment.logger');

        // Security check if request was sent NOT from LatCard
        $merchantId = $this->container->getParameter('latcard_merchant_id');
        $latCardToken = sha1($merchantId.$request->get('ORDER_ID').$request->get('AMOUNT').$request->get('DT'));

        if ($request->get('INDEX') != $latCardToken) {
            $logger->error("[SECURITY] Request was not from LatCard.", $request->request->all());
            throw new \Exception('Security, something went wrong!');
        }

        $logger->info("[INFO] Payment data.", $request->request->all());

        if (trim(strip_tags($request->get('STATUS'))) != "SUCCEEDED") {
            $logger->error("[ERROR] Something went wrong.", ['status' => $request->get('STATUS')]);
            throw new \Exception('Something went wrong!');
        }

        $orderId = (int) $request->get('ORDER_ID');

        /** @var TandemTransaction $transaction */
        $transaction = $dm->getRepository('StoreBundle:TandemTransaction')->findOneById($orderId);

        if (!$transaction) {
            throw $this->createNotFoundException('Not found Tandem transaction by ORDER_ID - '.$orderId);
        }

        $date = new \DateTime('now');
        $dateTo = clone $date;
        $dateTo->modify("+1 year");

        $transaction->setIsActive(true);
        $transaction->setDateValidFrom($date);
        $transaction->setDateValidTo($dateTo);
        $transaction->setPaymentStatus(TandemTransaction::PAYMENT_STATUS_PAID);

        $manager->persist($transaction);

        /** @var TandemDiscountCode $discount */
        $discount = $dm->getRepository('StoreBundle:TandemDiscountCode')
            ->findOneByCode($transaction->getDiscountCode());

        if ($discount) {
            $discount->setUsedNumTimes($discount->getUsedNumTimes() + 1);

            if ($discount->getUsageQty() == $discount->getUsedNumTimes()) {
                $discount->setIsActive(false);
            }

            $manager->persist($discount);
        }

        // Add new buyer into Tandem newsletter

        /** @var NewsletterContactGroup $newsletterGroup */
        $newsletterGroup = $dm->getRepository('NewsletterBundle:ContactGroup')
            ->findOneByName('Buyers of tandem cards');

        if ($newsletterGroup) {
            /** @var NewsletterContact $newsletterContact */
            $newsletterContact = $dm->getRepository('NewsletterBundle:Contact')
                ->findOneByEmail($transaction->getClientEmail());

            if ($newsletterContact) {
                if (!in_array($newsletterGroup->getId(), $newsletterContact->getGroupIds())) {
                    $newsletterContact->addGroup($newsletterGroup);

                    $manager->persist($newsletterContact);
                }
            } else {
                $newsletterContact = new NewsletterContact;
                $newsletterContact->addGroup($newsletterGroup);
                $newsletterContact->setEmail($transaction->getClientEmail());
                $newsletterContact->setFullName($transaction->getClientFullName());
                $newsletterContact->setPhone($transaction->getClientPhone());
                $newsletterContact->setIsActive(true);

                $manager->persist($newsletterContact);
            }
        }

        $manager->flush();

        // Send email with invoice to client
        $this->sendTandemCardInvoiceEmailToClient($transaction);

        return Response::create(null);
    }

    /**
     * Send invoice email to client.
     *
     * @param TandemTransaction $transaction
     *
     * @return bool|\Swift_Mime_MimePart
     */
    private function sendTandemCardInvoiceEmailToClient(TandemTransaction $transaction)
    {
        $dm = $this->getDoctrine();

        /** @var EmailTemplate $emailTemplate */
        $emailTemplate = $dm
            ->getRepository('EmailTemplateBundle:EmailTemplate')
            ->findOneByKeyword('tandem-store-card-invoice');

        if (!$emailTemplate) {
            return false;
        }

        $body = str_replace(
            unserialize($emailTemplate->getVariables()),
            [
                $transaction->getCode(),
                $this->generateUrl('adrenalins_page_tandem_card_print', ['code' => $transaction->getCode()], true),
                $transaction->getClientFullName(),
            ],
            $emailTemplate->getContent()
        );

        $mailer = $this->get('mailer');

        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject($emailTemplate->getSubject())
            ->setFrom('info@adrenalins.lv')
            ->setTo($transaction->getClientEmail())
            ->setBody($body, 'text/html');

        // Attach Tandem card invoice in PDF format
        $pdf = $this->get('knp_snappy.pdf')
            ->getOutputFromHtml(
                $this->renderView(
                    'PageBundle:Tandem:invoicePdf.html.twig',
                    [
                        'transaction' => $transaction
                    ]
                )
            );

        $attach = \Swift_Attachment::newInstance(
            $pdf,
            'Tandem_Invoice_'.$transaction->getCode().'.pdf',
            'application/pdf'
        );

        $message->attach($attach);

        $status = $mailer->send($message);

        return $status;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function printTandemCardAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $orderCode = trim(strip_tags($request->get('code')));

        /** @var TandemTransaction $transaction */
        $transaction = $dm
            ->getRepository('StoreBundle:TandemTransaction')
            ->findOneByCode($orderCode);

        if (!$transaction) {
            throw $this->createNotFoundException('Not found Tandem transaction by CODE - '.$orderCode);
        }

        $html = $this->renderView(
            'PageBundle:Tandem:invoicePdf.html.twig',
            [
                'transaction' => $transaction,
            ]
        );

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="Tandem_Invoice_Code_'.$orderCode.'.pdf"',
            ]
        );

//        Commented for HTML debug purposes
//
//        return $this->render(
//            'PageBundle:Tandem:invoicePdf.html.twig',
//            [
//                'transaction' => $transaction,
//            ]);

    }

    /**
     * @param Registry          $dm
     * @param TandemTransaction $transaction
     * @param float             $price
     * @param float             $discountCode
     *
     * @return float
     */
    private function calculateTandemCardPriceWithDiscount($dm, $transaction, $price, $discountCode)
    {
        $discountCode = trim(strip_tags($discountCode));

        if ($discountCode) {
            /** @var TandemDiscountCode $discount */
            $discount = $dm->getRepository('StoreBundle:TandemDiscountCode')
                ->findOneBy(
                    [
                        'code'     => $discountCode,
                        'isActive' => true,
                    ]
                );

            if ($discount) {
                if ($discount->getUsageQty() > $discount->getUsedNumTimes()) {
                    $price = $price - ($price / 100) * $discount->getPercent();
                    $transaction->setDiscountCode($discount->getCode());
                }
            }
        }

        return $price;
    }

    /**
     * @param TandemTransaction $transaction
     *
     * @return Form
     */
    private function getTandemTransactionForm(TandemTransaction $transaction)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        if (is_object($user) || $user instanceof UserInterface) {
            $transaction->setClient($user);
        }

        $transaction->setIsActive(false);
        $transaction->setPaymentStatus(TandemTransaction::PAYMENT_STATUS_NOT_PAID);

        return $this->createForm(new TandemCardType(), $transaction);
    }

    /**
     * @param Form $form
     *
     * @return array
     */
    private function getErrorMessages(Form $form)
    {
        $errors = [];

        /** @var Form $child */
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }
}