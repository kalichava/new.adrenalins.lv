<?php

namespace Adrenalins\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;

/**
 * Class PageController
 *
 * @package Adrenalins\PageBundle\Controller
 */
class PageController extends Controller
{
    /**
     * Homepage
     *
     * @param Request $request
     *
     * @return Response
     */
    public function homeAction(Request $request)
    {
        $em = $this->getDoctrine();

        $slides = $em
            ->getRepository('SlidesBundle:Slide')
            ->findBy(
                [
                    'isActive' => true,
                ]
            );

        /** @var PageRepository $repo */
        $page = $em
            ->getRepository('PageBundle:Page')
            ->findActivePage('homepage', null);

        return $this->render(
            'PageBundle::homepage.html.twig',
            [
                'page'   => $page,
                'slides' => $slides,
            ]
        );
    }

    /**
     * Page output
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        /** @var PageRepository $repo */
        $repo = $this->getDoctrine()->getRepository('PageBundle:Page');
        $parent = null;
        $gallery = null;

        $slug = trim(strip_tags($request->get('slug')));
        $parentSlug = trim(strip_tags($request->get('parent')));

        if ($parentSlug) {
            $parent = $repo->findOneBySlug($parentSlug);
            if (!$parent) {
                throw $this->createNotFoundException('Not found parent page by slug - '.$parentSlug);
            }
        }

        $page = $repo->findActivePage($slug, $parent); // @todo: why parent is need to be passed ?
        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - '.$slug);
        }

        $content = $page->getContent();
        $template = sprintf('PageBundle::show_%s.html.twig', $page->getLayout());

        preg_match('/%%gallery_(.*)%%/iU', $page->getContent(), $match);
        if (!empty($match) && array_key_exists(1, $match)) {
            $galleryId = (int) $match[1];
            if ($galleryId) {
                /** @var Gallery $gallery */
                $gallery = $this->getDoctrine()
                    ->getRepository('ApplicationSonataMediaBundle:Gallery')
                    ->find($galleryId);
            }

            $content = preg_replace('/%%gallery_(.*)%%/iU', '', $content);
        }

        return $this->render(
            $template,
            [
                'page'    => $page,
                'parent'  => $parent,
                'content' => $content,
                'gallery' => $gallery,
            ]
        );
    }
}