<?php

namespace Adrenalins\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;

/**
 * Class ClubController
 *
 * @package Adrenalins\PageBundle\Controller
 */
class ClubController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function pilotsAction(Request $request)
    {
        $em = $this->getDoctrine();
        $pageSlug = 'pilots';
        $parentPageSlug = 'club';

        /** @var PageRepository $pageRepo */
        $pageRepo = $em->getRepository('PageBundle:Page');

        /** @var Page $parent */
        $parent = $pageRepo->findOneBySlug($parentPageSlug);

        if (!$parent) {
            throw $this->createNotFoundException('Not found parent page by slug - ' . $parentPageSlug);
        }

        /** @var Page $page */
        $page = $pageRepo->findActivePage($pageSlug, $parent);

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        $users = $em
            ->getRepository('UserBundle:User')
            ->findActivePilots();

        return $this->render(
            'PageBundle:Club:pilots.html.twig',
            [
                'page'  => $page,
                'users' => $users
            ]
        );
    }
}