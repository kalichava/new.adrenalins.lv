<?php

namespace Adrenalins\PageBundle\Controller;

use Locale;
use IntlDateFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Adrenalins\PageBundle\Entity\Page;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;

/**
 * Class ParamotorsController
 *
 * @package Adrenalins\PageBundle\Controller
 */
class ParamotorsController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function mediaAction(Request $request)
    {
        $em = $this->getDoctrine();
        $pageSlug = 'media';
        $parentPageSlug = 'paramotors';

        /** @var PageRepository $pageRepo */
        $pageRepo = $em->getRepository('PageBundle:Page');

        /** @var Page $parent */
        $parent = $pageRepo->findOneBySlug($parentPageSlug);

        if (!$parent) {
            throw $this->createNotFoundException('Not found parent page by slug - ' . $parentPageSlug);
        }

        /** @var Page $page */
        $page = $pageRepo->findActivePage($pageSlug, $parent);

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        $repo = $em->getRepository('ApplicationSonataMediaBundle:Gallery');

        if ($request->isXmlHttpRequest()) {
            $galleries = $repo->findBy(
                [
                    'enabled' => true,
                    'context' => 'paramotors'
                ],
                [
                    'createdAt' => 'desc'
                ]
            );

            $items = [];
            $formatter = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::NONE, IntlDateFormatter::NONE);
            $formatter->setPattern("dd MMMM yyyy");

            /** @var Gallery $item */
            foreach ($galleries as $item) {

                $galleryImage = '';
                $galleryMedia = $item->getGalleryHasMedias();
                $galleryMediaNum = $galleryMedia->count();

                if ($galleryMediaNum) {
                    /** @var Media $media */
                    $media = $galleryMedia->first()->getMedia();
                    $provider = $this->get($media->getProviderName());
                    $providerFormat = $provider->getFormatName($media, 'medium');
                    $galleryImage = $provider->generatePublicUrl($media, $providerFormat);
                }

                $route = 'sonata_media_gallery_view';
                $itemUrl = $this->generateUrl($route, ['id' => $item->getId()], true);
                $publishedAt = $formatter->format($item->getCreatedAt());

                $items[] = [
                    'url'         => $itemUrl,
                    'title'       => $item->getName(),
                    'image'       => $galleryImage,
                    'publishedAt' => $publishedAt,
                ];
            }

            return new JsonResponse(
                [
                    'total'  => (!empty($items)) ? count($items) : 0,
                    'result' => $items
                ]
            );
        }

        return $this->render(
            'PageBundle:Paramotors:media.html.twig',
            [
                'page' => $page
            ]
        );
    }
}