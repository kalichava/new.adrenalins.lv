<?php

namespace Adrenalins\PageBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Doctrine\ORM\EntityManager;
use Adrenalins\PageBundle\Entity\Page;

/**
 * Class SubMenuBlock
 *
 * @package Adrenalins\MenuBundle\Block
 */
class SubMenuBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param Router          $router
     * @param EntityManager   $entityManager
     */
    public function __construct(EngineInterface $templating, Router $router, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        if (array_key_exists('attr', $settings) && array_key_exists('page', $settings['attr'])) {

            /** @var Page $page */
            $page = $settings['attr']['page'];
            $parent = $page->getParent();

            if ($page && $parent) {

                $items = $this->entityManager
                    ->getRepository('PageBundle:Page')
                    ->findActivePagesByParent($page->getParent());

                if ($items) {

                    return $this->renderResponse(
                        $blockContext->getTemplate(),
                        [
                            'block'  => $blockContext->getBlock(),
                            'page'   => $page,
                            'items'  => $items,
                            'parent' => $parent
                        ],
                        $response
                    );

                }
            }

        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'PageBundle:Block:submenu.html.twig'
            ]
        );
    }

    /**
     * Generate menu items
     *
     * @param integer $menuId
     *
     * @return array
     */
    private function generateItems($menuId)
    {
        $items = [];

        $menuItems = $this
            ->entityManager
            ->getRepository('MenuBundle:MenuItem')
            ->findActiveItemsByMenu($menuId);

        /** @var MenuItem $menuItem */
        foreach ($menuItems as $menuItem) {
            $url = trim($menuItem->getUrl());
            $page = $menuItem->getPage();

            if (!$url && $page) {
                $pageParent = $page->getParent();
                if ($pageParent) {
                    $url = $this->router->generate(
                        'adrenalins_page_child_show',
                        [
                            'parent' => $pageParent->getSlug(),
                            'slug'   => $page->getSlug(),
                        ],
                        true
                    );
                } else {
                    $url = $this->router->generate('adrenalins_page_show', ['slug' => $page->getSlug()], true);
                }
            }

            $items[] = array(
                'url'   => $url,
                'title' => $menuItem->getTitle()
            );
        }

        return $items;
    }
}
