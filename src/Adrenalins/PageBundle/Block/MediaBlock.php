<?php

namespace Adrenalins\PageBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class MediaBlock
 *
 * @package Adrenalins\PageBundle\Block
 */
class MediaBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param Router          $router
     * @param EntityManager   $entityManager
     */
    public function __construct(EngineInterface $templating, Router $router, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $media = $this->entityManager
            ->getRepository('ApplicationSonataMediaBundle:Media')
            ->findActiveRandomVideo();

        if ($media) {

            $embedCode = $media->getMetadataValue('html');

            if (mb_stristr($embedCode, 'vimeo')) {
                $params = 'badge=0&byline=0&portrait=0';
            } else {
                $params = 'rel=0&modestbranding=1&theme=light&autohide=1';
            }

            preg_match('/src="(.*)"/iU', $embedCode, $match);
            if(!empty($match) && array_key_exists(1, $match)) {
                $params = (mb_stristr($match[1], '?')) ? '&' . $params : '?' . $params;
                $embedCode = str_replace($match[1], $match[1] . $params, $embedCode);
            }

            return $this->renderResponse(
                $blockContext->getTemplate(),
                [
                    'block'     => $blockContext->getBlock(),
                    'media'     => $media,
                    'embedCode' => $embedCode
                ],
                $response
            );
        } else {
            return new Response(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'ApplicationSonataMediaBundle:Block:media.html.twig'
            ]
        );
    }
}
