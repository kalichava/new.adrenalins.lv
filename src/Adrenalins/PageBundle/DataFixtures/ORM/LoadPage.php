<?php

namespace Adrenalins\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\PageTranslation;

/**
 * Class LoadPage
 *
 * @package Adrenalins\PageBundle\DataFixtures\ORM
 */
class LoadPage extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = ['ru', 'en'];

        $faker['lv'] = Factory::create('lv_LV');
        $faker['en'] = Factory::create('en_EN');
        $faker['ru'] = Factory::create('ru_RU');

        $pagesData = $this->getPagesData();

        foreach ($pagesData as $pageData) {

            $page = $this->createPage($pageData, $locales, $faker);

            if (!empty($pageData['childs'])) {

                foreach ($pageData['childs'] as $childData) {
                    $child = $this->createPage($childData, $locales, $faker);
                    $child->setParent($page);

                    $this->setReference('page-' . $page->getSlug() . '-' . $child->getSlug(), $child);

                    $manager->persist($page);
                    $manager->persist($child);
                    $manager->flush();

                    unset($child, $childData);
                }

            } else {
                $manager->persist($page);

                $this->setReference('page-' . $page->getSlug(), $page);
            }

            $manager->flush();

            unset($page, $pageData);
        }
    }

    /**
     * @param array   $data
     * @param array   $locales
     * @param Factory $faker
     *
     * @return Page
     */
    private function createPage($data, $locales, $faker)
    {
        $page = new Page();
        $page->setSlug($data['slug']);
        $page->setTitle($data['title']);
        $page->setContent(
            '<p>' . ((isset($data['content'])) ? $data['content'] : $faker['lv']->text(mt_rand(100, 400))) . '</p>'
        );
        $page->setLayout($data['layout']);
        $page->setIsActive(true);

        foreach ($locales as $locale) {

            $trans = new PageTranslation();
            $trans->setField('title');
            $trans->setContent($data['title']);
            $trans->setLocale($locale);
            $page->addTranslation($trans);

            $trans = new PageTranslation();
            $trans->setField('content');
            $trans->setContent(
                '<p>' . ((isset($data['content'])) ? $data['content'] : $faker['lv']->text(mt_rand(100, 400))) . '</p>'
            );
            $trans->setLocale($locale);
            $page->addTranslation($trans);

        }

        return $page;
    }

    /**
     * @return array
     */
    private function getPagesData()
    {
        return [
            [
                'slug'    => 'contacts',
                'title'   => 'Contact us',
                'layout'  => 'default',
                'content' => '<h4>Mūsu jinstruktori</h4><blockquote><p>Docendi lucilius invenire per ei, has ex vidit idque scriptorem. An wisi debitis lobortis has, ei vis omnium equidem. Iuvaret recteque efficiendi vix id, ad ius fugit persius praesent. Integre dissentiunt his ex.</p></blockquote>',
            ],
            [
                'slug'   => 'about',
                'title'  => 'About us',
                'layout' => 'default'
            ],
            [
                'slug'   => 'school',
                'title'  => 'School',
                'layout' => 'default',
                'childs' => [
                    [
                        'slug'   => 'about',
                        'title'  => 'About the school',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'programm',
                        'title'  => 'Programm',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'faq',
                        'title'  => 'Questions and answers',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'media',
                        'title'  => 'Media gallery',
                        'layout' => '2columns',
                    ]
                ]
            ],
            [
                'slug'   => 'tandem',
                'title'  => 'Tandem Flights',
                'layout' => 'default',
            ],
            [
                'slug'   => 'club',
                'title'  => 'Club',
                'layout' => 'default',
                'childs' => [
                    [
                        'slug'   => 'about',
                        'title'  => 'About club',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'pilots',
                        'title'  => 'Our pilots',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'calendar',
                        'title'  => 'Calendar',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'places',
                        'title'  => 'Places we fly',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'rules',
                        'title'  => 'Our rules',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'resources',
                        'title'  => 'Useful resources',
                        'layout' => '2columns',
                    ],
                ]
            ],
            [
                'slug'   => 'media',
                'title'  => 'Media',
                'layout' => 'default'
            ],
            [
                'slug'   => 'paramotors',
                'title'  => 'Paramotors',
                'layout' => 'default',
                'childs' => [
                    [
                        'slug'   => 'about',
                        'title'  => 'About the PPG',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'programm',
                        'title'  => 'Scholl programm',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'faq',
                        'title'  => 'Questions and answers',
                        'layout' => '2columns',
                    ],
                    [
                        'slug'   => 'media',
                        'title'  => 'Media Gallery',
                        'layout' => '2columns',
                    ]
                ]
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}