<?php

namespace Adrenalins\PageBundle\Twig;

use Adrenalins\PageBundle\Entity\Page;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PageBundleExtension
 *
 * @package Adrenalins\PageBundle\Twig
 */
class PageBundleExtension extends \Twig_Extension
{
    private $request;
    private $defaultLocale = 'lv';
    private $availableLocales = [];

    public function __construct($request, $defaultLocale = 'lv', $availableLocales)
    {
        $this->request = $request;
        $this->defaultLocale = $defaultLocale;
        $this->availableLocales = $availableLocales;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getMetaTitle', [$this, 'getMetaTitle']),
            new \Twig_SimpleFunction('currentPathWithLocale', [$this, 'currentPathWithLocale']),
        ];
    }

    /**
     * Generate current url for exact locale
     *
     * @param string $uri
     * @param string $locale
     * @return string
     */
    public function currentPathWithLocale($uri, $locale)
    {
        if($this->request->getLocale() != $this->defaultLocale) {
            $to = ($locale == $this->defaultLocale) ? '/' : sprintf('/%s/', $locale);
            $uri = preg_replace('/\/' . implode('\/|\/', $this->availableLocales) .'\//', $to, $uri);
        } else {
            $host = $this->request->getSchemeAndHttpHost();
            $to = ($locale == $this->defaultLocale) ? $host : sprintf('%s/%s', $host, $locale);
            $uri = str_replace($host, $to, $uri);
        }

        return $uri;
    }

    /**
     * @param string $title
     * @param string $parentTitle
     *
     * @return string
     */
    public function getMetaTitle($title, $parentTitle = null)
    {
        $title = sprintf('%s / ', $title);

        if($parentTitle) {
            $title .= sprintf('%s / ', $parentTitle);
        }

        return $title;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'page_bundle_extension';
    }
}