<?php

namespace Adrenalins\SlidesBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class SlidesBlock
 *
 * @package Adrenalins
 */
class SlidesBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param EntityManager   $entityManager
     */
    public function __construct(EngineInterface $templating, EntityManager $entityManager)
    {
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $slides = $this
            ->entityManager
            ->getRepository('SlidesBundle:Slide')
            ->findBy(
                [
                    'isActive' => true
                ]
            );

        return $this->renderResponse(
            $blockContext->getTemplate(),
            [
                'block' => $blockContext->getBlock(),
                'items' => $slides
            ],
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'SlidesBundle:Block:slides.html.twig'
            ]
        );
    }
}
