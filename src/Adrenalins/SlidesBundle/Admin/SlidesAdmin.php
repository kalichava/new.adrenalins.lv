<?php

namespace Adrenalins\SlidesBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Adrenalins\SlidesBundle\Entity\Slide;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Slides admin
 */
class SlidesAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('title', null, ['label' => 'Title'])
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, ['label' => 'Title'])
            ->add('subTitle', null, ['label' => 'Sub title'])
            ->add('description', null, ['label' => 'Description'])
            ->add('url', null, ['label' => 'URL path'])
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => true])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();

        $formMapper
            ->add('url', null, ['label' => 'URL path', 'help' => 'Example: school/about'])
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                [
                    'label'              => 'Translations',
                    'translatable_class' => 'Adrenalins\SlidesBundle\Entity\Slide',
                    'fields'             => [
                        'title'       => [
                            'required' => true,
                            'label'    => 'Title',
                        ],
                        'subTitle'    => [
                            'required' => true,
                            'label'    => 'Sub title',
                        ],
                        'description' => [
                            'required' => true,
                            'label'    => 'Description'
                        ]
                    ]
                ]
            )
            ->add(
                'blockFile',
                'image',
                [
                    'label'       => 'Block image',
                    'required'    => false,
                    'path'        => ($object->getId()) ? $object->getBlockImage() : '',
                    'help'        => ($object->getBlockImage()) ? '<img src="'. $object->getBlockImage() .'" class="admin-preview" />' : '',
                    'width'       => 334,
                    'height'      => 334,
                    'constraints' => [
                        new Assert\Image(
                            [
                                'maxSize' => '10M'
                            ]
                        )
                    ]
                ]
            )
            ->add(
                'slideFile',
                'image',
                [
                    'label'       => 'Slide image',
                    'required'    => false,
                    'path'        => ($object->getId()) ? $object->getSlideImage() : '',
                    'width'       => 2600,
                    'height'      => 700,
                    'help'        => ($object->getSlideImage()) ? '<img src="'. $object->getSlideImage() .'" class="admin-preview" />' : '',
                    'constraints' => [
                        new Assert\Image(
                            [
                                'maxSize' => '10M'
                            ]
                        )
                    ]
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label' => 'Is Active'
                ]
            );
    }

    /**
     * @param mixed $object
     *
     * @return mixed|void
     */
    public function preUpdate($object)
    {
        $this->uploadImages($object);
    }

    /**
     * @param mixed $object
     *
     * @return mixed|void
     */
    public function prePersist($object)
    {
        $this->uploadImages($object);
    }

    /**
     * @param Slide $object
     */
    public function uploadImages($object)
    {
        foreach(['blockFile', 'slideFile'] as $setter) {
            if (false !== $image = $this->uploadFile($object, $setter)) {

                if($setter == 'blockFile') {
                    $object->setBlockImage($image);
                    $object->setBlockFile(null);
                }

                if($setter == 'slideFile') {
                    $object->setSlideImage($image);
                    $object->setSlideFile(null);
                }
            }
        }
    }

    /**
     * @param UploadFile|mixed $object
     * @param string           $setter
     *
     * @return string
     */
    protected function uploadFile($object, $setter)
    {
        $file = call_user_func_array([$object, 'get'. ucfirst($setter)], []);

        if (!$file instanceof UploadedFile) {
            return false;
        }

        if (null === $ext = $file->guessExtension()) {
            $ext = $file->getExtension();
        }

        $fileName = sha1(uniqid(mt_rand(), true));

        if (null !== $ext) {
            $fileName .= '.' . $ext;
        }

        $fullUploadPath = $object->getFullUploadPath();
        $rootPath = $object->getRootWebPath();

        $movePath = $rootPath . $fullUploadPath;

        $file->move($movePath, $fileName);

        return $fullUploadPath . '/' . $fileName;
    }
}