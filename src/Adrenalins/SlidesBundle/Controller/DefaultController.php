<?php

namespace Adrenalins\SlidesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SlidesBundle:Default:index.html.twig', array('name' => $name));
    }
}
