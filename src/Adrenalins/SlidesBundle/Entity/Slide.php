<?php

namespace Adrenalins\SlidesBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Adrenalins\SlidesBundle\Entity\SlideTranslation;

/**
 * @ORM\Entity(repositoryClass="Adrenalins\SlidesBundle\Entity\Repository\SlideRepository")
 * @ORM\Table(name="slides")
 * @Gedmo\TranslationEntity(class="Adrenalins\SlidesBundle\Entity\SlideTranslation")
 * @ORM\HasLifecycleCallbacks
 */
class Slide
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    protected $url;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\Length(
     *     max="255"
     * )
     */
    protected $title;

    /**
     * @ORM\Column(name="sub_title", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\Length(
     *     max="255"
     * )
     */
    protected $subTitle;

    /**
     * @ORM\Column(name="description", type="text")
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @ORM\Column(name="block_image", type="string", length=255, nullable=true)
     */
    protected $blockImage;

    /**
     * @ORM\Column(name="slide_image", type="string", length=255, nullable=true)
     */
    protected $slideImage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="SlideTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * Variable for file uploading
     *
     * @var UploadedFile|null
     *
     * @Assert\File(
     *     maxSize = "8M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $blockFile;

    /**
     * Variable for file uploading
     *
     * @var UploadedFile|null
     *
     * @Assert\File(
     *     maxSize = "8M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $slideFile;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Slide
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Slide
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subTitle
     *
     * @param string $subTitle
     * @return Slide
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Slide
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Adds translations.
     *
     * @param SlideTranslation $translation
     *
     * @return $this
     */
    public function addTranslation(SlideTranslation $translation)
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setObject($this);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param SlideTranslation $translations
     */
    public function removeTranslation(SlideTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set slideImage
     *
     * @param string $slideImage
     * @return Slide
     */
    public function setSlideImage($slideImage)
    {
        $this->slideImage = $slideImage;

        return $this;
    }

    /**
     * Get slideImage
     *
     * @return string 
     */
    public function getSlideImage()
    {
        return $this->slideImage;
    }

    /**
     * Set blockImage
     *
     * @param string $blockImage
     * @return Slide
     */
    public function setBlockImage($blockImage)
    {
        $this->blockImage = $blockImage;

        return $this;
    }

    /**
     * Get blockImage
     *
     * @return string 
     */
    public function getBlockImage()
    {
        return $this->blockImage;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $blockFile
     */
    public function setBlockFile(UploadedFile $blockFile = null)
    {
        $this->blockFile = $blockFile;
    }

    /**
     * Get blockFile.
     *
     * @return UploadedFile
     */
    public function getBlockFile()
    {
        return $this->blockFile;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $slideFile
     */
    public function setSlideFile(UploadedFile $slideFile = null)
    {
        $this->slideFile = $slideFile;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getSlideFile()
    {
        return $this->slideFile;
    }

    /**
     * Get uplaod path for image files relative to root upload path
     *
     * @return string
     */
    public function getUploadPath()
    {
        return '/slides';
    }

    /**
     * Get root web path
     *
     * @return string
     */
    public function getRootWebPath()
    {
        return realpath(__DIR__ . '/../../../../web');
    }

    /**
     * Get full upload path relative to root web path
     *
     * @return string
     */
    public function getFullUploadPath()
    {
        return '/uploads' . $this->getUploadPath();
    }
}
