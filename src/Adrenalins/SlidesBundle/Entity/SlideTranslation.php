<?php

namespace Adrenalins\SlidesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="slides_translations",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="slides_translation_unique_idx", columns={
 *          "locale", "object_id", "field"
 *      })}
 * )
 */
class SlideTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Slide", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $object;
}
