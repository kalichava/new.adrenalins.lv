<?php

namespace Adrenalins\EventsBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Adrenalins\SettingsBundle\Core\SettingsLoader as Settings;
use Doctrine\ORM\EntityManager;
use Adrenalins\EventsBundle\Entity\Event;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class EventsBlock
 *
 * @package Adrenalins
 */
class EventsBlock extends BaseBlockService
{
    /**
     * @param EngineInterface $templating
     * @param EntityManager   $entityManager
     * @param SecurityContext $securityContext
     * @param Settings        $settings
     */
    public function __construct(
        EngineInterface $templating,
        EntityManager $entityManager,
        SecurityContext $securityContext,
        Settings $settings
    )
    {
        $this->settings = $settings;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->securityContext = $securityContext;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $date = new \Datetime('now');
        $limit = (int) $this->settings->get('events-block-limit', 5);
        $types = [Event::TYPE_PUBLIC];

        if (is_object($this->securityContext->getToken()->getUser())) {
            $types[] = Event::TYPE_MEMBERS;
        }

        $events = $this
            ->entityManager
            ->getRepository('EventsBundle:Event')
            ->findActiveByDateAndTypesAndLimit($date->format('Y-m-d'), $types, $limit);

        return $this->renderResponse(
            $blockContext->getTemplate(),
            [
                'block' => $blockContext->getBlock(),
                'items' => $events
            ],
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'template' => 'EventsBundle:Block:events.html.twig'
            ]
        );
    }
}
