<?php

namespace Adrenalins\EventsBundle\Controller;

use Adrenalins\EventsBundle\Entity\Event;
use Adrenalins\EventsBundle\Entity\EventTranslation;
use Adrenalins\EventsBundle\Form\Type\EventType;
use Adrenalins\PageBundle\Entity\Page;
use Adrenalins\PageBundle\Entity\Repository\PageRepository;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class EventsController
 *
 * @package Adrenalins\EventsBundle\Controller
 */
class EventsController extends Controller
{
    /**
     * Show single event
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');
        $page = $this->getPage();

        /** @var Event $event */
        $event = $dm
            ->getRepository('EventsBundle:Event')
            ->findOneBy(
                [
                    'id'       => $id,
                    'isActive' => true
                ]
            );

        if (!$event) {
            throw $this->createNotFoundException('Not found event by id - ' . $id);
        }

        return $this->render(
            'EventsBundle::show.html.twig',
            [
                'page'  => $page,
                'event' => $event
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function calendarAction(Request $request)
    {
        $em = $this->getDoctrine();
        $date = new \Datetime('now');
        $types = [Event::TYPE_PUBLIC];
        $eventsData = [];

        if ($this->getUser()) {
            $types[] = Event::TYPE_MEMBERS;
        }

        $page = $this->getPage();

        $events = $this
            ->getDoctrine()
            ->getRepository('EventsBundle:Event')
            ->findActiveByTypesAndLimit($types);

        /** @var Event $event */
        foreach ($events as $event) {
            $dateEnd = $event->getDateEnd();
            $dateBegin = $event->getDateBegin();

            $eventData = [
                'url'    => $this->generateUrl('events_club_calendar_event', ['id' => $event->getId()]),
                'title'  => $event->getTitle(),
                'start'  => $dateBegin->format('Y-m-d H:i:s'),
                'allDay' => true
            ];

            if ($dateEnd) {
                $eventData['end'] = $dateEnd->format('Y-m-d H:i:s');
            }

            $eventsData[] = $eventData;
        }

        return $this->render(
            'EventsBundle::calendar.html.twig',
            [
                'page'   => $page,
                'events' => json_encode($eventsData)
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $id = (int) $request->get('id');
        $page = null;

        if ($id) {
            /** @var Event $event */
            $event = $dm
                ->getRepository('EventsBundle:Event')
                ->findOneBy(
                    [
                        'id'     => $id,
                        'author' => $user
                    ]
                );

            if (!$event) {
                throw $this->createNotFoundException('Not found event by id - ' . $id);
            }
        } else {
            $page = $this->getPage();

            $event = new Event();
            $event->setAuthor($user);
            $event->setIsActive(true);
        }

        $form = $this->createForm(new EventType(), $event);

        if ($request->isMethod("POST")) {
            $form->submit($request);

            if ($form->isValid()) {
                $manager = $dm->getManager();

                $locales = array_diff(
                    $this->container->getParameter('available_locales'),
                    [
                        $this->container->getParameter('locale')
                    ]
                );

                if ($event->getCreatedAt()) {
                    /** @var EventTranslation $trans */
                    foreach ($event->getTranslations() as $trans) {
                        $trans->setContent($form[$trans->getField()]->getData());
                        $manager->persist($trans);
                    }

                    $redirectUrl = $this->generateUrl('adrenalins_user_events_edit', ['id' => $id]);
                    $flashMessage = 'events.flash.edited';
                } else {
                    foreach ($locales as $locale) {
                        $event->addTranslation(new EventTranslation($locale, 'title', $form['title']->getData()));
                        $event->addTranslation(new EventTranslation($locale, 'content', $form['content']->getData()));
                    }

                    $redirectUrl = $this->generateUrl('adrenalins_user_events_create');
                    $flashMessage = 'events.flash.created';
                }

                $manager->persist($event);
                $manager->flush();

                $this->get('session')->getFlashBag()->set('success', $flashMessage);

                return new RedirectResponse($redirectUrl . '#content-table');
            } else {
                $flashMessage = 'events.flash.error';
                $formErrors = $this->getErrorMessages($form);
                $this->get('session')->getFlashBag()->set('error', $flashMessage);
            }
        }

        /** @var Event $events */
        $events = $dm
            ->getRepository('EventsBundle:Event')
            ->findBy(
                [
                    'author'   => $user,
                    'isActive' => true
                ],
                [
                    'createdAt' => 'DESC'
                ]
            );

        return $this->render(
            'EventsBundle:Create:index.html.twig',
            [
                'id'         => $id,
                'page'       => $page,
                'user'       => $user,
                'events'     => $events,
                'form'       => $form->createView(),
                'formErrors' => (isset($formErrors) ? $formErrors : null)
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        $dm = $this->getDoctrine();
        $id = (int) $request->get('id');

        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var Event $event */
        $event = $dm
            ->getRepository('EventsBundle:Event')
            ->findOneBy(
                [
                    'id'     => $id,
                    'author' => $user
                ]
            );

        if (!$event) {
            throw $this->createNotFoundException('Not found events by id - ' . $id);
        }

        $event->setIsActive(false);

        $dm->getManager()->flush();

        $this->get('session')->getFlashBag()->set('notice', 'evets.flash.deleted');

        return new RedirectResponse($this->generateUrl('adrenalins_user_events_create'));
    }

    /**
     * @return Page
     */
    private function getPage()
    {
        /** @var PageRepository $pageRepo */
        $pageRepo = $this->getDoctrine()->getRepository('PageBundle:Page');

        $pageSlug = 'calendar';
        $parentPageSlug = 'club';

        /** @var Page $parent */
        $parent = $pageRepo->findOneBySlug($parentPageSlug);

        if (!$parent) {
            throw $this->createNotFoundException('Not found parent page by slug - ' . $parentPageSlug);
        }

        /** @var Page $page */
        $page = $pageRepo->findActivePage($pageSlug, $parent);

        if (!$page) {
            throw $this->createNotFoundException('Not found page by slug - ' . $pageSlug);
        }

        return $page;
    }

    /**
     * @param Form $form
     *
     * @return array
     */
    private function getErrorMessages(Form $form)
    {
        $errors = array();

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()] = $error->getMessage();
                }
            }
        }

        return $errors;
    }
}
