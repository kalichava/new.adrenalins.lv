<?php

namespace Adrenalins\EventsBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class DatetimeToDateTransformer
 *
 * @package Adrenalins\EventsBundle\Form\DataTransformer
 */
class DatetimeToDateTransformer implements DataTransformerInterface
{
    /**
     * @param \DateTime $dateTime
     *
     * @return string
     */
    public function transform($dateTime)
    {
        if (null === $dateTime) {
            return "";
        }

        return $dateTime->format('d/m/Y');
    }

    /**
     * @param string $dateString
     *
     * @return \Datetime
     */
    public function reverseTransform($dateString)
    {
        if (!$dateString) {
            return null;
        }

        $date = explode('/', $dateString);
        if(count($date) < 3) {
            return null;
        }

        $dateTime = new \DateTime();
        $dateTime->setDate($date[2],$date[1],$date[0]);

        return $dateTime;
    }
}