<?php

namespace Adrenalins\EventsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Adrenalins\EventsBundle\Form\DataTransformer\DatetimeToDateTransformer;

/**
 * Class EventType
 *
 * @package Adrenalins\EventsBundle\Form\Type
 */
class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $hours = [];
        foreach(range(0,23) as $num) {
            $val = sprintf("%02d", $num);
            $hours[$val] = $val;
        }

        $transformer = new DatetimeToDateTransformer();

        $builder
            ->add(
                'title',
                null,
                [
                    'label'       => false,
                    'required'    => true,
                    'attr'        => [
                        'placeholder' => 'user.events.manage.form.label_title'
                    ],
                    'constraints' => [
                        new Assert\NotBlank()
                    ]
                ]
            )
            ->add(
                'content',
                'textarea',
                [
                    'label'       => false,
                    'required'    => true,
                    'attr'        => [
                        'rows'        => 3,
                        'placeholder' => 'user.events.manage.form.label_content'
                    ],
                    'constraints' => [
                        new Assert\NotBlank()
                    ]
                ]
            )
            ->add(
                $builder->create(
                    'dateBegin',
                    'text',
                    [
                        'attr'     => [
                            'class' => 'input-date'
                        ],
                        'label'    => 'user.events.manage.form.label_date_begin',
                        'required' => true
                    ]
                )->addViewTransformer($transformer)
            )
            ->add(
                'dateHoursBegin',
                'choice',
                [
                    'attr'        => [
                        'class' => 'input-time'
                    ],
                    'empty_value' => "—",
                    'choices'     => $hours,
                    'label'       => false,
                    'required'    => true
                ]
            )
            ->add(
                'dateMinutesBegin',
                'choice',
                [
                    'attr'        => [
                        'class' => 'input-time'
                    ],
                    'empty_value' => "—",
                    'choices'     => [
                        '00' => '00',
                        '15' => '15',
                        '30' => '30',
                        '45' => '45',
                    ],
                    'label'       => false,
                    'required'    => true
                ]
            )
            ->add(
                $builder->create(
                    'dateEnd',
                    'text',
                    [
                        'attr'     => [
                            'class' => 'input-date'
                        ],
                        'label'    => 'user.events.manage.form.label_date_end',
                        'required' => false
                    ]
                )->addViewTransformer($transformer)
            )
            ->add(
                'dateHoursEnd',
                'choice',
                [
                    'attr'        => [
                        'class' => 'input-time'
                    ],
                    'empty_value' => "—",
                    'choices'     => $hours,
                    'label'       => false,
                    'required'    => false
                ]
            )
            ->add(
                'dateMinutesEnd',
                'choice',
                [
                    'attr'        => [
                        'class' => 'input-time'
                    ],
                    'empty_value' => "—",
                    'choices'     => [
                        '00' => '00',
                        '15' => '15',
                        '30' => '30',
                        '45' => '45',
                    ],
                    'label'       => false,
                    'required'    => false
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Adrenalins\EventsBundle\Entity\Event'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'event';
    }
}