<?php

namespace Adrenalins\EventsBundle\DataFixtures\ORM;

use Faker\Factory;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Adrenalins\EventsBundle\Entity\Event;
use Adrenalins\EventsBundle\Entity\EventTranslation;

/**
 * Class LoadEvents
 *
 * @package Adrenalins\EventsBundle\DataFixtures\ORM
 */
class LoadEvents extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = ['ru', 'en'];

        $faker['lv'] = Factory::create('en_EN');
        $faker['en'] = Factory::create('en_EN');
        $faker['ru'] = Factory::create('ru_RU');

        $fixturesCount = 10;

        while ($fixturesCount) {

            $dateBegin = $faker['lv']->dateTimeBetween('+5 day', new \DateTime('+3 month'));
            $dateEnd = clone $dateBegin;
            $dateEnd->modify('+' . rand(10, 20) . ' day');

            $event = new Event();
            $event->setTitle($faker['lv']->sentence());
            $event->setContent('<p>' . $faker['lv']->text(mt_rand(400, 4000)) . '</p>');
            $event->setAuthor($this->getReference('user-' . $fixturesCount));
            $event->setIsPublic((int) rand(0, 1));
            $event->setDateBegin($dateBegin);
            $event->setDateEnd($dateEnd);
            $event->setIsActive(true);

            foreach ($locales as $locale) {

                $trans = new EventTranslation();
                $trans->setField('title');
                $trans->setContent($faker[$locale]->sentence());
                $trans->setLocale($locale);
                $event->addTranslation($trans);

                $trans = new EventTranslation();
                $trans->setField('content');
                $trans->setContent('<p>' . $faker[$locale]->text(mt_rand(400, 4000)) . '</p>');
                $trans->setLocale($locale);
                $event->addTranslation($trans);

                $manager->persist($event);
            }

            $manager->persist($event);
            $manager->flush($event);

            $fixturesCount--;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}