<?php

namespace Adrenalins\EventsBundle\Entity;

use Adrenalins\NewsBundle\Entity\News;
use Adrenalins\PlaceBundle\Entity\Place;
use Adrenalins\UserBundle\Entity\User;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Adrenalins\EventsBundle\Entity\EventTranslation;

/**
 * @ORM\Entity(repositoryClass="Adrenalins\EventsBundle\Entity\Repository\EventRepository")
 * @ORM\Table(name="events", indexes={
 *      @ORM\Index(name="events_key_idx", columns={"slug"})
 * })
 * @Gedmo\TranslationEntity(class="Adrenalins\EventsBundle\Entity\EventTranslation")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("slug")
 */
class Event
{
    CONST TYPE_PUBLIC = 1;
    CONST TYPE_MEMBERS = 0;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true, length=255, nullable=false)
     * @Gedmo\Slug(fields={"title"}, separator="_", updatable=false, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\Length(
     *     max="255"
     * )
     */
    protected $title;

    /**
     * @ORM\Column(name="content", type="text")
     * @Gedmo\Translatable
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    protected $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_begin", type="datetime")
     */
    protected $dateBegin;

    protected $dateHoursBegin;

    protected $dateMinutesBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    protected $dateEnd;

    protected $dateHoursEnd;

    protected $dateMinutesEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="EventTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    protected $isPublic = 1;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\NewsBundle\Entity\News")
     */
    protected $news;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    protected $videos;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
     */
    protected $galleries;

    /**
     * @ORM\ManyToMany(targetEntity="Adrenalins\PlaceBundle\Entity\Place")
     */
    protected $places;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Event
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     *
     * @return Event
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    public function getDateHoursBegin()
    {
        return ($this->dateBegin) ? $this->dateBegin->format('H') : null;
    }

    public function getDateMinutesBegin()
    {
        return ($this->dateBegin) ? $this->dateBegin->format('i') : null;
    }

    public function setDateHoursBegin($hours)
    {
        $this->dateHoursBegin = $hours;
    }

    public function getDateMinutesEnd()
    {
        return ($this->dateEnd) ? $this->dateEnd->format('i') : null;
    }

    public function getDateHoursEnd()
    {
        return ($this->dateEnd) ? $this->dateEnd->format('H') : null;
    }

    public function setDateMinutesBegin($minutes)
    {
        $this->dateMinutesBegin = $minutes;
        $this->dateBegin->setTime($this->dateHoursBegin, $this->dateMinutesBegin);

        return $this;
    }

    public function setDateMinutesEnd($minutes)
    {
        $this->dateMinutesEnd = $minutes;
        $this->dateEnd->setTime($this->dateHoursEnd, $this->dateMinutesEnd);

        return $this;
    }

    public function setDateHoursEnd($hours)
    {
        $this->dateHoursEnd = $hours;
    }

    /**
     * @param \DateTime $timeBegin
     */
    public function setTimeBegin($timeBegin)
    {
        if ($timeBegin) {
            $this->dateBegin->setTime($timeBegin->format('H'), $timeBegin->format('i'));
        }
    }

    /**
     * @return \DateTime
     */
    public function getTimeBegin()
    {
        return $this->dateBegin;
    }

    /**
     * @param \DateTime $timeEnd
     */
    public function setTimeEnd($timeEnd)
    {
        if ($timeEnd) {
            $this->dateEnd->setTime($timeEnd->format('H'), $timeEnd->format('i'));
        }
    }

    /**
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Event
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return Event
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set author
     *
     * @param User $author
     *
     * @return Event
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Adds translations.
     *
     * @param EventTranslation $translation
     *
     * @return $this
     */
    public function addTranslation(EventTranslation $translation)
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setObject($this);
        }

        return $this;
    }

    /**
     * Remove translations
     *
     * @param EventTranslation $translations
     */
    public function removeTranslation(EventTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add galleries
     *
     * @param Gallery $galleries
     * @return Event
     */
    public function addGallery(Gallery $galleries)
    {
        if(!$this->galleries->contains($galleries)) {
            $this->galleries[] = $galleries;
        }

        return $this;
    }

    /**
     * Remove galleries
     *
     * @param Gallery $galleries
     */
    public function removeGallery(Gallery $galleries)
    {
        $this->galleries->removeElement($galleries);
    }

    /**
     * Has galleries
     *
     * @return boolean
     */
    public function hasGalleries()
    {
        return count($this->galleries);
    }

    /**
     * Get galleries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * Add videos
     *
     * @param Media $videos
     * @return Event
     */
    public function addVideo(Media $videos)
    {
        if(!$this->videos->contains($videos)) {
            $this->videos[] = $videos;
        }

        return $this;
    }

    /**
     * Remove videos
     *
     * @param Media $videos
     */
    public function removeVideo(Media $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Has videos
     *
     * @return boolean
     */
    public function hasVideos()
    {
        return count($this->videos);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add places
     *
     * @param Place $places
     * @return Event
     */
    public function addPlace(Place $places)
    {
        if(!$this->places->contains($places)) {
            $this->places[] = $places;
        }

        return $this;
    }

    /**
     * Remove places
     *
     * @param Place $places
     */
    public function removePlace(Place $places)
    {
        $this->places->removeElement($places);
    }

    /**
     * Get places
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Add news
     *
     * @param News $news
     * @return Event
     */
    public function addNews(News $news)
    {
        if(!$this->news->contains($news)) {
            $this->news[] = $news;
        }

        return $this;
    }

    /**
     * Remove news
     *
     * @param News $news
     */
    public function removeNews(News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews()
    {
        return $this->news;
    }
}
