<?php

namespace Adrenalins\EventsBundle\Entity\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;

/**
 * Class EventRepository
 *
 * @package Adrenalins\EventsBundle\Entity\Repository
 */
class EventRepository extends EntityRepository
{
    /**
     * Find active events by types
     * Event::TYPE_PUBLIC or Event::TYPE_MEMBERS
     *
     * @param Datetime $date
     * @param array    $types
     * @param integer  $limit
     *
     * @return array
     */
    public function findActiveByDateAndTypesAndLimit($date, array $types, $limit = 0)
    {
        $qb = $this
            ->createQueryBuilder('e')
            ->where('e.isActive = true')
            ->andWhere('e.dateBegin >= :date')
            ->andWhere('e.isPublic IN (:types)')
            ->setParameters(
                [
                    'date' => $date,
                    'types' => $types
                ]
            )
            ->orderBy('e.dateBegin', 'ASC');

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Find active events by types
     * Event::TYPE_PUBLIC or Event::TYPE_MEMBERS
     *
     * @param array    $types
     * @param integer  $limit
     *
     * @return array
     */
    public function findActiveByTypesAndLimit(array $types, $limit = 0)
    {
        $qb = $this
            ->createQueryBuilder('e')
            ->where('e.isActive = true')
            ->andWhere('e.isPublic IN (:types)')
            ->setParameter('types', $types)
            ->orderBy('e.dateBegin', 'ASC');

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}
