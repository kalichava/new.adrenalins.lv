<?php

namespace Adrenalins\EventsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Events admin
 */
class EventsAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('title', null, ['label' => 'Title'])
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, ['label' => 'Title'])
            ->add(
                'dateBegin',
                null,
                [
                    'pattern' => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.events.date.format'
                    ),
                    'locale'  => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.events.date.locale'
                    ),
                    'label'   => 'Date begin',
                ]
            )
            ->add(
                'dateEnd',
                null,
                [
                    'pattern' => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.events.date.format'
                    ),
                    'locale'  => $this->getConfigurationPool()->getContainer()->getParameter(
                        'adrenalins.events.date.locale'
                    ),
                    'label'   => 'End date',
                ]
            )
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => true])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ]
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $formMapper
            ->add(
                'author',
                null,
                [
                    'label'    => 'Author',
                    'required' => false
                ]
            )
            ->add(
                'dateBegin',
                'genemu_jquerydate',
                [
                    'label'    => 'Date begin',
                    'required' => false,
                    'widget'   => 'single_text'
                ]
            )
            ->add(
                'dateEnd',
                'genemu_jquerydate',
                [
                    'label'    => 'End date',
                    'required' => false,
                    'widget'   => 'single_text'
                ]
            );

        $i18nFormType = $container->getParameter('adrenalins.events.admin.i18n_form_type');

        $i18nFields = [
            'title'   => [
                'required' => true,
                'label'    => 'Title',
            ],
            'content' => [
                'required' => true,
                'attr'     => [
                    'class' => 'tinymce'
                ],
                'label'    => 'Content',
            ]
        ];

        $formMapper
            ->add(
                'translations',
                $i18nFormType,
                [
                    'label'              => 'Translations',
                    'translatable_class' => 'Adrenalins\EventsBundle\Entity\Event',
                    'fields'             => $i18nFields
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label' => 'Is Active'
                ]
            );
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setAuthor(
            $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()
        );

        return $instance;
    }
}