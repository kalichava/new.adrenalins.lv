<?php

namespace Adrenalins\StoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Validator\Constraints as Assert;
use Adrenalins\StoreBundle\Entity\TandemTransaction;

/**
 * Tandem cards store admin
 */
class TandemTransactionsAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('code', null, ['label' => 'Unique code'])
            ->add('dateValidFrom', null, ['label' => 'Valid from'])
            ->add('dateValidTo', null, ['label' => 'Valid to'])
            ->add('clientEmail', null, ['label' => 'Email'])
            ->add('clientPhone', null, ['label' => 'Phone'])
            ->add(
                'paymentStatus',
                'doctrine_orm_string',
                [
                    'label' => 'Payment Status',
                ],
                'choice',
                [
                    'choices' => TandemTransaction::getPaymentStatusList(),
                ]
            )
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, ['label' => 'Id'])
            ->addIdentifier('code', null, ['label' => 'Unique code'])
            ->add('dateValidFrom', null, ['label' => 'Valid from'])
            ->add('dateValidTo', null, ['label' => 'Valid to'])
            ->add('clientPhone', null, ['label' => 'Phone'])
            ->add('clientFullName', null, ['label' => 'Name'])
            ->add('flightQty', null, ['label' => 'Flights'])
            ->add('amountEur', null, ['label' => 'Amount'])
            ->add(
                'paymentStatus',
                null,
                [
                    'label'    => 'Is Payed',
                    'template' => 'PageBundle:TandemAdmin:_payment_status.html.twig',
                ]
            )
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => true]);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var TandemTransaction $object */
        $object = $this->getSubject();

        $formMapper
            ->with('General', ['class' => 'col-md-6'])
                ->add(
                    'code',
                    'text',
                    [
                        'label'    => 'Unique code',
                        'disabled' => 'disabled',
                    ]
                )
                ->add(
                    'discountCode',
                    'text',
                    [
                        'label'    => 'Unique code',
                        'disabled' => 'disabled',
                    ]
                )
                ->add(
                    'discountCode',
                    'text',
                    [
                        'label'    => 'Discount code',
                        'disabled' => 'disabled',
                    ]
                )
                ->add(
                    'flightQty',
                    'number',
                    [
                        'label' => 'Flights Qty',
                    ]
                )
                ->add(
                    'amount',
                    'text',
                    [
                        'label' => 'Amount',
                        'help'  => 'EUR'
                    ]
                )
                ->add(
                    'dateValidFrom',
                    'genemu_jquerydate',
                    [
                        'data'      => ($object && $object->getId()) ? $object->getDateValidFrom() : new \Datetime('now'),
                        'label'     => 'Valid from',
                        'widget'    => 'single_text',
                        'required'  => true
                    ]
                )
                ->add(
                    'dateValidTo',
                    'genemu_jquerydate',
                    [
                        'data'      => ($object && $object->getId()) ? $object->getDateValidTo() : new \Datetime('now'),
                        'label'     => 'Valid to',
                        'widget'    => 'single_text',
                        'required'  => true
                    ]
                )
                ->add(
                    'isActive',
                    null,
                    [
                        'label' => 'Is Active',
                    ]
                )
            ->end()
            ->with('Client', ['class' => 'col-md-6'])
                ->add(
                    'clientFullName',
                    null,
                    [
                        'label' => 'Full name',
                    ]
                )
                ->add(
                    'clientPhone',
                    null,
                    [
                        'label' => 'Phone',
                    ]
                )
                ->add(
                    'clientEmail',
                    null,
                    [
                        'label' => 'Email',
                    ]
                )
            ->end();
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}