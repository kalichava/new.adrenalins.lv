<?php

namespace Adrenalins\StoreBundle\Admin;

use Hashids\Hashids;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tandem discount codes admin
 */
class TandemDiscountCodeAdmin extends Admin
{
    protected $maxPerPage = 100;

    protected $datagridValues = [
        '_page'       => 1,
        '_sort_by'    => 'createdAt',
        '_sort_order' => 'DESC',
    ];

    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $mapper)
    {
        $mapper
            ->add('code', null, ['label' => 'Unique discount code'])
            ->add('isActive', null, ['label' => 'Is Active']);
    }

    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, ['label' => 'Id'])
            ->add('code', null, ['label' => 'Unique code'])
            ->add('percent', null, ['label' => 'Percent'])
            ->add('usageQty', null, ['label' => 'Usage Qty'])
            ->add('usedNumTimes', null, ['label' => 'Used time'])
            ->add('isActive', null, ['label' => 'Is Active', 'editable' => false])
            ->add(
                '_action',
                '_action',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ],
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'code',
                null,
                [
                    'label'    => 'Unique discount code',
                    'disabled' => 'disabled',
                ]
            )
            ->add(
                'percent',
                null,
                [
                    'label' => 'Percent of discount',
                ]
            )
            ->add(
                'usageQty',
                null,
                [
                    'label' => 'Discount usage Qty',
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label' => 'Is Active',
                ]
            )
            ->add(
                'isActive',
                null,
                [
                    'label'    => 'Is Active',
                    'disabled' => 'disabled',
                ]
            );
    }

    /**
     * @param mixed $object
     *
     * @return mixed|void
     */
    public function prePersist($object)
    {
        $hashIds = new Hashids('adrenalins_tandem_cards_store_discount', 10, 'abcdef1234567890');
        $object->setCode($hashIds->encode(time()));

        return $object;
    }
}