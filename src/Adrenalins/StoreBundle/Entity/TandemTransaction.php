<?php

namespace Adrenalins\StoreBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Adrenalins\UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="store_tandem_transactions")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("code")
 */
class TandemTransaction
{
    const PAYMENT_STATUS_NOT_PAID = 0;
    const PAYMENT_STATUS_PAID = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=true)
     */
    protected $code;

    /**
     * @ORM\ManyToOne(targetEntity="Adrenalins\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=true)
     */
    protected $client;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_code", type="string", nullable=true)
     */
    protected $discountCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_valid_from", type="datetime", nullable=true)
     */
    protected $dateValidFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_valid_to", type="datetime", nullable=true)
     */
    protected $dateValidTo;

    /**
     * @var string
     *
     * @ORM\Column(name="client_email", type="string", nullable=false)
     */
    protected $clientEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="client_phone", type="string", nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $clientPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="client_full_name", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $clientFullName;

    /**
     * @var string
     *
     * @ORM\Column(name="flight_qty", type="integer", nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1,
     *      max = 10
     * )
     */
    protected $flightQty = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="float", nullable=false)
     */
    protected $amount = 40;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date", type="datetime", nullable=true)
     */
    protected $paymentDate;

    /**
     * @ORM\Column(name="payment_status", type="smallint", nullable=false)
     */
    protected $paymentStatus = TandemTransaction::PAYMENT_STATUS_NOT_PAID;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TandemTransaction
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return User
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set client
     *
     * @param User $client
     *
     * @return TandemTransaction
     */
    public function setClient(User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return string
     */
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * @param string $discountCode
     *
     * @return TandemTransaction
     */
    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientFirstName()
    {
        $firstName = '';
        if ($this->getClientFullName() && mb_stristr($this->getClientFullName(), ' ')) {
            $name = explode(' ', $this->getClientFullName());
            $firstName = current($name);
        }

        return $firstName;
    }

    /**
     * @return string
     */
    public function getClientLastName()
    {
        $lastName = '';
        if ($this->getClientFullName() && mb_stristr($this->getClientFullName(), ' ')) {
            $name = explode(' ', $this->getClientFullName());
            $lastName = end($name);
        }

        return $lastName;
    }

    /**
     * @return string
     */
    public function getClientFullName()
    {
        return $this->clientFullName;
    }

    /**
     * @param string $clientFullName
     *
     * @return TandemTransaction
     */
    public function setClientFullName($clientFullName)
    {
        $this->clientFullName = $clientFullName;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param string $clientEmail
     *
     * @return TandemTransaction
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientPhone()
    {
        return $this->clientPhone;
    }

    /**
     * @param string $clientPhone
     *
     * @return TandemTransaction
     */
    public function setClientPhone($clientPhone)
    {
        $this->clientPhone = $clientPhone;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFlightQty()
    {
        return $this->flightQty;
    }

    /**
     * @param integer $flightQty
     *
     * @return TandemTransaction
     */
    public function setFlightQty($flightQty)
    {
        $this->flightQty = (int) $flightQty;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getAmountEur()
    {
        return $this->amount ." EUR";
    }

    /**
     * @param float $amount
     *
     * @return TandemTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $paymentDate
     *
     * @return TandemTransaction
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * Get paymentDate
     *
     * @return \DateTime
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @return integer
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param integer $paymentStatus
     *
     * @return TandemTransaction
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateValidFrom()
    {
        return $this->dateValidFrom;
    }

    /**
     * @param \DateTime $dateValidFrom
     *
     * @return TandemTransaction
     */
    public function setDateValidFrom($dateValidFrom)
    {
        $this->dateValidFrom = $dateValidFrom;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateValidTo()
    {
        return $this->dateValidTo;
    }

    /**
     * @param \DateTime $dateValidTo
     *
     * @return TandemTransaction
     */
    public function setDateValidTo($dateValidTo)
    {
        $this->dateValidTo = $dateValidTo;

        return $this;
    }

    /**
     * @param boolean $isActive
     *
     * @return TandemTransaction
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return array
     */
    public static function getPaymentStatusList()
    {
        return [
            self::PAYMENT_STATUS_NOT_PAID => "Waiting",
            self::PAYMENT_STATUS_PAID     => "Confirmed",
        ];
    }
}
