<?php

namespace Adrenalins\StoreBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Adrenalins\UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="store_tandem_discount")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("code")
 */
class TandemDiscountCode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=false)
     */
    protected $code;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float", nullable=false)
     */
    protected $percent = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="usage_qty", type="smallint", nullable=false)
     */
    protected $usageQty = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="used_num_times", type="smallint", nullable=false)
     */
    protected $usedNumTimes = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    protected $isActive = true;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TandemTransaction
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return User
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return TandemTransaction
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return TandemDiscountCode
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @return integer
     */
    public function getUsageQty()
    {
        return $this->usageQty;
    }

    /**
     * @param integer $usageQty
     *
     * @return TandemDiscountCode
     */
    public function setUsageQty($usageQty)
    {
        $this->usageQty = (int) $usageQty;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUsedNumTimes()
    {
        return $this->usedNumTimes;
    }

    /**
     * @param integer $usedNumTimes
     *
     * @return TandemDiscountCode
     */
    public function setUsedNumTimes($usedNumTimes)
    {
        $this->usedNumTimes = (int) $usedNumTimes;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @return TandemDiscountCode
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \Datetime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param boolean $isActive
     *
     * @return TandemDiscountCode
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
