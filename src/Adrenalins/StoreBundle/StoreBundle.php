<?php

namespace Adrenalins\StoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class StoreBundle
 *
 * @package Adrenalins\StoreBundle
 */
class StoreBundle extends Bundle
{
}
