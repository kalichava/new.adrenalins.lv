<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),

            new FOS\UserBundle\FOSUserBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Sonata\IntlBundle\SonataIntlBundle(),
            new Sonata\MediaBundle\SonataMediaBundle(),

            new JMS\I18nRoutingBundle\JMSI18nRoutingBundle(),
            new A2lix\TranslationFormBundle\A2lixTranslationFormBundle(),
            new Stfalcon\Bundle\TinymceBundle\StfalconTinymceBundle(),
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new Pix\SortableBehaviorBundle\PixSortableBehaviorBundle(),
            new Snc\RedisBundle\SncRedisBundle(),

            new FOS\RestBundle\FOSRestBundle(),
            new FOS\CommentBundle\FOSCommentBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle($this),
            new IAkumaI\SphinxsearchBundle\SphinxsearchBundle(),
            new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),

            new Adrenalins\TranslationBundle\TranslationBundle(),

            new Adrenalins\EmailTemplateBundle\EmailTemplateBundle(),
            new Adrenalins\FaqBundle\FaqBundle(),
            new Adrenalins\UserBundle\UserBundle(),
            new Adrenalins\PageBundle\PageBundle(),
            new Adrenalins\NewsBundle\NewsBundle(),
            new Adrenalins\SettingsBundle\SettingsBundle(),
            new Adrenalins\EventsBundle\EventsBundle(),
            new Adrenalins\SlidesBundle\SlidesBundle(),
            new Adrenalins\MenuBundle\MenuBundle(),
            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new Adrenalins\SearchBundle\SearchBundle(),
            new Adrenalins\PlaceBundle\PlaceBundle(),
            new Adrenalins\FeedbackBundle\FeedbackBundle(),
            new Adrenalins\NewsletterBundle\NewsletterBundle(),
            new JMS\Payment\CoreBundle\JMSPaymentCoreBundle(),
            new Adrenalins\StoreBundle\StoreBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
