<?php

$pathToImages = realpath(__DIR__ . '/../../web/uploads/media/');
$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$galleries = $oldDbh->query("SELECT * FROM gallery WHERE alt_2 != '' ORDER BY id ASC");
foreach ($galleries as $gallery) {

    $authorId = ($gallery['user_id']) ? (int) $gallery['user_id'] : 1;

    $stmt = $dbh->prepare("SELECT * FROM users WHERE id = :user_id LIMIT 1");
    $stmt->execute([':user_id' => $authorId]);
    $author = $stmt->fetch(PDO::FETCH_ASSOC);

    if (empty($author)) {
        $authorId = 1;
    }

    $stmt = $dbh->prepare("SELECT * FROM media__gallery WHERE name = :name LIMIT 1");
    $stmt->execute([':name' => $gallery['alt_2']]);
    $galleryItem = $stmt->fetch(PDO::FETCH_ASSOC);

    if (empty($galleryItem)) {

        $sql = "INSERT INTO media__gallery (
            author_id,
            name,
            context,
            default_format,
            enabled,
            updated_at,
            created_at,
            num_comments,
            is_public)
            VALUES (:authorId,:name,:context,:defaultFormat,:enabled,:updatedAt,:createdAt,:numComments,:isPublic)";

        $q = $dbh->prepare($sql);

        $galleryWasImported = $q->execute(
            [
                ':authorId'      => $authorId, // admin acc
                ':name'          => $gallery['alt_2'],
                ':context'       => 'photo',
                ':defaultFormat' => 'medium',
                ':enabled'       => 1,
                ':updatedAt'     => date('Y-m-d H:i:s', $gallery['post_date']),
                ':createdAt'     => date('Y-m-d H:i:s', $gallery['post_date']),
                ':numComments'   => 0,
                ':isPublic'      => !$gallery['hidden']
            ]
        );

        if ($galleryWasImported) {
            $galleryId = $dbh->lastInsertId();

            $galleryImages = $oldDbh->query(
                'SELECT * FROM gallery_images WHERE gallery_id = ' . $gallery['id'] . ' ORDER BY orderid ASC'
            );

            foreach ($galleryImages as $galleryImage) {
                $mediaSQL = "INSERT INTO media__media (
                author_id,
                name,
                description,
                enabled,
                provider_name,
                provider_status,
                provider_reference,
                provider_metadata,
                width,
                height,
                length,
                content_type,
                content_size,
                context,
                updated_at,
                created_at,
                is_public
            )
            VALUES
            (
                :authorId,
                :name,
                :description,
                :enabled,
                :providerName,
                :providerStatus,
                :providerReference,
                :providerMetadata,
                :width,
                :height,
                :length,
                :contentType,
                :contentSize,
                :context,
                :updatedAt,
                :createdAt,
                :isPublic
                )";

                $mediaQ = $dbh->prepare($mediaSQL);

                $nextInsertQuery = $dbh->query("SHOW TABLE STATUS LIKE 'media__media'");
                $nextInsertData = $nextInsertQuery->fetch();
                $nextMediaInsertId = $nextInsertData['Auto_increment'];

                $imageRemoteUrl = 'http://www.adrenalins.lv/data/gallery_head_images/' . $galleryImage['image'];
                $imageRemoteData = @getimagesize($imageRemoteUrl);

                if (!empty($imageRemoteData)) {

                    $imageName = md5(basename($galleryImage['image'])) . '.jpg';
                    $imagePath = $pathToImages . '/' . generatePath($nextMediaInsertId);
                    $fullImagePath = $imagePath . '/' . $imageName;

                    if (!file_exists($imagePath)) {
                        mkdir($imagePath, 0777, true);
                    }

                    if (!file_exists($fullImagePath)) {
                        if (copy($imageRemoteUrl, $fullImagePath)) {
                            $sizes = [
                                'admin'  => 100,
                                'tiny'   => 170,
                                'small'  => 300,
                                'medium' => 600,
                                'big'    => 1600
                            ];

                            $imagine = new \Imagine\Gd\Imagine();

                            foreach ($sizes as $prefix => $width) {

                                $thumbFilePath = $imagePath . '/thumb_' . $nextMediaInsertId;

                                if ($prefix == 'admin') {
                                    $thumbFilePath .= '_admin.jpg';
                                } else {
                                    $thumbFilePath .= '_photo_' . $prefix . '.jpg';
                                }

                                if (!file_exists($thumbFilePath)) {
                                    $thumb = $imagine->open($fullImagePath);
                                    if ($thumb->getSize()->getWidth() > $width) {
                                        $thumb->resize($thumb->getSize()->widen($width));
                                    }

                                    $thumb->save($thumbFilePath, ['jpeg_quality' => 95]);

                                    unset($thumb);
                                }

                                unset($thumbFilePath);

                            }

                        }
                    }

                    $size = filesize($fullImagePath);

                    $mediaWasImported = $mediaQ->execute(
                        [
                            ':name'              => $imageName,
                            ':enabled'           => 1,
                            ':authorId'          => $authorId,
                            ':description'       => $galleryImage['alt'],
                            ':providerName'      => 'sonata.media.provider.image',
                            ':providerStatus'    => 1,
                            ':providerReference' => $imageName,
                            ':providerMetadata'  => serialize(['filename' => $imageName]),
                            ':width'             => $imageRemoteData[0],
                            ':height'            => $imageRemoteData[1],
                            ':length'            => null,
                            ':contentType'       => $imageRemoteData['mime'],
                            ':contentSize'       => $size,
                            ':context'           => 'photo',
                            ':updatedAt'         => date('Y-m-d H:i:s'),
                            ':createdAt'         => date('Y-m-d H:i:s'),
                            ':isPublic'          => 1
                        ]
                    );

                    if ($mediaWasImported) {
                        $mediaId = $dbh->lastInsertId();

                        $dbh->query(
                            "INSERT INTO media__gallery_media (gallery_id, media_id, position, enabled, created_at, updated_at) VALUES ('{$galleryId}', '{$mediaId}', '" . $galleryImage['orderid'] . "', '1', '" . date(
                                'Y-m-d H:i:s'
                            ) . "', '" . date('Y-m-d H:i:s') . "')"
                        );
                    }

                }

            }

        }
    }

}

function generatePath($id, $firstLevel = 100000, $secondLevel = 1000)
{
    $context = 'photo';
    $rep_first_level = (int) ($id / $firstLevel);
    $rep_second_level = (int) (($id - ($rep_first_level * $firstLevel)) / $secondLevel);

    return sprintf('%s/%04s/%02s', $context, $rep_first_level + 1, $rep_second_level + 1);
}

$stmt = null;
$dbh = null;
