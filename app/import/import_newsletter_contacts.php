<?php

# импортировать пользователей в тандемную рассылку со старого сайта
# SELECT `mail`, `post_date`, `ip` FROM `emails_subscribers`

# импортировать пользователей в школьную рассылку со старого сайта:
# SELECT  `name`,  `email`,  `postdate`,  `ip`,  `phone`  FROM  `contacts`

# импортировать пользователей в клубную рассылку со старого сайта
# SELECT `name`, `email`, `phone` FROM `users`

$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

// Tandem newsletter contacts
$tandemEmailsSubscribers = $oldDbh->query("SELECT `mail` FROM `emails_subscribers`");
foreach ($tandemEmailsSubscribers as $tandemEmailsSubscriber) {

    $g = $dbh->prepare("INSERT INTO newsletter_contact (`email`, `group_id`) VALUES (:email, :group_id)");
    // id 3 = 'Tandem' group
    $g->execute([':email' => $tandemEmailsSubscriber['mail'], ':group_id' => 3]);

}

// School newsletter contacts
$schoolEmailsSubscribers = $oldDbh->query("SELECT `name`, `email`, `phone` FROM `contacts`");
foreach ($schoolEmailsSubscribers as $schoolEmailsSubscriber) {

    $g = $dbh->prepare("INSERT INTO newsletter_contact (`name`, `email`, `phone`, `group_id`) VALUES (:name, :email, :phone, :group_id)");
    // id 2 = 'School' group
    $g->execute(
        [
            ':name'     => $schoolEmailsSubscriber['name'],
            ':email'    => $schoolEmailsSubscriber['email'],
            ':phone'    => $schoolEmailsSubscriber['phone'],
            ':group_id' => 2
        ]
    );

}

// Club newsletter contacts
$clubEmailsSubscribers = $oldDbh->query("SELECT `name`, `email`, `phone` FROM `users` WHERE `email` != ''");
foreach ($clubEmailsSubscribers as $clubEmailsSubscriber) {

    $g = $dbh->prepare("INSERT INTO newsletter_contact (`name`, `email`, `phone`, `group_id`) VALUES (:name, :email, :phone, :group_id)");
    // id 1 = 'Club' group
    $g->execute(
        [
            ':name'     => $clubEmailsSubscriber['name'],
            ':email'    => $clubEmailsSubscriber['email'],
            ':phone'    => $clubEmailsSubscriber['phone'],
            ':group_id' => 1
        ]
    );

}
