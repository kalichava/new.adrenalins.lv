<?php

# восстановить группы контактов для рассылок, после изменения связи ManyToOne -> ManyToMany

$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$contacts = $oldDbh->query("SELECT id, group_id FROM newsletter_contact WHERE group_id IS NOT NULL ORDER BY id ASC");
foreach ($contacts as $oldContact) {

    $c = $dbh->prepare(
        "INSERT INTO newsletter_contacts_to_groups (`contact_id`, `contactgroup_id`) VALUES ('{$oldContact['id']}', '{$oldContact['group_id']}')"
    );
    $c->execute();

}