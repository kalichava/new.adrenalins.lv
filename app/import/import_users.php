<?php

$pathToImages = realpath(__DIR__ . '/../../web/uploads/users/');
$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

foreach ($oldDbh->query('SELECT * FROM users ORDER BY id ASC') as $row) {

    $id = ($row['id'] == 1) ? $row['id'] + 1000 : $row['id'];

    $stmt = $dbh->prepare("SELECT * FROM users WHERE id='{$id}' LIMIT 1");
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    // import only not exists users
    if(empty($user)) {

        $stmt = $dbh->prepare("SELECT * FROM users WHERE username='{$row['login']}' LIMIT 1");
        $stmt->execute();
        $userCheckUsername = $stmt->fetch(PDO::FETCH_ASSOC);

        if($userCheckUsername) {
            $row['login'] .= '_duplicate';
        }

        $sql = "INSERT INTO users (
            id,
            roles,
            salt,
            password,
            firstname,
            lastname,
            username,
            username_canonical,
            email,
            email_canonical,
            avatar,
            date_of_birth,
            biography,
            phone,
            enabled,
            created_at,
            updated_at)
            VALUES (:id, :roles, :salt, :password, :firstname, :lastname, :username, :username_canonical, :email, :email_canonical, :avatar, :date_of_birth, :biography, :phone, :enabled, :createdAt, :updatedAt)";

        $q = $dbh->prepare($sql);

        $roles = serialize(['ROLE_USER']);
        $email = $row['email'] ?: $row['login'] .'-without-email@adrenalins.lv';
        $phone = $row['phone'];
        $avatar = $row['image'];
        $fullName = explode(' ', $row['name']);
        $firstname = current($fullName);
        $lastname = end($fullName);
        $username = $row['login'];
        $biography = $row['about'];
        $dateOfBirth = date('Y-m-d H:i:s', $row['bithday']);

        // pass - omega8
        $passwordHash = '9KLUeP6AsAPJxPdIbXjOfaSmTvbqzj/dhch1lEqQRi1MLqr/+yLoPhlQfnt2FIUEvT4AiHviYKltsFhDQsKyXA==';
        $passwordSalt = 'qf05oys40n4wkcg8o4c44kk00k0wsgw';

        if ($row['image']) {
            $imageName = basename($row['image']);
            $fullImagePath = $pathToImages . '/' . $imageName;

            if (!file_exists($fullImagePath)) {
                if (copy(
                    'http://www.adrenalins.lv/data/gallery_users_images/' . rawurlencode($row['image']),
                    $fullImagePath
                )) {
                    try {
                        $imagine = new \Imagine\Gd\Imagine();
                        $imagine
                            ->open($fullImagePath)
                            ->thumbnail(new \Imagine\Image\Box(140, 140), \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND)
                            ->save($fullImagePath);

                        $avatar = '/uploads/users/' . $imageName;
                    } catch (\Exception $e) {

                    }
                }
            } else {
                $avatar = '/uploads/users/' . $imageName;
            }

        }

        $insertParams = [
            ':id'                 => $id,
            ':roles'              => $roles,
            ':salt'               => $passwordSalt,
            ':password'           => $passwordHash,
            ':firstname'          => $firstname,
            ':lastname'           => $lastname,
            ':username'           => $username,
            ':username_canonical' => $username,
            ':email'              => $email,
            ':email_canonical'    => $email,
            ':avatar'             => $avatar,
            ':date_of_birth'      => $dateOfBirth,
            ':biography'          => $biography,
            ':phone'              => $phone,
            ':enabled'            => 1,
            ':createdAt'          => date('Y-m-d H:i:s'),
            ':updatedAt'          => date('Y-m-d H:i:s')
        ];

        $userWasCreated = $q->execute($insertParams);

        if ($userWasCreated) {
            $userId = $dbh->lastInsertId();
            $gliders = explode(',', $row['glider_model']);

            foreach ($gliders as $gliderValue) {
                $gliderValue = trim(strip_tags($gliderValue));

                $dbh->query("INSERT INTO users_paragliders (user_id, model) VALUES ('{$userId}', '{$gliderValue}')");
            }

        } else {
            echo 'User '. $id .' '. $email .' not created' . PHP_EOL;
        }

    }

}

$stmt = null;
$dbh = null;
