<?php

use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Entity\MediaManager;

$loader = require_once __DIR__ . '/../bootstrap.php.cache';
require_once __DIR__ . '/../AppKernel.php';

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
$kernel->boot();

$pathToImages = realpath(__DIR__ . '/../../web/uploads/users/');
$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
$repo = $em->getRepository('UserBundle:User');

$ycnt = 0;
$vcnt = 0;
$importedCnt = 0;
$withoutVideoCnt = 0;

foreach ($oldDbh->query("SELECT * FROM news WHERE embed_video != '' ORDER BY id ASC") as $row) {

    $urls = [];
    $userId = ($row['user_id'] == 1) ? (int) $row['user_id'] + 1000 : (int) $row['user_id'];

    $stmt = $dbh->prepare("SELECT * FROM news WHERE title = :title LIMIT 1");
    $stmt->execute([':title' => $row['header']]);
    $newsItem = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!empty($newsItem)) {

        $stmt = $dbh->prepare("SELECT * FROM users WHERE id = :user_id LIMIT 1");
        $stmt->execute([':user_id' => $userId]);
        $oldUser = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($oldUser)) {
            $userId = 1;
        }

        $embedVideo = $row['embed_video'];
        $publishedAt = date('Y-m-d H:i:s', $row['post_date']);

        $user = $repo->find($userId);

        if ($user) {

            $embedVideo = stripcslashes($embedVideo);

            // youtube
            preg_match_all("/\/v\/(.{11})|\/embed\/(.{11})/", $embedVideo, $matches);
            $matches = array_filter($matches);

            if (!empty($matches)) {

                if (!empty($matches[2][0])) {
                    foreach (array_unique($matches[2]) as $youtubeId) {
                        $urls[] = 'https://www.youtube.com/watch?v=' . $youtubeId;
                    }
                } elseif (!empty($matches[1][0])) {
                    foreach (array_unique($matches[1]) as $youtubeId) {
                        $urls[] = 'https://www.youtube.com/watch?v=' . $youtubeId;
                    }
                }

                $ycnt++;
            } elseif (stristr($embedVideo, 'vimeo')) {
                $regex = '~
		# Match Vimeo link and embed code
		(?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
		(?:                             # Group vimeo url
				https?:\/\/             # Either http or https
				(?:[\w]+\.)*            # Optional subdomains
				vimeo\.com              # Match vimeo.com
				(?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
				\/                      # Slash before Id
				([0-9]+)                # $1: VIDEO_ID is numeric
				[^\s]*                  # Not a space
		)                               # End group
		"?                              # Match end quote if part of src
		(?:[^>]*></iframe>)?            # Match the end of the iframe
		(?:<p>.*</p>)?                  # Match any title information stuff
		~ix';
                preg_match_all($regex, $embedVideo, $matches);

                array_unique($matches[1]);

                if (count($matches[1])) {
                    foreach ($matches[1] as $match) {
                        $urls[] = 'https://vimeo.com/' . $match;
                    }
                } else {
                    preg_match(
                        '#http://(?:\w+.)?vimeo.com/(?:video/|moogaloop\.swf\?clip_id=)(\w+)#i',
                        $embedVideo,
                        $match
                    );
                    if (!empty($match)) {
                        $urls[] = 'https://vimeo.com/' . $match[1];
                    }
                }

                $vcnt++;
            }

            if (!empty($urls)) {

                foreach ($urls as $url) {

                    $url = trim(strip_tags($url));
                    $provider = 'sonata.media.provider.vimeo';
                    if (mb_stristr($url, 'youtube')) {
                        $provider = 'sonata.media.provider.youtube';
                    }

                    try {

                        /** @var MediaManager $mediaManager */
                        $mediaManager = $kernel->getContainer()->get('sonata.media.manager.media');

                        $media = new Media;
                        $media->setName($newsItem['title']);
                        $media->setAuthor($user);
                        $media->setContext('video');
                        $media->setBinaryContent($url);
                        $media->setProviderName($provider);
                        $media->setEnabled(true);
                        $media->setIsPublic(true);
                        $media->setUpdatedAt(new \DateTime($publishedAt));
                        $media->setCreatedAt(new \DateTime($publishedAt));

                        $user->setVideosNum($user->getVideosNum() + 1);

                        $mediaManager->save($media, true);

                        $n = $dbh->prepare(
                            "INSERT INTO news_media (`news_id`, `media_id`) VALUES (:news_id, :media_id)"
                        );
                        $n->execute([':news_id' => $newsItem['id'], ':media_id' => $media->getId()]);

                        $m = $dbh->prepare(
                            "INSERT INTO media_news (`media_id`, `news_id`) VALUES (:media_id, :news_id)"
                        );
                        $m->execute([':media_id' => $media->getId(), ':news_id' => $newsItem['id']]);

                        $importedCnt++;

                    } catch (\Exception $e) {
                        var_dump($url, $e->getMessage()); die('neok');
                    }

                }

            }

        }

    }

}

var_dump($importedCnt, $withoutVideoCnt, $vcnt, $ycnt);

$stmt = null;
$dbh = null;
