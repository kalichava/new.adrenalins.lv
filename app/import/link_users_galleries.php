<?php

# привязать фотогалереи к пользователям (таблица gallery поле user_id)

$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO('mysql:host=localhost;dbname=old_adrenalins', 'adrenalins', 'adrenalins', [PDO::ATTR_PERSISTENT => true]);
$dbh = new PDO('mysql:host=localhost;dbname=adrenalins', 'adrenalins', 'adrenalins', [PDO::ATTR_PERSISTENT => true]);

$galleries = $oldDbh->query("SELECT * FROM gallery WHERE alt_2 != '' ORDER BY id ASC");
foreach ($galleries as $oldGallery) {

    $title = $oldGallery['alt_2'];
    $userId = (int) $oldGallery['user_id'] ?: 1;

//    echo "UPDATE media__gallery SET `author_id` = '{$userId}' WHERE `name` = '{$title}';" . PHP_EOL;

    $dbh->query("UPDATE media__gallery SET `author_id` = '{$userId}' WHERE `name` = '{$title}'");
}