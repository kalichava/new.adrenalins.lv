<?php

$pathToImages = realpath(__DIR__ . '/../../web/uploads/news/');
$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$start = microtime(true);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$cnt = 0;

foreach ($oldDbh->query('SELECT * FROM news ORDER BY post_date ASC') as $row) {

    $publishedAt = date('Y-m-d H:i:s', $row['post_date']);

    $stmt = $dbh->prepare(
        "SELECT * FROM news WHERE title = :title LIMIT 1"
    );
    $stmt->execute([':title' => $row['header']]);
    $newsItem = $stmt->fetch(PDO::FETCH_ASSOC);

    if (empty($newsItem)) {

        $sql = "INSERT INTO news (
                author_id,
                type,
                title,
                slug,
                short_description,
                content,
                image_thumb,
                published_at,
                created_at,
                updated_at,
                is_active)
                VALUES (
                :author_id,
                :type,
                :title,
                :slug,
                :short_description,
                :content,
                :image_thumb,
                :published_at,
                :created_at,
                :updated_at,
                :is_active
                )";

        $q = $dbh->prepare($sql);

        $slug = url_slug($row['header'], ['delimiter' => '_', 'transliterate' => true]) .'_'. date('Ymd', $row['post_date']);
        $content = str_replace($row['lead'], '', $row['news_text']);
        $imageThumb = null;

        if ($row['img_with_thumb']) {

            $imageName = 'thumb-' . basename($row['img_with_thumb']);
            $fullImagePath = $pathToImages . '/' . $imageName;

            if (!file_exists($fullImagePath)) {
                if (@copy(
                    'http://www.adrenalins.lv/data/gallery_news_images/' . rawurlencode($row['img_with_thumb']),
                    $fullImagePath
                )
                ) {
                    try {
                        $imagine = new \Imagine\Gd\Imagine();
                        $thumb = $imagine->open($fullImagePath);
                        if ($thumb->getSize()->getWidth() > 300) {
                            $thumb->resize($thumb->getSize()->widen(300));
                        }

                        $thumb->save($fullImagePath, ['jpeg_quality' => 95]);

                        $imageThumb = '/uploads/news/' . $imageName;
                    } catch (\Exception $e) {

                    }
                }
            } else {
                $imageThumb = '/uploads/news/' . $imageName;
            }

        }

        $authorId = ((int) $row['user_id']) ? (int) $row['user_id'] : 1;

        $stmt = $dbh->prepare("SELECT * FROM users WHERE id = :user_id LIMIT 1");
        $stmt->execute([':user_id' => $authorId]);
        $author = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$author) {
            $authorId = 1;
        }

        $miniForumPID = 53;

        $isAdded = $q->execute(
            [
                ':type'              => ($row['pid'] == $miniForumPID) ? 1 : 0,
                ':author_id'         => $authorId,
                ':title'             => $row['header'],
                ':slug'              => $slug,
                ':short_description' => $row['lead'],
                ':content'           => $content,
                ':image_thumb'       => $imageThumb,
                ':published_at'      => $publishedAt,
                ':created_at'        => $publishedAt,
                ':updated_at'        => $publishedAt,
                ':is_active'         => 1
            ]
        );

        echo $isAdded . ' - ' . $authorId . ' - ' . $row['header'] . PHP_EOL;

        $cnt++;
    }

}

echo 'Count: ' . $cnt;

function url_slug($str, $options = [])
{
    $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

    $options = array_merge(
        [
            'delimiter'     => '-',
            'limit'         => null,
            'lowercase'     => true,
            'replacements'  => [],
            'transliterate' => false,
        ],
        $options
    );

    $char_map = [
        // Latin
        'À' => 'A',
        'Á' => 'A',
        'Â' => 'A',
        'Ã' => 'A',
        'Ä' => 'A',
        'Å' => 'A',
        'Æ' => 'AE',
        'Ç' => 'C',
        'È' => 'E',
        'É' => 'E',
        'Ê' => 'E',
        'Ë' => 'E',
        'Ì' => 'I',
        'Í' => 'I',
        'Î' => 'I',
        'Ï' => 'I',
        'Ð' => 'D',
        'Ñ' => 'N',
        'Ò' => 'O',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ö' => 'O',
        'Ő' => 'O',
        'Ø' => 'O',
        'Ù' => 'U',
        'Ú' => 'U',
        'Û' => 'U',
        'Ü' => 'U',
        'Ű' => 'U',
        'Ý' => 'Y',
        'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'æ' => 'ae',
        'ç' => 'c',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ð' => 'd',
        'ñ' => 'n',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'ő' => 'o',
        'ø' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ü' => 'u',
        'ű' => 'u',
        'ý' => 'y',
        'þ' => 'th',
        'ÿ' => 'y',
        // Latin symbols
        '©' => '(c)',
        // Greek
        'Α' => 'A',
        'Β' => 'B',
        'Γ' => 'G',
        'Δ' => 'D',
        'Ε' => 'E',
        'Ζ' => 'Z',
        'Η' => 'H',
        'Θ' => '8',
        'Ι' => 'I',
        'Κ' => 'K',
        'Λ' => 'L',
        'Μ' => 'M',
        'Ν' => 'N',
        'Ξ' => '3',
        'Ο' => 'O',
        'Π' => 'P',
        'Ρ' => 'R',
        'Σ' => 'S',
        'Τ' => 'T',
        'Υ' => 'Y',
        'Φ' => 'F',
        'Χ' => 'X',
        'Ψ' => 'PS',
        'Ω' => 'W',
        'Ά' => 'A',
        'Έ' => 'E',
        'Ί' => 'I',
        'Ό' => 'O',
        'Ύ' => 'Y',
        'Ή' => 'H',
        'Ώ' => 'W',
        'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'α' => 'a',
        'β' => 'b',
        'γ' => 'g',
        'δ' => 'd',
        'ε' => 'e',
        'ζ' => 'z',
        'η' => 'h',
        'θ' => '8',
        'ι' => 'i',
        'κ' => 'k',
        'λ' => 'l',
        'μ' => 'm',
        'ν' => 'n',
        'ξ' => '3',
        'ο' => 'o',
        'π' => 'p',
        'ρ' => 'r',
        'σ' => 's',
        'τ' => 't',
        'υ' => 'y',
        'φ' => 'f',
        'χ' => 'x',
        'ψ' => 'ps',
        'ω' => 'w',
        'ά' => 'a',
        'έ' => 'e',
        'ί' => 'i',
        'ό' => 'o',
        'ύ' => 'y',
        'ή' => 'h',
        'ώ' => 'w',
        'ς' => 's',
        'ϊ' => 'i',
        'ΰ' => 'y',
        'ϋ' => 'y',
        'ΐ' => 'i',
        // Russian
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'Yo',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'J',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'C',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Sh',
        'Ъ' => '',
        'Ы' => 'Y',
        'Ь' => '',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'sh',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        // Latvian
        'Ā' => 'A',
        'Č' => 'C',
        'Ē' => 'E',
        'Ģ' => 'G',
        'Ī' => 'i',
        'Ķ' => 'k',
        'Ļ' => 'L',
        'Ņ' => 'N',
        'Š' => 'S',
        'Ū' => 'u',
        'Ž' => 'Z',
        'ā' => 'a',
        'č' => 'c',
        'ē' => 'e',
        'ģ' => 'g',
        'ī' => 'i',
        'ķ' => 'k',
        'ļ' => 'l',
        'ņ' => 'n',
        'š' => 's',
        'ū' => 'u',
        'ž' => 'z'
    ];

    $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }

    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

    $str = trim($str, $options['delimiter']);

    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

$stmt = null;
$dbh = null;
