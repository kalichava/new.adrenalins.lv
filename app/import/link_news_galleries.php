<?php

# привязать фотогалереи к новостям и минифоруму (таблица news поле gallery)

$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$newsItems = $oldDbh->query("SELECT * FROM news WHERE gallery != '0' ORDER BY post_date ASC");
foreach ($newsItems as $oldNewsItem) {

    $oldNewsTitle = $oldNewsItem['header'];
    $oldGalleryId = (int) $oldNewsItem['gallery'];

    $stmt = $dbh->prepare("SELECT * FROM news WHERE title = :title LIMIT 1");
    $stmt->execute([':title' => $oldNewsItem['header']]);
    $newsItem = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($newsItem) {

        $stmt = $oldDbh->prepare("SELECT alt_2 FROM gallery WHERE id = :id LIMIT 1");
        $stmt->execute([':id' => $oldGalleryId]);
        $oldGallery = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($oldGallery) {

            $stmt = $dbh->prepare("SELECT * FROM media__gallery WHERE name = :name LIMIT 1");
            $stmt->execute([':name' => $oldGallery['alt_2']]);
            $galleryItem = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($galleryItem) {

                $stmt = $dbh->prepare("SELECT * FROM gallery_news WHERE gallery_id = :gallery_id AND news_id = :news_id LIMIT 1");
                $stmt->execute([':gallery_id' => $galleryItem['id'], ':news_id' => $newsItem['id']]);
                $galleryNewsItem = $stmt->fetch(PDO::FETCH_ASSOC);

                if(empty($galleryNewsItem)) {
                    $g = $dbh->prepare("INSERT INTO gallery_news (`gallery_id`, `news_id`) VALUES (:gallery_id, :news_id)");
                    $g->execute([':gallery_id' => $galleryItem['id'], ':news_id' => $newsItem['id']]);
                }

                $stmt = $dbh->prepare("SELECT * FROM news_gallery WHERE news_id = :news_id AND gallery_id = :gallery_id LIMIT 1");
                $stmt->execute([':news_id' => $newsItem['id'], ':gallery_id' => $galleryItem['id']]);
                $newsGalleryItem = $stmt->fetch(PDO::FETCH_ASSOC);

                if(empty($newsGalleryItem)) {
                    $n = $dbh->prepare("INSERT INTO news_gallery (`news_id`, `gallery_id`) VALUES (:news_id, :gallery_id)");
                    $n->execute([':news_id' => $newsItem['id'], ':gallery_id' => $galleryItem['id']]);
                }

                echo $galleryItem['id'] . ' - ' . $newsItem['id'] . PHP_EOL;

            }

        }

    }

}