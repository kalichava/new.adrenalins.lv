<?php

# привязать комментарии к новостям и минифоруму
# (таблица news_comments поле news_id, user_id, message, post_date)

$pathToProjectRoot = realpath(__DIR__ . '/../../');

require $pathToProjectRoot . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$oldDbh = new PDO(
    'mysql:host=localhost;dbname=old_adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh = new PDO(
    'mysql:host=localhost;dbname=adrenalins',
    'adrenalins',
    'adrenalins',
    [
        PDO::ATTR_PERSISTENT         => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    ]
);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$comments = $oldDbh->query("SELECT * FROM news_comments ORDER BY post_date ASC");
foreach ($comments as $oldComment) {

    $stmt = $oldDbh->prepare("SELECT id, header FROM news WHERE id = :news_id LIMIT 1");
    $stmt->execute([':news_id' => $oldComment['news_id']]);
    $oldNewsItem = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($oldNewsItem) {

        $stmt = $dbh->prepare("SELECT * FROM news WHERE title = :title LIMIT 1");
        $stmt->execute([':title' => $oldNewsItem['header']]);
        $newsItem = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($newsItem) {

            $threadId = sprintf('news-%s', $newsItem['id']);
            $threadUrl = sprintf('http://new.adrenalins.lv/news/%s', $newsItem['slug']);

            if ($oldComment['post_date'] == null) {
                $oldComment['post_date'] = strtotime($newsItem['published_at']);
            }

            $threadLastCommentAt = date('Y-m-d H:i:s', $oldComment['post_date']);

            $stmt = $dbh->prepare("SELECT * FROM comments_thread WHERE id = :thread_id LIMIT 1");
            $stmt->execute([':thread_id' => $threadId]);
            $threadItem = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($threadItem) {
                $t = $dbh->prepare(
                    "UPDATE comments_thread SET `num_comments` = num_comments + 1, `last_comment_at` = '{$threadLastCommentAt}' WHERE `id` = '{$threadId}'"
                );
                $t->execute();
            } else {
                $t = $dbh->prepare(
                    "INSERT INTO comments_thread (`id`, `permalink`, `is_commentable`, `num_comments`, `last_comment_at`) VALUES ('{$threadId}', '{$threadUrl}', '1', '1', '{$threadLastCommentAt}')"
                );
                $t->execute();
            }

            $userId = ($oldComment['user_id'] == 1) ? $oldComment['user_id'] + 1000 : $oldComment['user_id'];

            $stmt = $dbh->prepare("SELECT * FROM users WHERE id = :user_id LIMIT 1");
            $stmt->execute([':user_id' => $userId]);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($user) {

                $message = trim(
                    str_ireplace(["<br />\r\n<br />", "<br />", "<br>", "<br/>"], "\n", $oldComment['message'])
                );
                $message = strip_tags($message);

                $c = $dbh->prepare(
                    "INSERT INTO comments (`author_id`, `thread_id`, `body`, `depth`, `created_at`, `state`) VALUES
                                          (:author_id, :thread_id, :body, '0', :created_at, '0')"
                );
                $c->execute(
                    [
                        ':author_id'  => $userId,
                        ':thread_id'  => $threadId,
                        ':body'       => $message,
                        ':created_at' => $threadLastCommentAt
                    ]
                );

                $n = $dbh->prepare("UPDATE news SET `num_comments` = num_comments + 1 WHERE `id` = :news_id");
                $n->execute([':news_id' => $newsItem['id']]);

            }

        } else {
            echo 'News by title not found - ' . $oldNewsItem['id'] . ' - ' . $oldNewsItem['header'] . PHP_EOL;
        }

    } else {
        echo 'News not found - ' . $oldComment['news_id'] . PHP_EOL;
    }

}