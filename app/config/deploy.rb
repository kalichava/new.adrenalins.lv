set :application, "new.adrenalins"
set :domain,      "#{application}.lv"
set :deploy_to,   "/var/www/#{domain}"
set :app_path,    "app"

set :repository,  "git@bitbucket.org:iniweb/adrenalins.git"
set :scm,         :git

set :model_manager, "doctrine"

set :user,          "developer"
set :password,      "#9TJobFVmc$kzEWkkGcr"
set :use_sudo,      false
set :keep_releases, 3
set :use_composer,  true
set :composer_bin,  "composer"
ssh_options[:port] = "2233"

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set :shared_files,      [app_path + "/config/parameters.yml"]
set :shared_children,   [app_path + "/sessions", app_path + "/spool", app_path + "/logs", web_path + "/uploads", web_path + "/data", "vendor"]

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

namespace :custom do

    task :create_backup_dirs do
        run "mkdir -p #{deploy_to}/backup #{deploy_to}/backup/translations"
    end

    task :backup_i18n do
        run "cp -rf #{deploy_to}/current/app/Resources/translations/* #{deploy_to}/backup/translations/"
    end

    task :restore_i18n do
        run "cp -rf #{deploy_to}/backup/translations/* #{deploy_to}/current/app/Resources/translations/"

        run "chmod -R 777 #{deploy_to}/current/app/Resources/translations"
    end

    task :copy_admin_i18n do
        run "cp -f #{deploy_to}/current/i18n/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.lv.xliff #{deploy_to}/current/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.lv.xliff"
        run "cp -f #{deploy_to}/current/i18n/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.lv.xliff #{deploy_to}/current/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.lv.xliff"
        run "cp -f #{deploy_to}/current/i18n/vendor/friendsofsymfony/user-bundle/FOS/UserBundle/Resources/translations/FOSUserBundle.lv.yml #{deploy_to}/current/vendor/friendsofsymfony/user-bundle/FOS/UserBundle/Resources/translations/FOSUserBundle.lv.yml"
        run "cp -f #{deploy_to}/current/i18n/vendor/sonata-project/user-bundle/Resources/translations/FOSUserBundle.lv.xliff #{deploy_to}/current/vendor/sonata-project/user-bundle/Resources/translations/FOSUserBundle.lv.xliff"
        run "cp -f #{deploy_to}/current/i18n/vendor/sonata-project/user-bundle/Resources/translations/SonataUserBundle.lv.xliff #{deploy_to}/current/vendor/sonata-project/user-bundle/Resources/translations/SonataUserBundle.lv.xliff"
        run "cp -f #{deploy_to}/current/i18n/vendor/sonata-project/media-bundle/Resources/translations/SonataMediaBundle.lv.xliff #{deploy_to}/current/vendor/sonata-project/media-bundle/Resources/translations/SonataMediaBundle.lv.xliff"
    end

end

before "deploy", "custom:create_backup_dirs"
before "deploy", "custom:backup_i18n"

after "deploy", "custom:restore_i18n"
after "deploy", "custom:copy_admin_i18n"
after "deploy", "symfony:assetic:dump"
