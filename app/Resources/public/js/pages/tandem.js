$(document).ready(function () {
    $('.tandem-customer-discount-link a').click(function () {
        $(this).hide();
        $('.tandem-customer-discount-code').show();
    });
    $('.tandem-customer-amount').change(function () {
        var totalPrice = parseFloat(TANDEM_CARD_PRICE) * parseInt($(this).val());
        $('.tandem-customer-price h2').html('&euro; ' + totalPrice);
    });
    $('.card-usage-rules').magnificPopup({
        items: {
            src: '#card_usage_rules_text',
            type: 'inline'
        }
    });

});