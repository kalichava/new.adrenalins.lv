$(document).ready(function () {

    var url = (window.location.origin + window.location.pathname).split('?')[0],
        page = 1;

    $('#waterfall-video').waterfall({
        colWidth: 570,
        gutterWidth: 30,
        gutterHeight: 30,
        align: 'center',
        isAnimated: true,
        loadingMsg: '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>',
        checkImagesLoaded: true,
        path: function() {
            var path = url + '?page=' + page;
            page += 1;
            return path;
        },
        callbacks: {
            loadingFinished: function() {
                $('.video').magnificPopup({
                    type: 'iframe',
                    removalDelay: 300,
                    mainClass: 'mfp-fade'
                });
            },
            renderData: function (data, dataType) {
                var template;

                if ( dataType === 'json' ||  dataType === 'jsonp'  ) { // json or jsonp format
                    template = Handlebars.templates['video_items'];
                    return template(data);
                } else { // html format
                    return data;
                }
            }
        }
    });
});