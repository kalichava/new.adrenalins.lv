$(document).ready(function() {
    $("#adrenalins-calendar").fullCalendar({
        firstDay: 1,
        weekMode: 'liquid',
        height: 700,
        allDaySlot: false,
        buttonIcons: {
            prev: 'left-single-arrow',
            next: 'right-single-arrow'
        }
//        eventClick: function(calEvent, jsEvent, view) {
//            $(this).qtip();
//        }
    });
});