$(document).ready(function () {

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#event_dateBegin').fdatepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        },
        format: 'dd/mm/yyyy',
        weekStart: 1
    }).data('datepicker');
    var checkout = $('#event_dateEnd').fdatepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        },
        format: 'dd/mm/yyyy',
        weekStart: 1
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('div.user-form.add30t .button.wide a').magnificPopup({
        type: 'ajax',
        alignTop: true,
        overflowY: 'scroll',
        callbacks: {
            ajaxContentAdded: function() {
                var $form = $('form#form-link-objects');

                $form.find('.chosen-select').chosen({
                    no_results_text: 'No results text',
                    max_selected_options: 50
                });

                $form.find('.button.inverse.at-center.add20t a').click(function (e) {
                    e.preventDefault();

                    var $post = $.post($form.attr('action'), $form.serialize());
                    $post.done(function( data ) {
                        if(data.hasOwnProperty('success')) {
                            if(data.success) {

                                $.each(data.linked, function(i, item) {
                                    var $container = $('#linked-' + item.link + '-' + item.objectId).find('.button.wide');
                                    $container.after(
                                        $('<div>')
                                            .attr('class', 'linked-item')
                                            .append(
                                                $('<a>')
                                                    .attr({
                                                        'href' : '#delete',
                                                        'data-id' : item.objectId,
                                                        'data-name' : item.module,
                                                        'data-link-id' : item.id,
                                                        'data-link-name' : item.link
                                                    })
                                                    .addClass('unlink hastip')
                                                    .html('<svg class="svg-icon animate" viewBox="0 0 16 16"><use xlink:href="#icon-minus2"></use></svg>')
                                            )
                                            .append(
                                                $('<a>')
                                                    .attr('href', '#')
                                                    .addClass('title')
                                                    .text(item.name)
                                            )
                                    );
                                });

                                initUserContentControls();

                                $.magnificPopup.close();
                            }
                        }
                    });

                });
            }
        }
    });

    function initUserContentControls()  {
        $('.linked-item a.unlink').click(function(e) {
            e.preventDefault();

            var $object = $(e.currentTarget),
                module = $object.data('name'),
                objectId = parseInt($object.data('id'), 10),
                linkId = parseInt($object.data('link-id'), 10),
                linkName = $object.data('link-name');

            if(objectId && linkId && linkName) {
                var process = $.post('/user/content/remove-link', {
                    'module' : module,
                    'linkId' : linkId,
                    'linkName' : linkName,
                    'objectId' : objectId
                });

                process.done(function( data ) {
                    if(data.hasOwnProperty('success')) {
                        if(data.success) {
                            $object.parent().fadeOut('fast').remove();
                        }
                    }
                });
            }
        });
    }

    initUserContentControls();

});