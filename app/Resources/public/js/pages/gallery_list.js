$(document).ready(function () {

    var url = (window.location.origin + window.location.pathname).split('?')[0],
        page = 1;

    $('#waterfall').waterfall({
        colWidth: 370,
        gutterWidth: 30,
        align: 'center',
        gutterHeight: 20,
        checkImagesLoaded: true,
        isAnimated: true,
        loadingMsg: '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>',
        path: function() {
            var path = url + '?page=' + page;
            page += 1;
            return path;
        },
        callbacks: {
            renderData: function (data, dataType) {
                var template;

                if(parseInt(data.total, 10) == 0) {
                    $('#waterfall').waterfall('pause');
                }

                if ( dataType === 'json' ||  dataType === 'jsonp'  ) { // json or jsonp format
                    template = Handlebars.templates['gallery_items'];
                    return template(data);
                } else { // html format
                    return data;
                }
            }
        }
    });
});