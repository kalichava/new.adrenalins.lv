$(document).ready(function () {

    $('.slides-menu-item a img, .news-thumbnail img').resizeToParent();

    var i = 0;
    $('.slide-content').find('h2').textillate({
        autoStart: true,
        in: {
            effect: 'fadeIn',
            delay: 60
        },
        out: {
            effect: 'fadeOut',
            sync: true
        }
    });

    var mainpageSlides = function () {
        var current = i % 3;
        var next = (i + 1) % 3;
        var slidesMenuItem = $('.slides-menu-item');
        var slideContainer = $('.slide-container');
        var slideContent = $('.slide-content');

        if (slideContainer.length !== 1) {
            slideContainer.eq(current).removeClass('current');
            slideContainer.eq(next).addClass('current');
        }

        slidesMenuItem.eq(current).removeClass('current');
        slideContent.eq(current).removeClass('current');
        slideContent.eq(next).find('h2').textillate('out');

        slidesMenuItem.eq(next).addClass('current');
        slideContent.eq(next).addClass('current');
        slideContent.eq(next).find('h2').textillate('in');
        i++;
    };

    var timer = setInterval(mainpageSlides, 7000);
    $('h2, .action a').hover(function () {
        clearInterval(timer);
    }, function () {
        timer = setInterval(mainpageSlides, 7000);
    });
});
