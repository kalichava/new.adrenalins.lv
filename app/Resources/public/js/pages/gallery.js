$(document).ready(function () {
    $('.photogallery-list').each(function () {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            mainClass: 'mfp-fade',
            tLoading: '',
            removalDelay: 300,
            showCloseBtn: false,
            gallery: {
                enabled: true,
                preload: [1, 2]
            }
        });
    });

    var $phonegalleryItem = $('.photogallery-list li');
    if ($phonegalleryItem.length > 12) {
        $phonegalleryItem.first().addClass('cover');
    }

    $('.photogallery-list a img, .video-list img').resizeToParent();

    $('.video').magnificPopup({
        type: 'iframe',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });
});