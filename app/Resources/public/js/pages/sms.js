$(document).ready(function () {
    $('.sms-system > form').submit(function (e) {
        e.preventDefault();

        var $form = $(this),
            url = $form.attr("action");

        $form.removeClass('error');

        var sms = $.post(url, $(this).serialize());

        sms.done(function( data ) {
            if(data.hasOwnProperty('success')) {
                if(data.success) {

                    $.magnificPopup.open({
                        items: {
                            src: '<div class="white-popup">'+ data.message +'</div>',
                            type: 'inline'
                        },
                        midClick: true,
                        overflowY: 'auto',
                        removalDelay: 300,
                        closeBtnInside: true
                    }, 0);

                    $form.find('input[type=text], input[type=email]').val('');

                } else {
                    $form.addClass('error');
                }
            }
        });
    });

    $('.sms-section').hide();
    $('#sms-tabs a').bind('click', function(e) {
        e.preventDefault();

        $('#sms-tabs a.current').removeClass('current');
        $('.sms-section:visible').hide();
        $(this.hash).show();
        $(this).addClass('current');
    });
});