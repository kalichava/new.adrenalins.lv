$(document).ready(function () {

    $.ajaxSetup({ cache: false });

    $('#waterfall-related-galleries').waterfall({
        align: 'center',
        colWidth: 570,
        gutterWidth: 0,
        gutterHeight: 30,
        checkImagesLoaded: true,
        isAnimated: true,
        loadingMsg: '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>',
        path: function() {
            return window.location.href.split('?')[0];
        },
        callbacks: {
            renderData: function (data, dataType) {
                var template;

                $('#waterfall-related-galleries').waterfall('pause');

                if ( dataType === 'json' ||  dataType === 'jsonp'  ) { // json or jsonp format
                    template = Handlebars.templates['static_gallery_items'];
                    return template(data);
                } else { // html format
                    return data;
                }
            }
        }
    });

});