function init(markerDescription, markerLatLng) {
    var latLng = markerLatLng.split(','),
        places = [[markerDescription, latLng[0], latLng[1], 1]];

    var mapOptions = {
        center: new google.maps.LatLng(latLng[0], latLng[1]),
        zoom: 12,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT
        },
        disableDoubleClickZoom: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
        },
        scaleControl: true,
        scrollwheel: true,
        panControl: false,
        streetViewControl: true,
        draggable: true,
        overviewMapControl: true,
        overviewMapControlOptions: {
            opened: false
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var el = document.getElementById('map-canvas'),
        map = new google.maps.Map(el, mapOptions);

    setMarkers(map, places);

    function setMarkers(map, locations) {
        var image = {
            url: '/img/pointer.png?flat',
            size: new google.maps.Size(61, 61),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(31, 56)
        };

        var shape = {
            coord: [12, 23, 31, 56, 50, 23, 31, 5],
            type: 'poly'
        };

        var marker = '';
        for (var i = 0; i < locations.length; i++) {
            var place = locations[i],
                myLatLng = new google.maps.LatLng(place[1], place[2]);

            marker = new google.maps.Marker({
                map: map,
                icon: image,
                shape: shape,
                title: place[0],
                zIndex: place[3],
                position: myLatLng
            });

            google.maps.event.addListener(marker, 'click', function () {
                var html = "<div style='color:#000;background-color:#fff;padding:5px;width:150px;'><p>" + this.title + "<p></div>";
                iw = new google.maps.InfoWindow({content: html});
                iw.open(map, marker);
            });
        }
    }
}