$(function () {

    $('#fileupload').fileupload({
        url: galleryUploadUrl,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 10000000, // 10 MB
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 300,
        previewMaxHeight: 180,
        limitConcurrentUploads: 1,
        previewCrop: false,
        imageOrientation: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').addClass('file-item').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<div/>');
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append($('<div class="msg-error"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('.progress .progress-bar').css(
            'width',
            progress + '%'
        );

    }).on('fileuploadstart', function(e, data) {

        $('#fileupload').find('button[type=submit]').attr('disabled', 'disabled');

    }).on('fileuploaddone', function (e, data) {

        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .attr('title', file.name)
                    .prop('href', file.url);

                $(data.context.children()[index]).wrap(link);

                var button = $('<button>')
                    .attr('href', file.deleteUrl)
                    .text(file.deleteType);

                var deleteButton = $('<div>')
                    .addClass('button')
                    .html(button);

                $(data.context.children()[index])
                    .after(deleteButton);

                button.on('click', function(e) {
                    e.preventDefault();
                    deleteTmpImage(e);
                });

            } else if (file.error) {
                var error = $('<div class="msg-error"/>').text(file.error);
                $(data.context.children()[index])
                    .append(error);
            }
        });

        var $container = $('#files'),
            filesTotalNum = $container.find('.file-item').length,
            uploadedTotalNum = $container.find('.file-item').find('button').length;

        if(uploadedTotalNum >= filesTotalNum) {
            // all files have finished uploading, re-enable submit
            $('#fileupload').find('button[type=submit]').removeAttr('disabled');
        }

    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<div class="msg-error"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('.files').each(function () {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            mainClass: 'mfp-fade',
            tLoading: '',
            removalDelay: 300,
            showCloseBtn: false,
            gallery: {
                enabled: true,
                preload: [1, 2]
            }
        });
    });

    $('.file-item .button button').on('click', function(e) {
        e.preventDefault();
        deleteTmpImage(e);
    });

    function deleteTmpImage(e) {
        var $object = $(e.currentTarget),
            url = $object.attr('href');

        $.get(url, function(data) {
            if(data.success) {
                $object.parents('.file-item').remove();
            }
        });
    }

});
