var $collectionHolder;
var collectionHolderLength = 0;

var svg = '<svg class="svg-icon add-new-pg" viewBox="0 0 16 16"><use xlink:href="#icon-plus2"></use></svg>';
var $addParagliderLink = $('<a href="#" class="add_paraglider_link">'+ svg +'</a>');
var $newLinkLi = $('<li></li>').append($addParagliderLink);

jQuery(document).ready(function() {
    $collectionHolder = $('ul.paragliders');
    $collectionHolder.append($newLinkLi);

    collectionHolderLength = $collectionHolder.find(':input').length;
    $collectionHolder.data('index', collectionHolderLength);

    $addParagliderLink.on('click', function(e) {
        e.preventDefault();

        addParagliderForm($collectionHolder, $newLinkLi);
    });

    if(collectionHolderLength == 0) {
        $addParagliderLink.click();
    }
});

function addParagliderForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}