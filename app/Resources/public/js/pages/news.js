function parseQueryString() {
    var query = (window.location.search || '?').substr(1),
        map = {};
    query.replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function (match, key, value) {
        (map[key] = map[key] || []).push(value);
    });
    return map;
}

$(document).ready(function () {

    var newsUrl = window.location.href.split('?')[0],
        newsItemTpl = Handlebars.templates['news_item'],
        $container = $('ul.news-list.rows'),
        $showMoreButton = $('div.button.at-center'),
        $showMoreButtonLink = $showMoreButton.find('a'),
        nextPageNum = parseInt($showMoreButtonLink.data('page'), 10);

    $showMoreButton.on('click', function (e) {
        e.preventDefault();

        var params = parseQueryString(),
            categoriesParamName = 'categories[]',
            filters = {
                'page': nextPageNum
            };

        if(params.hasOwnProperty(categoriesParamName)) {
            filters[categoriesParamName] = params[categoriesParamName];
        }

        var $loader = $('.spinner').clone().insertAfter('ul.news-list').show();

        $.get(newsUrl, filters, function (response) {
            if (response.hideButton) {
                $showMoreButton.hide();
            }

            if (response.news.length) {
                $.each(response.news, function (i, news) {
                    $container.append(
                        newsItemTpl({'news': news})
                    );
                });
            }

            if (response.nextPage) {
                nextPageNum = response.nextPage;
                $showMoreButtonLink.data('page', nextPageNum);
            }

            $('.news-thumbnail img').resizeToParent();
            $loader.hide().remove();

        });

        History.pushState(null, null, '?' + decodeURIComponent( $.param(filters) ));
    });

    var $categoriesTabs = $('.tabs.outlines.float-right > li');
    $categoriesTabs.on('click', function (e) {
        e.preventDefault();

        $('.spinner').show();

        var $currentTab = $(e.currentTarget),
            activeCategories = {'categories' : []};

        $currentTab.toggleClass('current');

        $categoriesTabs
            .filter('.current')
            .each(function () {
                activeCategories.categories.push($.trim($(this).data('slug')));
            })
        ;

        $.get(newsUrl, {'categories[]': activeCategories.categories}, function (response) {
            if (response.hideButton) {
                $showMoreButton.hide();
            } else {
                $showMoreButton.show();
            }

            var content = '';

            $.each(response.news, function (i, news) {
                content += newsItemTpl({'news': news});
            });

            $container.html(content);

            $('.news-thumbnail img').resizeToParent();
            $('.spinner').hide();
        });

        History.replaceState(null, null, '?' + decodeURIComponent( $.param(activeCategories) ));
    });

    $('.news-thumbnail img').resizeToParent();
});