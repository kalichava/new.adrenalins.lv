$(document).ready(function () {

    var searchUrl = window.location.href.split('?')[0],
        searchItemTpl = Handlebars.templates['search_item'],
        $container = $('ul.search-results'),
        $showMoreButton = $('div.button.at-center'),
        $showMoreButtonLink = $showMoreButton.find('a'),
        nextPageIndex = parseInt($showMoreButtonLink.attr('data-next-page-index')),
        searchQuery = $showMoreButtonLink.attr('data-search-query');

    $showMoreButtonLink.on('click', function (e) {
        e.preventDefault();

        $.get(searchUrl, {'page': nextPageIndex, 'query': searchQuery}, function (response) {
            if (Object.keys(response.docs).length) {
                $.each(response.docs, function (i, doc) {
                    $container.append(
                        searchItemTpl({'doc': doc})
                    );
                });
            }

            if (response.nextPageIndex) {
                $showMoreButtonLink.attr('data-next-page-index', response.nextPageIndex);
            }

            if (!response.showMore) {
                $showMoreButton.hide();
            }
        });
    });
});