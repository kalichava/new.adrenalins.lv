$(document).ready(function () {
    var $faqContainer = $('.faq-container');
    $faqContainer.find('.answer').hide();

    $faqContainer.find('.question').click(function () {
        $(this).parent().toggleClass('opened');
        $(this).next().slideToggle('300');
    });
});