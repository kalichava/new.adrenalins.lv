$(document).ready(function () {
    $(window).stellar({
        horizontalScrolling: false
    });
    $('.vp').viewportChecker({
        classToAdd: 'animated fadeFromBottom',
        offset: 100
    });
    $('.sticky .submenu a[href]').each(function () {
        this.href = this.href.concat('#start');
    });

    // mobile menu begin
    $("<select />").appendTo("nav.main-menu");
    $("<option />", {
        "selected": "selected",
        "value": "",
        "text": GOTO_MENU_LABEL
    }).appendTo("nav select");
    $("nav.main-menu a").each(function() {
        var el = $(this);
        $("<option />", {
            "value": el.attr("href"),
            "text": el.text()
        }).appendTo("nav.main-menu select");
    });
    $("nav.main-menu select").change(function() {
        window.location = $(this).find("option:selected").val();
    });
    // mobile menu end

    $('.pilot-login-link').qtip({
        style: {
            def: false,
            classes: 'qtip-light pilot-login'
        },
        content: {
            text: $('#pilot-login')
        },
        position: {
            my: 'top right',
            at: 'bottom right',
            adjust: {
                x: 0,
                y: -30
            }
        },
        show: {
            solo: true,
            effect: function (offset) {
                $(this).fadeIn(300);
            }
        },
        hide: {
            fixed: true,
            event: 'unfocus',
            effect: function (offset) {
                $(this).fadeOut(300);
            }
        }
    });

    $(window).load(function () {
        var hash = location.hash.replace('#', '');
        if (hash === 'start') {
            var $startContainer = $('#start');
            if ($startContainer.length) {
                setTimeout(function () {
                    $('html, body').animate({scrollTop: $startContainer.offset().top - 40}, 1000);
                }, 500);
                $('html, body').mousewheel(function () {
                    $('html, body').stop();
                });
            }
        } else if(hash === 'login') {
            $('.pilot-login-link').qtip('show');
        }
    });

    $('.pilot-login').submit(function (e) {
        e.preventDefault();

        var $form = $(this),
            url = $form.attr("action"),
            username = $form.find("input[name='_username']").val(),
            password = $form.find("input[name='_password']").val();

        $form.removeClass('error');

        var login = $.post(url, { '_username' : username, '_password' : password });

        login.done(function (data) {
            if (data.hasOwnProperty('success')) {
                if (data.success) {
                    if (window.location.pathname == '/') {
                        window.location.hash = '#start';
                        window.location.reload();
                    } else {
                        window.location.href = window.location.origin + '/#start';
                    }
                } else {
                    $form.addClass('error');
                }
            }
        });
    });

    var $subMenus = $('.submenu.submenu-popup'),
        subMenusNum = $subMenus.length;

    $.each($subMenus, function(index, item) {

        var $item = $(item),
            itemId = parseInt($item.attr('id').replace('submenu-', ''), 10);

        $('#main-menu-link-' + itemId).qtip({
            style: {
                def: false,
                classes: 'qtip-submenu',
                width: 300,
                tip: false
            },
            content: {
                text: $item
            },
            position: {
                //my: (index === subMenusNum - 1) ? 'top right' : 'top center',
                //at: (index === subMenusNum - 1) ? 'bottom right' : 'bottom center'
                my: 'top center',
                at: 'bottom center'
            },
            show: {
                delay: 200,
                solo: true,
                effect: function () {
                    var visible = $(this).is(':visible');
                    if (!visible) {
                        $(this).show();
                    }
                    $(this).animate({
                        top: '-=10px',
                        opacity: '1'
                    }, 300);
                    if (visible) {
                        $(this).hide();
                    }
                }
            },
            hide: {
                fixed: true,
                effect: function(offset) {
                    $(this).fadeOut(300);
                }
            }
        });
    });

    $('.user-menu-link').qtip({
        style: {
            def: false,
            classes: 'qtip-light user-menu'
        },
        content: {
            text: $('#user-menu')
        },
        position: {
            my: 'top right',
            at: 'bottom right'
        },
        show: {
            effect: function (offset) {
                $(this).fadeIn(300);
            }
        },
        hide: {
            fixed: true,
            event: 'unfocus',
            effect: function (offset) {
                $(this).fadeOut(300);
            }
        }
    });

    $('.hastip').qtip({
        style: {
            classes: 'qtip-light'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        },
        show: {
            effect: function(offset) {
                $(this).fadeIn(200);
            }
        }
    });

    var searchLinks = $('.search-link');
    searchLinks.click(function() {
        $('.panel').toggleClass('flip');
        searchLinks.toggleClass('flip');
        if($('.panel').hasClass('flip')) {
            $('.search-keyword').focus();
        }
    });

    $('form.promo-form, form.add40r').submit(function (e) {
        e.preventDefault();

        var $form = $(this),
            url = $form.attr("action");

        if($form.hasClass('email-form-attr') || $form.hasClass('sms-form-attr')) {
            $form.parent().removeClass('error');
        } else {
            $form.removeClass('error');
        }

        var feedback = $.post(url, $(this).serialize());

        feedback.done(function( data ) {
            if(data.hasOwnProperty('success')) {
                if(data.success) {

                    $.magnificPopup.open({
                        items: {
                            src: '<div class="white-popup">'+ data.message +'</div>',
                            type: 'inline'
                        },
                        midClick: true,
                        overflowY: 'auto',
                        removalDelay: 300,
                        closeBtnInside: true
                    }, 0);

                    $form.find('input[type=text], input[type=email]').val('');

                } else {
                    if($form.hasClass('email-form-attr') || $form.hasClass('sms-form-attr')) {
                        $form.parent().addClass('error');
                    } else {
                        $form.addClass('error');
                    }

                    $form.find('.capcha-image img').click();
                }
            }
        });
    });

    $('#feedback_captcha_refresh, #feedback_smsCaptcha_refresh').click(function(e) {
        var $image = $(this).find('img');
        if($image) {
            $image.attr('src', $image.data('url') + '?' + Math.random());
        }
    });

    $('.forms-container').css({
        'left': '-100%',
        'height': '260px'
    });

    $('.email-form-link').click(function (e) {
        e.preventDefault();

        $('.forms-container').animate({
            'left': '0%'
        });
        $('.promo-form.club').animate({
            'height': '250px'
        });
    });

    $('.sms-form-link').click(function (e) {
        e.preventDefault();

        $('.forms-container').animate({
            'left': '-200%'
        });
        $('.promo-form.club').animate({
            'height': '250px'
        });
    });

    $('.promo-form .back').click(function (e) {
        e.preventDefault();

        $('.forms-container').animate({
            'left': '-100%'
        });
        $('.promo-form.club').animate({
            'height': '260px'
        });
    });
});
