(function(window, $, easyXDM){
    "use strict";
    var FOS_COMMENT = {
        /**
         * Shorcut post method.
         *
         * @param string url The url of the page to post.
         * @param object data The data to be posted.
         * @param function success Optional callback function to use in case of succes.
         * @param function error Optional callback function to use in case of error.
         */
        post: function(url, data, success, error, complete) {
            // Wrap the error callback to match return data between jQuery and easyXDM
            var wrappedErrorCallback = function(response){
                if('undefined' !== typeof error) {
                    error(response.responseText, response.status);
                }
            };
            var wrappedCompleteCallback = function(response){
                if('undefined' !== typeof complete) {
                    complete(response.responseText, response.status);
                }
            };
            $.post(url, data, success).error(wrappedErrorCallback).complete(wrappedCompleteCallback);
        },

        /**
         * Shorcut get method.
         *
         * @param string url The url of the page to get.
         * @param object data The query data.
         * @param function success Optional callback function to use in case of succes.
         * @param function error Optional callback function to use in case of error.
         */
        get: function(url, data, success, error) {
            // Wrap the error callback to match return data between jQuery and easyXDM
            var wrappedErrorCallback = function(response){
                if('undefined' !== typeof error) {
                    error(response.responseText, response.status);
                }
            };
            $.get(url, data, success).error(wrappedErrorCallback);
        },

        /**
         * Gets the comments of a thread and places them in the thread holder.
         *
         * @param string identifier Unique identifier url for the thread comments.
         * @param string url Optional url for the thread. Defaults to current location.
         */
        getThreadComments: function(identifier, permalink) {
            var event = jQuery.Event('fos_comment_before_load_thread');

            event.identifier = identifier;
            event.params = {
                permalink: encodeURIComponent(permalink || window.location.href)
            };

            FOS_COMMENT.thread_container.trigger(event);
            FOS_COMMENT.get(
                FOS_COMMENT.base_url  + '/' + encodeURIComponent(event.identifier) + '/comments',
                event.params,
                // success
                function(data) {
                    FOS_COMMENT.thread_container.html(data);
                    FOS_COMMENT.thread_container.attr('data-thread', event.identifier);
                    FOS_COMMENT.thread_container.trigger('fos_comment_load_thread', event.identifier);
                    $('.spinner').hide();
                }
            );
        },

        /**
         * Initialize the event listeners.
         */
        initializeListeners: function() {
            FOS_COMMENT.thread_container.on('submit',
                'form.fos_comment_comment_new_form',
                function(e) {
                    var that = $(this);
                    var serializedData = FOS_COMMENT.serializeObject(this);

                    e.preventDefault();

                    var event = $.Event('fos_comment_submitting_form');
                    that.trigger(event);

                    if (event.isDefaultPrevented()) {
                        return;
                    }

                    var $loader = $('.spinner').clone().insertBefore(that.parents('li')).show();

                    FOS_COMMENT.post(
                        this.action,
                        serializedData,
                        // success
                        function(data, statusCode) {
                            FOS_COMMENT.appendComment(data, that);
                            that.trigger('fos_comment_new_comment', data);
                        },
                        // error
                        function(data, statusCode) {
                            //var parent = that.parents('.new-comment-form, .fos_comment_reply_form_display');
                            var parent = that.parent().parent();
                            parent.after(data);
                            parent.remove();
                        },
                        // complete
                        function(data, statusCode) {
                            $loader.hide().remove();
                            that.trigger('fos_comment_submitted_form', statusCode);
                        }
                    );
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_comment_reply_show_form',
                function(e) {
                    var form_data = $(this).data();
                    var that = $(this),
                        parent = that.parents('li'),
                        parentClasses = parent.attr('class').split(" "),
                        $container = '',
                        $commentForm = $('.new-comment-form'),
                        $commentThread = $("ul#fos_comment_thread");

                    if (parent.hasClass('fos_comment_comment_depth_0')) {
                        $container = parent.nextAll('.fos_comment_comment_depth_0').eq(0);
                        if (!$container.length) {
                            $container = $commentForm;
                        }
                    }

                    if (parent.hasClass('fos_comment_replying')) {
                        return that;
                    } else {
                        $('.fos_comment_replying').removeClass('fos_comment_replying');
                    }

                    var $loader = $('.spinner').clone();
                    if ($container.length) {
                        $container.before($loader.show());
                    } else {
                        if (parent.next('.fos_comment_comment_depth_0').length) {
                            parent.after($loader.show());
                        } else {
                            // reply on replay case
                            var parentId = parseInt(parent.attr('id').replace('fos_comment_', ''), 10),
                                replayParent = $commentThread.find("li[data-parent='" + parentId + "']:last");

                            if (replayParent.length) {

                                var isNext = true,
                                    nextIsReply;

                                while(isNext) {
                                    nextIsReply = replayParent.nextAll("li[data-parent='" + parseInt(replayParent.attr('id').replace('fos_comment_', ''), 10) + "']:last");
                                    if(nextIsReply.length) {
                                        replayParent = nextIsReply;
                                    } else {
                                        isNext = false;
                                    }
                                }

                                parent = (nextIsReply.length) ? nextIsReply : replayParent;
                            }

                            parent.after($loader.show());
                        }
                    }

                    parent.addClass('fos_comment_replying');
                    $('.fos_comment_reply_form_display').remove();

                    FOS_COMMENT.get(
                        form_data.url,
                        {parentId: form_data.parentId},
                        function (data) {

                            // changed default behavior, added after parent li node and hide general form
                            $commentForm.hide();

                            var offsetClass = 'fos_comment_comment_depth_' +
                                parseInt(
                                    parseInt(
                                        parentClasses.slice(-1)[0].replace('fos_comment_comment_depth_', ''),
                                        10
                                    )
                                    + 1,
                                    10
                                );

                            var $replyForm = $(data).addClass("fos_comment_reply_form_display " + offsetClass);

                            if ($container.length) {
                                $container.before($replyForm);
                            } else {
                                parent.after($replyForm);
                            }

                            $('html, body').animate({
                                scrollTop: $replyForm.offset().top
                            }, 1000);

                            that.trigger('fos_comment_show_form', data);

                            $loader.hide().remove();
                        }
                    );
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_comment_reply_cancel',
                function(e) {
                    var form_holder = $(this).closest('.fos_comment_comment_form_holder');

                    var event = $.Event('fos_comment_cancel_form');
                    form_holder.trigger(event);

                    if (event.isDefaultPrevented()) {
                        return;
                    }

                    $('.fos_comment_replying').removeClass('fos_comment_replying');
                    $('.fos_comment_reply_form_display').remove();
                    form_holder.remove();

                    // changed default behavior, show general form
                    $('.new-comment-form').show();
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_comment_edit_show_form',
                function(e) {
                    var form_data = $(this).data();
                    var that = $(this);

                    FOS_COMMENT.get(
                        form_data.url,
                        {},
                        function(data) {
                            var commentBody = $(form_data.container);

                            // save the old comment for the cancel function
                            commentBody.data('original', commentBody.html());

                            // show the edit form
                            commentBody.html(data);

                            that.trigger('fos_comment_show_edit_form', data);
                        }
                    );
                }
            );

            FOS_COMMENT.thread_container.on('submit',
                'form.fos_comment_comment_edit_form',
                function(e) {
                    var that = $(this);

                    FOS_COMMENT.post(
                        this.action,
                        FOS_COMMENT.serializeObject(this),
                        // success
                        function(data) {
                            FOS_COMMENT.editComment(data);
                            that.trigger('fos_comment_edit_comment', data);
                        },

                        // error
                        function(data, statusCode) {
                            var parent = that.parent();
                            parent.after(data);
                            parent.remove();
                        }
                    );

                    e.preventDefault();
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_comment_edit_cancel',
                function(e) {
                    FOS_COMMENT.cancelEditComment($(this).parents('.fos_comment_comment_body'));
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_comment_vote',
                function(e) {
                    var that = $(this);
                    var form_data = that.data();

                    // Get the form
                    FOS_COMMENT.get(
                        form_data.url,
                        {},
                        function(data) {
                            // Post it
                            var form = $($.trim(data)).children('form')[0];
                            var form_data = $(form).data();

                            FOS_COMMENT.post(
                                form.action,
                                FOS_COMMENT.serializeObject(form),
                                function(data) {
                                    $('#' + form_data.scoreHolder).html(data);
                                    that.trigger('fos_comment_vote_comment', data, form);
                                }
                            );
                        }
                    );
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_comment_remove',
                function(e) {
                    var form_data = $(this).data();

                    var event = $.Event('fos_comment_removing_comment');
                    $(this).trigger(event);

                    if (event.isDefaultPrevented()) {
                        return
                    }

                    // Get the form
                    FOS_COMMENT.get(
                        form_data.url,
                        {},
                        function(data) {
                            // Post it
                            var form = $($.trim(data)).children('form')[0];

                            FOS_COMMENT.post(
                                form.action,
                                FOS_COMMENT.serializeObject(form),
                                function(data) {
                                    var commentHtml = $($.trim(data));

                                    var originalComment = $('#' + commentHtml.attr('id'));

                                    originalComment.replaceWith(commentHtml);
                                }
                            );
                        }
                    );
                }
            );

            FOS_COMMENT.thread_container.on('click',
                '.fos_comment_thread_commentable_action',
                function(e) {
                    var form_data = $(this).data();

                    // Get the form
                    FOS_COMMENT.get(
                        form_data.url,
                        {},
                        function(data) {
                            // Post it
                            var form = $($.trim(data)).children('form')[0];

                            FOS_COMMENT.post(
                                form.action,
                                FOS_COMMENT.serializeObject(form),
                                function(data) {
                                    var form = $($.trim(data)).children('form')[0];
                                    var threadId = $(form).data().fosCommentThreadId;

                                    // reload the intire thread
                                    FOS_COMMENT.getThreadComments(threadId);
                                }
                            );
                        }
                    );
                }
            );
        },

        appendComment: function(commentHtml, form) {
            var form_data = form.data();

            if('' != form_data.parent) {
                var parent = $('#fos_comment_' + form_data.parent),
                    form_parent = form.closest('.fos_comment_comment_form_holder');

                if(form_parent.length) {
                    form_parent.parent().replaceWith(commentHtml);
                }

                // Remove the form
                $('.fos_comment_replying').removeClass('fos_comment_replying');
                form_parent.remove();
            } else {
                // Insert the comment
                form.parents('li').before(commentHtml);
                form.trigger('fos_comment_add_comment', commentHtml);

                // "reset" the form
                form = $(form[0]);
                form[0].reset();
                form.children('.fos_comment_form_errors').remove();
            }

            var $counter = $('span.fos-comment-count:last');
            if ($counter.length) {
                // update comments count num
                $('span.fos-comment-count').text(parseInt($counter.text(), 10) + 1);
            }

            // changed default behavior, show general form
            $('.new-comment-form').show();
        },

        editComment: function(commentHtml) {
            var commentHtml = $($.trim(commentHtml));
            var originalCommentBody = $('#' + commentHtml.attr('id')).children('.fos_comment_comment_body');

            originalCommentBody.html(commentHtml.children('.fos_comment_comment_body').html());
        },

        cancelEditComment: function(commentBody) {
            commentBody.html(commentBody.data('original'));
        },

        /**
         * easyXdm doesn't seem to pick up 'normal' serialized forms yet in the
         * data property, so use this for now.
         * http://stackoverflow.com/questions/1184624/serialize-form-to-json-with-jquery#1186309
         */
        serializeObject: function(obj)
        {
            var o = {};
            var a = $(obj).serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        },

        loadCommentCounts: function()
        {
            var threadIds = [];
            var commentCountElements = $('span.fos-comment-count');

            commentCountElements.each(function(i, elem){
                var threadId = $(elem).data('fosCommentThreadId');
                if(threadId) {
                    threadIds.push(threadId);
                }
            });

            FOS_COMMENT.get(
                FOS_COMMENT.base_url + '.json',
                {
                    ids: threadIds
                },
                function(data) {
                    // easyXdm doesn't always serialize
                    if (typeof data != "object") {
                        data = jQuery.parseJSON(data);
                    }

                    var threadData = {};

                    for (var i in data.threads) {
                        threadData[data.threads[i].id] = data.threads[i];
                    }

                    $.each(commentCountElements, function(){
                        var threadId = $(this).data('fosCommentThreadId');
                        if(threadId) {
                            FOS_COMMENT.setCommentCount(this, threadData[threadId]);
                        }
                    });
                }
            );

        },

        setCommentCount: function(elem, threadObject) {
            if (threadObject == undefined) {
                elem.innerHTML = '0';

                return;
            }

            elem.innerHTML = threadObject.num_comments;
        }
    };

    // Check if a thread container was configured. If not, use default.
    FOS_COMMENT.thread_container = window.fos_comment_thread_container || $('#fos_comment_thread');

    // AJAX via easyXDM if this is configured
    if(typeof window.fos_comment_remote_cors_url != "undefined") {
        /**
         * easyXDM instance to use
         */
        FOS_COMMENT.easyXDM = easyXDM.noConflict('FOS_COMMENT');

        /**
         * Shorcut request method.
         *
         * @param string method The request method to use.
         * @param string url The url of the page to request.
         * @param object data The data parameters.
         * @param function success Optional callback function to use in case of succes.
         * @param function error Optional callback function to use in case of error.
         */
        FOS_COMMENT.request = function(method, url, data, success, error) {
            // wrap the callbacks to match the callback parameters of jQuery
            var wrappedSuccessCallback = function(response){
                if('undefined' !== typeof success) {
                    success(response.data, response.status);
                }
            };
            var wrappedErrorCallback = function(response){
                if('undefined' !== typeof error) {
                    error(response.data.data, response.data.status);
                }
            };

            // todo: is there a better way to do this?
            FOS_COMMENT.xhr.request({
                    url: url,
                    method: method,
                    data: data
            }, wrappedSuccessCallback, wrappedErrorCallback);
        };

        FOS_COMMENT.post = function(url, data, success, error) {
            this.request('POST', url, data, success, error);
        };

        FOS_COMMENT.get= function(url, data, success, error) {
            // make data serialization equals to that of jquery
            var params = jQuery.param(data);
            url += '' != params ? '?' + params : '';

            this.request('GET', url, undefined, success, error);
        };

        /* Initialize xhr object to do cross-domain requests. */
        FOS_COMMENT.xhr = new FOS_COMMENT.easyXDM.Rpc({
                remote: window.fos_comment_remote_cors_url
        }, {
            remote: {
                request: {} // request is exposed by /cors/
            }
        });
    }

    // set the appropriate base url
    FOS_COMMENT.base_url = window.fos_comment_thread_api_base_url;

    // Load the comment if there is a thread id defined.
    if(typeof window.fos_comment_thread_id != "undefined") {
        // get the thread comments and init listeners
        FOS_COMMENT.getThreadComments(window.fos_comment_thread_id);
    }

    if(typeof window.fos_comment_thread_comment_count_callback != "undefined") {
        FOS_COMMENT.setCommentCount = window.fos_comment_thread_comment_count_callback;
    }

    //if($('span.fos-comment-count').length > 0) {
    //    FOS_COMMENT.loadCommentCounts();
    //}

    FOS_COMMENT.initializeListeners();

    window.fos = window.fos || {};
    window.fos.Comment = FOS_COMMENT;
})(window, window.jQuery, window.easyXDM);
