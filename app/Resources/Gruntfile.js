module.exports = function(grunt) {

grunt.initConfig({
    cssmin: {
        magic: {
            src: 'public/css/adrenalins.style.css',
            dest: 'public/css/adrenalins.style.css'
        }
    },
    autoprefixer: {
        magic: {
            src: 'public/css/adrenalins.style.css',
            dest: 'public/css/adrenalins.style.css'
        }    
    },
    cssbeautifier : {
        files : ['']
    },
    sass: {
        dist: {
            options: {
                style: 'expanded',
                sourcemap: 'none'
            },
            files: {
                'public/css/adrenalins.style.css': 'sass/adrenalins.style.scss'
            }
        }
    },
    watch: {
        styles: {
            files: 'sass/**/*.scss',
            tasks: ['sass', 'autoprefixer', 'cssmin']
        }
    }
});

grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-concat-css');
grunt.loadNpmTasks('grunt-autoprefixer');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-cssbeautifier');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-concat');

grunt.registerTask('default', ['sass']);
grunt.registerTask('transform-svg', 'Transform a SVG sprites to a JS file',
  function () {
    var LINE_LENGTH = 256, svg = [], i, l, content;

    content = grunt.file.read('public/img/svg-icons.svg');
    content = content.replace(/'/g, "\\'");
    content = content.replace(/>\s+</g, "><").trim();
    l = Math.ceil(content.length / LINE_LENGTH);

    for (i = 0; i < l; i++) {
      svg.push("'" + content.substr(i * LINE_LENGTH, LINE_LENGTH) + "'");
    }

    grunt.file.write('public/js/svg-icons.js',
      'var SVG_SPRITE = ' + svg.join('+\n') + ';');
  }
);
};