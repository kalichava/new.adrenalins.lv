upstream phpfcgi {
    server unix:/var/run/php5-fpm.sock;
}

server {
    listen 80;

    server_name {{ project_domain }};
    root        {{ project_doc_root }};

    sendfile                   on;
    tcp_nopush                 on;
    tcp_nodelay                on;
    client_max_body_size       20M;
    reset_timedout_connection  on;

    error_log   "/var/log/nginx/{{ project_name }}/error.log";
    access_log  off;

    gzip on;
    gzip_vary on;
    gzip_min_length 1100;
    gzip_buffers 64 8k;
    gzip_comp_level 3;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    gzip_disable "MSIE [1-6]\.";

    rewrite ^/app\.php/?(.*)$ /$1 permanent;

    location ~* ^.+\.(jpg|jpeg|gif|png|ico|css|pdf|ppt|txt|bmp|rtf|js)$ {
        access_log off;
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }

    location / {
        index app.php;
        try_files $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite ^(.*)$ /app.php/$1 last;
    }

    location ~ ^/(app|app_dev|config)\.php(/|$) {
        fastcgi_pass                phpfcgi;
        fastcgi_split_path_info     ^(.+\.php)(/.*)$;
        include                     fastcgi_params;
        fastcgi_param               SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param               HTTPS           off;
        fastcgi_buffer_size         128k;
        fastcgi_buffers             4 256k;
        fastcgi_busy_buffers_size   256k;
    }
}
